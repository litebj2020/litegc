<?php

namespace App\Http\Controllers;

use App\Models\Ttiers;
use App\Models\Ppays;
use App\Models\Pville;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class TtiersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('client.index', [
            'ClientList'=>Ttiers::where('TypeTiers', 1)->orderBy('CodeTiers'),
            'FournisseurList'=>Ttiers::where('TypeTiers', 0)->orderBy('CodeTiers'),
            'VilleList'=>Pville::all()->sortBy('NomVille'),
            'PaysList' =>Ppays::all()->sortBy('NomPays')
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'codeTiers' => 'required|max:10|unique:Ttiers,CodeTiers,',
            'intitule' => 'required',
            'noCompte' => 'required',
            'email' => 'email:rfc,dns',
        ]);

        try {
            Ttiers::create([
                'CodeTiers' => $request->codeTiers,
                'Intitule' => $request->intitule,
                'TypeTiers' => $request->typeTiers,
                'NoCompte' => $request->noCompte,
                'identifiantFiscal' => $request->idFiscal,
                'InfoFiscal1' => $request->infoFiscal1,
                'InfoFiscal2' => $request->infoFiscal2,
                'Contact1' => $request->contact1,
                'Contact2' => $request->contact2,
                'Email' => $request->email,
                'Adresse' => $request->adresse,
                'CodePostal' => $request->codePostal,
                'Region' => $request->region,
                'Ville' => $request->ville,
                'Pays' => $request->pays,
            ]);
            toastr()->success('Le nouveau tiers [' .$request->codeTiers .'] a été ajouté avec succès !');
            return back()->with('successMessage', "Le nouveau tiers [" .$request->codeTiers ."] a été ajouté avec succès !");

        } catch (QueryException $ex) {
            toastr()->error('Échec de création du tiers ! ' .$ex->getMessage());
            return back()->with('errorMessage', "Échec de création du tiers ! " .$ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ttiers  $ttiers
     * @return \Illuminate\Http\Response
     */
    public function show(Ttiers $ttiers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ttiers  $ttiers
     * @return \Illuminate\Http\Response
     */
    public function edit(Ttiers $ttiers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ttiers  $ttiers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'CodeTiers2' => 'required|max:10|unique:Ttiers,CodeTiers,',
            'Intitule2' => 'required',
            'TypeTiers2' => 'required',
            'NoCompte2' => 'required',
        ]);
            //dd($request);

        try {
            $currentData = Ttiers::find($id);
            $currentData->CodeTiers = $request->codeTiers2;
            $currentData->Intitule = $request->intitule2;
            $currentData->TypeTiers = $request->typeTiers2;
            $currentData->NoCompte = $request->noCompte2;
            $currentData->identifiantFiscal = $request->identifiantFiscal2;
            $currentData->InfoFiscal1 = $request->infoFiscal12;
            $currentData->InfoFiscal2 = $request->infoFiscal22;
            $currentData->Contact1 = $request->contact12;
            $currentData->Contact2 = $request->contact22;
            $currentData->Email = $request->email2;
            $currentData->Adresse = $request->adresse2;
            $currentData->CodePostal = $request->codePostal2;
            $currentData->Region = $request->region2;
            $currentData->Ville = $request->ville2;
            $currentData->Pays = $request->pays2;
            $currentData->save();

            toastr()->success('La modification du tiers a été effectuée avec succès !');
            return back()->with('successMessage', "La modification du tiers a été effectuée avec succès !");

        } catch (QueryException $ex) {
            toastr()->error('Échec de modification du tiers ! ' .$ex->getMessage());
            return back()->with('errorMessage', "Échec de modification du tiers ! " .$ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ttiers  $ttiers
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Ttiers::destroy($id);
            toastr()->success('La suppression du tiers a été effectuée avec succès !');
            return back()->with('successMessage', "La suppression du tiers a été effectuée avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de suppression du tiers ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de suppression du tiers ! " .$ex->getMessage());
        }
    }
}
