<?php

namespace App\Http\Controllers;

use App\Models\Tcomptegene;
use App\Models\Tjournal;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class TjournalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('journal.index', [
            'JournalList' => Tjournal::all()->sortBy('CodeJournal'),
            'NCompteList' => Tcomptegene::all()->sortBy('NoCompte'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'libelleJournal' => 'required',
        ]);

        try {
            Tjournal::create([
                'CodeJournal' => $request->codeJournal,
                'LibelleJournal' => $request->libelleJournal,
                'TypeJournal' => $request->typeJournal,
                'NoCompte' => $request->noCompte,
            ]);

            toastr()->success('Le nouveau journal [' . $request->codeJournal . '] a été ajoutée avec succès !');
            return back()->with('successMessage', "Le nouveau journal [" . $request->codeJournal . "] a été ajoutée avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de création du journal ! ' . $ex->getMessage());
            return back()->with('errorMessage', "Échec de création du journal ! " . $ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tjournal  $tjournal
     * @return \Illuminate\Http\Response
     */
    public function show(Tjournal $tjournal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tjournal  $tjournal
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('journal.edit', [
            'tjournal' => Tjournal::where('id', $id)->get()[0],
            'NCompteList' => Tcomptegene::all()->sortBy('NoCompte'),

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tjournal  $tjournal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'libelleJournal2' => 'required',
        ]);


        try {
            $currentData = Tjournal::find($id);
            //$currentData->CodeJournal = $request->codeJournal2;
            $currentData->LibelleJournal = $request->libelleJournal2;
            // $currentData->TypeJournal = $request->typeJournal2;
            // //$currentData->NoCompte = $request->noCompte2;
            $currentData->save();

            toastr()->success('La modification du journal a été effectuée avec succès !');
            return redirect()->route('journal.index')->with('successMessage', "La modification du journal a été effectuée avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de modification du journal ! ' . $ex->getMessage());
            return back()->with('errorMessage', "Échec de modification du journal ! " . $ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tjournal  $tjournal
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Tjournal::destroy($id);
            toastr()->success('La suppression du journal a été effectuée avec succès !');
            return back()->with('successMessage', "La suppression du journal a été effectuée avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de suppression du journal ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de suppression du journal ! " . $ex->getMessage());
        }
    }
}
