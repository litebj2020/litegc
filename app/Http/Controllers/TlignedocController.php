<?php

namespace App\Http\Controllers;

use App\Models\Tarticle;
use App\Models\Tlignedoc;
use App\Models\Psuivistock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

class TlignedocController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->typeDoc == 20) {
            //recupération du type de suivi
            $suivi = Tarticle::find($request->idArticle);
            //cas cmup
            if ($suivi && $suivi['CodeSuivi'] == 0) {
                $cmup = DB::table('StockArticle')->where('id',$request->idArticle)->get();
                $cmup = (float) $cmup[0]->CoutMoyen;

                try {
                    Tlignedoc::create([
                        'EntetedocId' => $request->idEntete,
                        'CodeDomaine' => $request->codeDomaine,
                        'TypeDoc' => $request->typeDoc,
                        'NoDocument' => $request->noDoc,
                        'DateDocument' => $request->dateDoc,
                        'ArticleId' => $request->idArticle,
                        'RefArticle' => $request->refArticle,
                        'Designation' => $request->designation,
                        'Complement' => $request->complement,
                        'Quantite' => (float)$request->quantite,
                        'PrixUnitHT' => (float)$request->prixUnitHT,
                        'PrixUnitTTC' => (float)$request->prixUnitHT,
                        'MontantHT' => (float)$request->quantite * (float)$request->prixUnitHT,
                        'MontantTTC' => (float)$request->quantite * (float)$request->prixUnitHT,
                        'MontantNET' => (float)$request->quantite * (float)$request->prixUnitHT,
                        'MagasinId',
                        'CodeMagasin',
                        'MvtStock' => 1,
                        'PrixRevientUnit' => $cmup,
                        'CMUP' => $cmup,
                    ]);
                    toastr()->success('Le nouvel article [' .$request->refArticle .'] a été ajouté avec succès !');
                    return back()->with('successMessage', "Le nouvel article [" .$request->refArticle ."] a été ajouté avec succès !");

                } catch (QueryException $ex) {
                    toastr()->error('Échec de création de article ! ' .$ex->getMessage());
                    return back()->with('errorMessage', "Échec de création de article ! " .$ex->getMessage());
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tlignedoc  $tlignedoc
     * @return \Illuminate\Http\Response
     */
    public function show(Tlignedoc $tlignedoc)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tlignedoc  $tlignedoc
     * @return \Illuminate\Http\Response
     */
    public function edit(Tlignedoc $tlignedoc)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tlignedoc  $tlignedoc
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tlignedoc $tlignedoc)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tlignedoc  $tlignedoc
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tlignedoc $tlignedoc)
    {
        //
    }
}
