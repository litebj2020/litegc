<?php

namespace App\Http\Controllers;

use App\Models\Psuivistock;
use App\Models\Punitevente;
use App\Models\Tcomptegene;
use App\Models\Tfamilleart;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class TfamilleartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('famillearticle.index', [
            'FamilleartList' => Tfamilleart::all(),
            'SuivistockList' => Psuivistock::all()->sortBy('LibelleSuivi'),
            'UniteventeList' => Punitevente::all()->sortBy('LibelleUnite'),
            'NCompteList' => Tcomptegene::all()->sortBy('NoCompte'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'codeFamille' => 'required|max:15|unique:Tfamilleart,CodeFamille',
            'libelleFamille' => 'required',
            'libelleSuivi' => 'required',
            // 'stockNegatif' => 'required',
            'libelleUnite' => 'required',
        ]);
        $stockNegatif = 0;
        $request->stockNegatif == "on" ? $stockNegatif = 1 : $stockNegatif = 0;

        list($id, $code) = explode("|", $request->libelleSuivi);

        try {
            Tfamilleart::create([
                'CodeFamille' => $request->codeFamille,
                'LibelleFamille' => $request->libelleFamille,
                'SuiviId' => $id,
                'CodeSuivi' => $code,
                'StockNegatif' => $stockNegatif,
                'UniteId' => $request->libelleUnite,
                'CptVente' => $request->noCompteVente,
                'CptAchat' => $request->noCompteAchat,
                'CptStock' => $request->noCompteStock,
            ]);
            toastr()->success('La famille d\'article [' . $request->codeFamille . '] a été ajoutée avec succès !');
            return back()->with('successMessage', "La famille d'article [" . $request->codeFamille . "] a été ajoutée avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de création de la famille article ! ' . $ex->getMessage());
            return back()->with('errorMessage', "Échec de création de la famille article ! " . $ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tfamilleart  $tfamilleart
     * @return \Illuminate\Http\Response
     */
    public function show(Tfamilleart $tfamilleart)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tfamilleart  $tfamilleart
     * @return \Illuminate\Http\Response
     */
    public function edit(Tfamilleart $tfamilleart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tfamilleart  $tfamilleart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request, $id);
        $request->validate([
            // 'codeFamille2' => 'required|max:15|unique:Tfamilleart,CodeFamille,'.$id,
            'libelleFamille2' => 'required',
            'libelleSuivi2' => 'required',
            //'stockNegatif2' => 'required',
            'libelleUnite2' => 'required',
        ]);

        $request->stockNegatif2 == "on" ? $stockNegatif = 1 : $stockNegatif = 0;

        list($identifiant, $code) = explode("|", $request->libelleSuivi2);

        try {

            $currentData = Tfamilleart::find($id);
            // dd($request->noCompteVente2, $request->noCompteAchat2, $request->noCompteStock2);

            $currentData->CodeFamille = $request->codeFamille2;
            $currentData->SuiviId = (int)$identifiant;
            $currentData->LibelleFamille = $request->libelleFamille2;
            $currentData->CodeSuivi = (int)$code;
            $currentData->StockNegatif = $stockNegatif;
            $currentData->UniteId = $request->libelleUnite2;
            $currentData->CptVente = $request->noCompteVente2;
            $currentData->CptAchat = $request->noCompteAchat2;
            $currentData->CptStock = $request->noCompteStock2;
            // dd($currentData);
            $currentData->save();

            toastr()->success('La modification de la famille article a été effectuée avec succès !');
            return back()->with('successMessage', "La modification de la famille article a été effectuée avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de modification de la famille article ! ' . $ex->getMessage());
            return back()->with('errorMessage', "Échec de modification de la famille article ! " . $ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tfamilleart  $tfamilleart
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Tfamilleart::destroy($id);
            toastr()->success('La suppression de la famille article a été effectuée avec succès !');
            return back()->with('successMessage', "La suppression de la famille article a été effectuée avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de suppression de la famille article ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de suppression de la famille article ! " . $ex->getMessage());
        }
    }
}
