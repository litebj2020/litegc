<?php

namespace App\Http\Controllers;

use toastr;
use App\Models\Ppays;
use App\Models\Pville;
use App\Models\Tmagasin;
use Illuminate\Http\Request;
use App\Models\Tcollaborateur;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

class TmagasinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('magasin.index', [
            'MagasinList' => Tmagasin::all(),
            'CollaborateurList' => Tcollaborateur::all()->sortBy('Matricule'),
            'VillesList' => Pville::all()->sortBy('NomVille'),
            'PaysList' => Ppays::all()->sortBy('NomPays')
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'codeMagasin' => 'required|max:20|unique:Tmagasin,CodeMagasin,',
        ]);

        try {
            if ($request->principal == null) {

                $isPrincipal = 0;

                Tmagasin::create([
                    'Intitule' => $request->intitule,
                    'CodeMagasin' => $request->codeMagasin,
                    'Contact' => $request->contact,
                    'Adresse' => $request->adresse,
                    'Ville' => $request->ville,
                    'Pays' => $request->nomPays,
                    'isPrincipal' => $isPrincipal,
                    'CollaborateurId' => $request->collaborateur,
                ]);
            } else {

                $isPrincipal = 1;

                DB::update('update tmagasin set isPrincipal = 0');
                Tmagasin::create([
                    'Intitule' => $request->intitule,
                    'CodeMagasin' => $request->codeMagasin,
                    'Contact' => $request->contact,
                    'Adresse' => $request->adresse,
                    'Ville' => $request->ville,
                    'Pays' => $request->nomPays,
                    'isPrincipal' => $isPrincipal,
                    'CollaborateurId' => $request->collaborateur,
                ]);
            };

            toastr()->success('Le magasin [' . $request->intitule . '] a été ajouté avec succès !');
            return back()->with('successMessage', " Le magasin [" . $request->intitule . "] a été ajouté avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de la création de la devise ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de la création de la devise ! " . $ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tmagasin  $tmagasin
     * @return \Illuminate\Http\Response
     */
    public function show(Tmagasin $tmagasin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tmagasin  $tmagasin
     * @return \Illuminate\Http\Response
     */
    public function edit(Tmagasin $tmagasin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tmagasin  $tmagasin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'codeMagasin2' => 'required|max:20|unique:Tmagasin,CodeMagasin,'.$id,
        ]);

        if ($request->principal2 == null) {

            $isPrincipal = 0;
            try {

                $currentData = Tmagasin::find($id);
                $currentData->Intitule = $request->intitule2;
                $currentData->CodeMagasin = $request->codeMagasin2;
                $currentData->Contact = $request->contact2;
                $currentData->Adresse = $request->adresse2;
                $currentData->Ville = $request->ville2;
                $currentData->Pays = $request->nomPays2;
                $currentData->isPrincipal = $isPrincipal;
                $currentData->CollaborateurId = $request->collaborateur2;
                $currentData->save();

                toastr()->success('La modification du magasin a été effectuée avec succès !');
                return back()->with('successMessage', "La modification du magasin a été effectuée avec succès !");
            } catch (QueryException $ex) {
                toastr()->error('Échec de modification du magasin ! ' . $ex->getMessage());
                return back()->with('errorMessage', "Échec de modification du magasin ! " . $ex->getMessage());
            }
        } else {

            $isPrincipal = 1;

            DB::update('update tmagasin set isPrincipal = 0');
            try {

                $currentData = Tmagasin::find($id);
                $currentData->Intitule = $request->intitule2;
                $currentData->CodeMagasin = $request->codeMagasin2;
                $currentData->Contact = $request->contact2;
                $currentData->Adresse = $request->adresse2;
                $currentData->Ville = $request->ville2;
                $currentData->Pays = $request->nomPays2;
                $currentData->isPrincipal = $isPrincipal;
                $currentData->CollaborateurId = $request->collaborateur2;
                $currentData->save();

                toastr()->success('La modification du magasin a été effectuée avec succès !');
                return back()->with('successMessage', "La modification du magasin a été effectuée avec succès !");
            } catch (QueryException $ex) {
                toastr()->error('Échec de modification du magasin ! ' . $ex->getMessage());
                return back()->with('errorMessage', "Échec de modification du magasin ! " . $ex->getMessage());
            }
        };
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tmagasin  $tmagasin
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $magasin = Tmagasin::find($id);
        if ($magasin->isPrincipal != 1) {

            try {
                Tmagasin::destroy($id);
                toastr()->success('La suppression du magasin a été effectuée avec succès !');
                return back()->with('successMessage', "La suppression du magasin a été effectuée avec succès !");
            } catch (QueryException $ex) {
                toastr()->error('Échec de suppression du magasin ! ', $ex->getMessage());
                return back()->with('errorMessage', "Échec de suppression du magasin ! " . $ex->getMessage());
            }
        } else {
            toastr()->error('Impossible de supprimer le magasin principal');
            return back()->with('errorMessage', "Impossible de supprimer le magasin principal ! ");
        }
    }
}
