<?php

namespace App\Http\Controllers;

use App\Models\Pdomaine;
use Illuminate\Http\Request;
use App\Models\Tcatcomptable;
use Illuminate\Database\QueryException;

class TcatcomptableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('catcomptable.index', [
            'CatcomptableList'=>Tcatcomptable::all(),
            'DomaineList' =>Pdomaine::all()->sortBy('NomDomaine')
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'intitule' => 'required|max:255',
            'nomDomaine' => 'required',
        ]);

        try {
            Tcatcomptable::create([
                'Intitule' => $request->intitule,
                'DomaineId' => $request->nomDomaine,
            ]);
            toastr()->success('La nouvelle catégorie comptable [' .$request->intitule .'] a été ajoutée avec succès !');
            return back()->with('successMessage', "La nouvelle catégorie comptable [" .$request->intitule ."] a été ajoutée avec succès !");

        } catch (QueryException $ex) {
            toastr()->error('Échec de création de la catégorie comptable ! ' .$ex->getMessage());
            return back()->with('errorMessage', "Échec de création de la catégorie comptable ! " .$ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tcatcomptable  $tcatcomptable
     * @return \Illuminate\Http\Response
     */
    public function show(Tcatcomptable $tcatcomptable)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tcatcomptable  $tcatcomptable
     * @return \Illuminate\Http\Response
     */
    public function edit(Tcatcomptable $tcatcomptable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tcatcomptable  $tcatcomptable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'intitule2' => 'required|max:255',
            'nomDomaine2' => 'required',
        ]);
            //dd($request);

        try {
            $currentData = Tcatcomptable::find($id);
            $currentData->Intitule = $request->intitule2;
            $currentData->DomaineId = $request->nomDomaine2;
            $currentData->save();

            toastr()->success('La modification de la catégorie comptable a été effectuée avec succès !');
            return back()->with('successMessage', "La modification de la catégorie comptable a été effectuée avec succès !");

        } catch (QueryException $ex) {
            toastr()->error('Échec de modification de la catégorie comptable ! ' .$ex->getMessage());
            return back()->with('errorMessage', "Échec de modification de la catégorie comptable ! " .$ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tcatcomptable  $tcatcomptable
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Tcatcomptable::destroy($id);
            toastr()->success('La suppression de la catégorie comptable a été effectuée avec succès !');
            return back()->with('successMessage', "La suppression de la catégorie comptable a été effectuée avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de suppression de la catégorie comptable ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de suppression de la catégorie comptable ! " .$ex->getMessage());
        }
    }
}
