<?php

namespace App\Http\Controllers;

use App\Models\Pdevise;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class PdeviseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('devise.index', ['DeviseList'=>Pdevise::all()->sortBy("CodeISO")]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'codeIso' => 'required|max:3|unique:Pdevise,CodeISO',
            'designation' => 'required|max:60',
            'symbole' => 'max:10',
        ]);


        try {
            if ($request->principal==null) {

                $isPrincipal=0;

                Pdevise::create([
                    'CodeISO' => $request->codeIso,
                    'Designation' => $request->designation,
                    'Symbole' => $request->symbole,
                    'Principal' => $isPrincipal,
                ]);
            } else {

                $isPrincipal=1;

                DB::update('update Pdevise set Principal = 0');

                Pdevise::create([
                    'CodeISO' => $request->codeIso,
                    'Designation' => $request->designation,
                    'Symbole' => $request->symbole,
                    'Principal' => $isPrincipal,
                ]);
                    //dd($isPrincipal);
            };

            toastr()->success('La devise [' .$request->codeIso .'] a été ajouté avec succès !');
            return back()->with('successMessage', " La devise [" .$request->codeIso ."] a été ajouté avec succès !");

        } catch(QueryException $ex){
            toastr()->error('Échec de la création de la devise ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de la création de la devise ! " .$ex->getMessage());

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pdevise  $pdevise
     * @return \Illuminate\Http\Response
     */
    public function show(Pdevise $pdevise)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pdevise  $pdevise
     * @return \Illuminate\Http\Response
     */
    public function edit(Pdevise $pdevise)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pdevise  $pdevise
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'codeIso2' => 'required|max:3|unique:Pdevise,CodeISO,'. $id,
            'designation2' => 'required|max:60',
            'symbole2' => 'max:10',
        ]);


        try {
            if ($request->principal2==null) {

                $isPrincipal=0;

                $currentData = Pdevise::find($id);
                $currentData->CodeISO = $request->codeIso2;
                $currentData->Designation = $request->designation2;
                $currentData->Symbole = $request->symbole2;
                $currentData->Principal = $isPrincipal;
                $currentData->save();
            } else {

                $isPrincipal=1;

                DB::update('update Pdevise set Principal = 0');

                $currentData = Pdevise::find($id);
                $currentData->CodeISO = $request->codeIso2;
                $currentData->Designation = $request->designation2;
                $currentData->Symbole = $request->symbole2;
                $currentData->Principal = $isPrincipal;
                $currentData->save();
            }


            toastr()->success('La modification de la devise a été effectuée avec succès !');
            return back()->with('successMessage', "La modification de la devise a été effectuée avec succès !");

        } catch(QueryException $ex){
            toastr()->error('Échec de modification de la devise ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de modification de la devise ! " .$ex->getMessage());
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pdevise  $pdevise
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $devise = Pdevise::find($id);
        if ($devise->Principal <= 0) {

            try {
                Pdevise::destroy($id);
                toastr()->success('La suppression de la devise a été effectuée avec succès !');
                return back()->with('successMessage', "La suppression de la devise a été effectuée avec succès !");
            } catch (QueryException $ex) {
                toastr()->error('Échec de suppression de la devise ! ', $ex->getMessage());
                return back()->with('errorMessage', "Échec de suppression de la devise ! " . $ex->getMessage());
            }
        } else {
            //dd('cccccc');
            toastr()->error('Impossible de supprimer la devise principale');
            return back()->with('errorMessage', "Impossible de supprimer la devise principale ! ");
        }
    }
}
