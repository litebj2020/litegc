<?php

namespace App\Http\Controllers;

use App\Models\Tarticle;
use App\Models\Psuivistock;
use App\Models\Punitevente;
use App\Models\Tcomptegene;
use App\Models\Tfamilleart;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class TarticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('article.index', [
            'DataList' => DB::table('StockArticle')->get(),
            'FamilleList' => Tfamilleart::all()->sortBy('CodeFamille'),
            'SuiviList' => Psuivistock::all()->sortBy('LibelleSuivi'),
            'UniteList' => Punitevente::all()->sortBy('LibelleUnite'),
            'StockInfo'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'refArticle' => 'required|max:20|unique:Tarticle,RefArticle',
            'designation' => 'required|max:255',
            'codeFamille' => 'required',
        ]);
        $famille = explode("|", $request->codeFamille);

        try {
            Tarticle::create([
                'RefArticle' => $request->refArticle,
                'Designation' => $request->designation,
                'FamilleId' => (int)$famille[0],
                'CptVente' => $famille[1],
                'CptAchat' => $famille[2],
                'CptStock' => $famille[3],
                'SuiviId' => (int)$famille[4],
                'CodeSuivi' => (int)$famille[5],
                'Type' => $request->type,
                'Marque' => $request->marque,
                'Nature' => $request->nature,
                'CodeBarre' => $request->codeBarre,
                'Actif' => 1,
                'StockNegatif' => (int)$famille[6],
                'UniteId' => (int)$famille[7],
            ]);
            toastr()->success('Le nouvel article [' . $request->refArticle . '] a été ajouté avec succès !');
            return back()->with('successMessage', "Le nouvel article [" . $request->refArticle . "] a été ajouté avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de création de article ! ' . $ex->getMessage());
            return back()->with('errorMessage', "Échec de création de article ! " . $ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tarticle  $tarticle
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('article.show', [
            'CurrentData' => Tarticle::findOrFail($id),
            'CompteList' => Tcomptegene::all()->sortBy('NoCompte'),
            'FamilleList' => Tfamilleart::all()->sortBy('CodeFamille'),
            'UniteList' => Punitevente::all()->sortBy('LibelleUnite'),
            'SuiviList' => Psuivistock::all()->sortBy('LibelleSuivi')
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tarticle  $tarticle
     * @return \Illuminate\Http\Response
     */
    public function edit(Tarticle $tarticle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tarticle  $tarticle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'designation' => 'required|max:255',
            'codeFamille' => 'required',
        ]);
        $famille = explode("|", $request->codeFamille);

        try {
            $currentData = Tarticle::find($id);

            $currentData->Designation = $request->designation;
            $currentData->FamilleId =  (int)$famille[0];
            $currentData->CptVente = $famille[1];
            $currentData->CptAchat = $famille[2];
            $currentData->CptStock = $famille[3];
            $currentData->SuiviId = (int)$famille[4];
            $currentData->CodeSuivi = (int)$famille[5];
            $currentData->StockNegatif = (int)$famille[6];
            $currentData->UniteId = (int)$famille[7];
            $currentData->Type = $request->type;
            $currentData->Marque = $request->marque;
            $currentData->Nature = $request->nature;
            $currentData->CodeBarre = $request->codeBarre;
            /*  $currentData->PrixVenteHT = $request->prixVenteHT2;
            $currentData->PrixVenteTTC = $request->prixVenteTTC2;
            $currentData->PrixAchatHT = $request->prixAchatHT2;
            $currentData->PrixAchatTTC = $request->prixAchatTTC2;
            $currentData->DernierPrixAchat = $request->dernierPrixAchat2; */
            $currentData->save();

            toastr()->success('La modification de article a été effectuée avec succès !');
            return back()->with('successMessage', "La modification de article a été effectuée avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de modification de article ! ' . $ex->getMessage());
            return back()->with('errorMessage', "Échec de modification de article ! " . $ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tarticle  $tarticle
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Tarticle::destroy($id);
            toastr()->success('La suppression de article a été effectuée avec succès !');
            return back()->with('successMessage', "La suppression de article a été effectuée avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de suppression de article ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de suppression de article ! " . $ex->getMessage());
        }
    }
}
