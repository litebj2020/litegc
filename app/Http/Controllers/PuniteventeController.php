<?php

namespace App\Http\Controllers;

use App\Models\Punitevente;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class PuniteventeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('unitevente.index', [
            'UniteventeList'=> Punitevente::all()->sortBy("LibelleUnite")]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'libelleUnite' => 'required|max:60|unique:Punitevente,LibelleUnite',
        ]);

        try {
            Punitevente::create([
                'LibelleUnite' => $request->libelleUnite,
            ]);
            toastr()->success('Unité de vente [' .$request->libelleUnite .'] a été ajouté avec succès !');
            return back()->with('successMessage', " Unité de vente [" .$request->libelleUnite ."] a été ajouté avec succès !");

        } catch(QueryException $ex){
            toastr()->error('Échec de la création Unité de vente ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de la création Unité de vente ! " .$ex->getMessage());

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Punitevente  $punitevente
     * @return \Illuminate\Http\Response
     */
    public function show(Punitevente $punitevente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Punitevente  $punitevente
     * @return \Illuminate\Http\Response
     */
    public function edit(Punitevente $punitevente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Punitevente  $punitevente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'libelleUnite2' => 'required|max:60|unique:Punitevente,LibelleUnite,'. $id,
        ]);


        try {
            $currentData = Punitevente::find($id);
            $currentData->LibelleUnite = $request->libelleUnite2;
            $currentData->save();

            toastr()->success('La modification unité de vente a été effectuée avec succès !');
            return back()->with('successMessage', "La modification unité de vente a été effectuée avec succès !");

        } catch(QueryException $ex){
            toastr()->error('Échec de modification unité de vente ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de modification unité de vente ! " .$ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Punitevente  $punitevente
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Punitevente::destroy($id);
            toastr()->success('La suppression unité de vente a été effectuée avec succès !');
            return back()->with('successMessage', "La suppression unité de vente a été effectuée avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de suppression unité de vente ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de suppression unité de vente ! " .$ex->getMessage());
        }
    }
}
