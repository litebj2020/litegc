<?php

namespace App\Http\Controllers;

use App\Models\Ppays;
use App\Models\Pville;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class PvilleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('ville.index', [
            'VilleList'=>Pville::all(),
            'PaysList' =>Ppays::all()->sortBy('NomPays')
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $request->validate([
            'nomVille' => 'required|max:255',
            'nomPays' => 'required',
        ]);

        try {
            Pville::create([
                'NomVille' => $request->nomVille,
                'PaysId' => $request->nomPays,
            ]);
            toastr()->success('La nouvelle ville [' .$request->nomVille .'] a été ajoutée avec succès !');
            return back()->with('successMessage', "La nouvelle ville [" .$request->nomVille ."] a été ajoutée avec succès !");

        } catch (QueryException $ex) {
            toastr()->error('Échec de création de la ville ! ' .$ex->getMessage());
            return back()->with('errorMessage', "Échec de création de la ville ! " .$ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pville  $pville
     * @return \Illuminate\Http\Response
     */
    public function show(Pville $pville)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pville  $pville
     * @return \Illuminate\Http\Response
     */
    public function edit(Pville $pville)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pville  $pville
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request);
        $request->validate([
            'nomVille2' => 'required|max:255',
            'nomPays2' => 'required',
        ]);
            //dd($request);

        try {
            $currentData = Pville::find($id);
            $currentData->NomVille = $request->nomVille2;
            $currentData->PaysId = $request->nomPays2;
            $currentData->save();

            toastr()->success('La modification de la ville a été effectuée avec succès !');
            return back()->with('successMessage', "La modification de la ville a été effectuée avec succès !");

        } catch (QueryException $ex) {
            toastr()->error('Échec de modification de la ville ! ' .$ex->getMessage());
            return back()->with('errorMessage', "Échec de modification de la ville ! " .$ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pville  $pville
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Pville::destroy($id);
            toastr()->success('La suppression de la ville a été effectuée avec succès !');
            return back()->with('successMessage', "La suppression de la ville a été effectuée avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de suppression de la ville ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de suppression de la ville ! " .$ex->getMessage());
        }
    }
}
