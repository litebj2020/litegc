<?php

namespace App\Http\Controllers;

use App\Models\Ttiers;
use App\Models\Pdomaine;
use App\Models\Tmagasin;
use App\Models\Tentetedoc;
use Illuminate\Http\Request;
use App\Models\Tcatcomptable;
use App\Models\Tcollaborateur;
use Illuminate\Database\QueryException;

class TentetedocController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('entetedoc.index', [
            'EntetedocList'=>Tentetedoc::all(),
            'PaysList' =>Pdomaine::all()->sortBy('NomDomaine'),
            'PaysList' =>Ttiers::all()->sortBy('CodeTiers'),
            'PaysList' =>Tmagasin::all()->sortBy('Intitule'),
            'PaysList' =>Tcollaborateur::all()->sortBy('Matricule'),
            'PaysList' =>Tcatcomptable::all()->sortBy('Intitule')
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $request->validate([
            'nomVille' => 'required|max:255',
            'nomPays' => 'required',
        ]);

        try {
            Tentetedoc::create([
                'NomVille' => $request->nomVille,
                'PaysId' => $request->nomPays,
            ]);
            toastr()->success('La nouvelle entête document [' .$request->nomVille .'] a été ajoutée avec succès !');
            return back()->with('successMessage', "La nouvelle entête document [" .$request->nomVille ."] a été ajoutée avec succès !");

        } catch (QueryException $ex) {
            toastr()->error('Échec de création de entête document ! ' .$ex->getMessage());
            return back()->with('errorMessage', "Échec de création de entête document ! " .$ex->getMessage());
        } */
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tentetedoc  $tentetedoc
     * @return \Illuminate\Http\Response
     */
    public function show(Tentetedoc $tentetedoc)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tentetedoc  $tentetedoc
     * @return \Illuminate\Http\Response
     */
    public function edit(Tentetedoc $tentetedoc)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tentetedoc  $tentetedoc
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      /*   $request->validate([
            'nomVille2' => 'required|max:255',
            'nomPays2' => 'required',
        ]);
            //dd($request);

        try {
            $currentData = Tentetedoc::find($id);
            $currentData->NomVille = $request->nomVille2;
            $currentData->PaysId = $request->nomPays2;
            $currentData->save();

            toastr()->success('La modification de entête document a été effectuée avec succès !');
            return back()->with('successMessage', "La modification de entête document a été effectuée avec succès !");

        } catch (QueryException $ex) {
            toastr()->error('Échec de modification de entête document ! ' .$ex->getMessage());
            return back()->with('errorMessage', "Échec de modification de entête document ! " .$ex->getMessage());
        } */
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tentetedoc  $tentetedoc
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       /*  try {
            Tentetedoc::destroy($id);
            toastr()->success('La suppression de entête document a été effectuée avec succès !');
            return back()->with('successMessage', "La suppression de entête document a été effectuée avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de suppression de entête document ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de suppression de entête document ! " .$ex->getMessage());
        } */
    }
}
