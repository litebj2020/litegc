<?php

namespace App\Http\Controllers;

use App\Models\Tlottempon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SessionLotController extends Controller
{
    public function index(Request $request)
    {
        $dataList = Tlottempon::all();

        return response()->json($dataList, Response::HTTP_OK);
    }

    public function store(Request $request)
    {

        $list = Tlottempon::create([
            'NoLot' => $request->noLot,
            'QteTotal' => $request->qteTotal,
            'QteEntree' => $request->qteEntree,
            'DatePeremption' =>  \date_format(\date_create($request->datePeremption), 'd/m/Y'),
            'DateFabrication' => $request->dateProduction,
            'ComplementSL' => $request->complement,
            'EnteteId' => $request->enteteId,
            'ArticleId' => $request->ref,
        ]);

        return response()->json(['code' => 200, 'message' => 'Lot ajouté avec success', 'data' => $list, 'QteTotal'=>$request->qteTotal], 200);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return response()->json(['reste' => Tlottempon::sum('QteEntree'), 'qteTotal' => $request->session()->get('qteLot', 0)], 200);
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
