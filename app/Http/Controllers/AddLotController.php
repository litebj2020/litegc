<?php

namespace App\Http\Controllers;

use App\Models\Tlignedoc;
use App\Models\Tentetedoc;
use App\Models\Tlottempon;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class AddLotController extends Controller
{
    public function savelotData(Request $request)
    {
        $datainfo = $request->session()->get('datainfo');
        $article = $datainfo['articleinfo'];
        $magasin = $datainfo['magasininfo'];
        $montantHT = $datainfo['montantHTinfo'];
        $lastData = $datainfo['lastdatainfo'];
        $stockArticle = $datainfo['stockarticleinfo'];
        $cmup = (float) $stockArticle[0]->CoutMoyen;
        $qteStock = (float) $stockArticle[0]->QteStock;
        $valStock = (float) $stockArticle[0]->ValeurStock;

        try {
            $list = Tlottempon::all();
            foreach ($list as $value) {
                Tlignedoc::create([
                    'EntetedocId' => $value->EnteteId,
                    'CodeDomaine' => $request->session()->get('codeDomaine'),
                    'TypeDoc' => $request->session()->get('typeDoc'),
                    'NoDocument' => $request->session()->get('noDoc'),
                    'DateDocument' => $request->session()->get('dateDoc'),
                    'ArticleId' => $value->ArticleId,
                    'RefArticle' => $article->RefArticle,
                    'Designation' => $request->session()->get('design'),
                    'Complement' => $value->complement,
                    'Quantite' => $value->QteEntree,
                    'PrixUnitHT' => $request->session()->get('prixUnitHT'),
                    'PrixUnitTTC' => $request->session()->get('prixUnitHT'),
                    'MontantHT' => (float)$value->QteEntree * (float)$request->session()->get('prixUnitHT'),
                    'MontantTTC' => (float)$value->QteEntree * (float)$request->session()->get('prixUnitHT'),
                    'MontantNET' => (float)$value->QteEntree * (float)$request->session()->get('prixUnitHT'),
                    'MagasinId' => $magasin[0],
                    'CodeMagasin' => $magasin[1],
                    'MvtStock' => 1,
                    'QteStock' => $lastData ? $lastData->QteStock + (float)$request->quantite : 0 + (float)$request->quantite,
                    'PrixRunit' => $request->session()->get('prixUnitHT'),
                    'CMUP' => ($valStock + $montantHT) / ($qteStock + $value->QteEntree),
                    'NoLot' => $value->NoLot,
                    'ComplementSL' => $value->ComplementSL,
                    'DateFabrication' => $value->DateFabrication,
                    'DatePeremption' => $value->DatePeremption,
                ]);

                $Total = Tlignedoc::where('EntetedocId', '=', $request->session()->get('idEntete'))->sum('MontantHT');

                $UpdateTotal = Tentetedoc::find($request->session()->get('idEntete'));
                $UpdateTotal->TotalHT = $Total;
                $UpdateTotal->TotalTTC = $Total;
                $UpdateTotal->save();
            }
            $request->session()->forget('open');
            $request->session()->forget('datainfo');
            $request->session()->forget('qteLot');
            $request->session()->forget('design');
            $request->session()->forget('magasin');
            $request->session()->forget('article');
            $request->session()->forget('dateDoc');
            $request->session()->forget('idEntete');
            $request->session()->forget('codeDomaine');
            $request->session()->forget('prixUnitHT');
            $request->session()->forget('noDoc');
            $request->session()->forget('typeDoc');
            $request->session()->forget('idEntete');
            Tlottempon::truncate();
            toastr()->success('Nouvelle ligne ajoutée au document !');
            return back()->with(['successMessage' => "Nouvelle ligne ajoutée au document !", 'lotList' => []]);
        } catch (QueryException $ex) {
            toastr()->error('Échec d\'ajout de la nouvelle ligne ! ' . $ex->getMessage());
            return back()->with('errorMessage', "Échec d'ajout de la nouvelle ligne !" . $ex->getMessage());
        }
    }
    public function deletelotline($id)
    {
        Tlottempon::destroy($id);
        return response()->json(['msg' => 'Le lot a éte supprimé avec success'], 200);
    }
}
