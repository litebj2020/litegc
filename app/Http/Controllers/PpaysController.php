<?php

namespace App\Http\Controllers;

use App\Models\Ppays;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Http\Controllers\Controller;

class PpaysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pays.index', ['PaysList'=>Ppays::all()->sortBy("NomPays")]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);

        $request->validate([
            'codePays' => 'required|max:3',
            'nomPays' => 'required|max:60|unique:Ppays,NomPays',
        ]);

        try {
            Ppays::create([
                'CodePays' => $request->codePays,
                'NomPays' => $request->nomPays,
            ]);
            toastr()->success('Le pays [' .$request->nomPays .'] a été ajouté avec succès !');
            return back()->with('successMessage', " Le pays[" .$request->nomPays ."] a été ajouté avec succès !");

        } catch(QueryException $ex){
            toastr()->error('Échec de la création du pays ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de la création du pays ! " .$ex->getMessage());

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ppays  $ppays
     * @return \Illuminate\Http\Response
     */
    public function show(Ppays $ppays)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ppays  $ppays
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ppays  $ppays
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //dd($id);
        $request->validate([
            'codePays2' => 'required|max:3',
            'nomPays2' => 'required|max:60|unique:Ppays,NomPays,'. $id,
        ]);


        try {
            $currentData = Ppays::find($id);
            $currentData->CodePays = $request->codePays2;
            $currentData->NomPays = $request->nomPays2;
            $currentData->save();

            toastr()->success('La modification du pays a été effectuée avec succès !');
            return back()->with('successMessage', "La modification du pays a été effectuée avec succès !");

        } catch(QueryException $ex){
            toastr()->error('Échec de modification du pays ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de modification du pays ! " .$ex->getMessage());
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ppays  $ppays
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //dd($id);
        try {
            Ppays::destroy($id);
            toastr()->success('La suppression du pays a été effectuée avec succès !');
            return back()->with('successMessage', "La suppression du pays a été effectuée avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de suppression du pays ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de suppression du pays ! " .$ex->getMessage());
        }
    }

}
