<?php

namespace App\Http\Controllers;

use App\Models\Tarticle;
use App\Models\Tentetedoc;
use App\Models\Tlignedoc;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TdocstockligneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->typeDoc == 20) {

            //recupération du type de suivi
            $article = Tarticle::find($request->idArticle);
            $magasin = explode('|', $request->emetteurId);

            $stockArticle = DB::table('StockArticle')->where('id', $request->idArticle)->get();
            $cmup = (float) $stockArticle[0]->CoutMoyen;
            $qteStock = (float) $stockArticle[0]->QteStock;
            $valStock = (float) $stockArticle[0]->ValeurStock;
            $montantHT = (float)$request->quantite * (float)$request->prixUnitHT;
            $lastData = Tlignedoc::where('ArticleId', '=', $request->idArticle)->orderBy('id', 'desc')->first();

            //cas cmup
            if ($article['CodeSuivi'] == 0 || $article['CodeSuivi'] == 1) {

                try {
                    Tlignedoc::create([
                        'EntetedocId' => $request->idEntete,
                        'CodeDomaine' => $request->codeDomaine,
                        'TypeDoc' => $request->typeDoc,
                        'NoDocument' => $request->noDoc,
                        'DateDocument' => $request->dateDoc,
                        'ArticleId' => $request->idArticle,
                        'RefArticle' => $request->refArticle,
                        'Designation' => $request->designation,
                        'Complement' => $request->complement,
                        'Quantite' => $request->quantite,
                        'PrixUnitHT' => $request->prixUnitHT,
                        'PrixUnitTTC' => $request->prixUnitHT,
                        'MontantHT' => $montantHT,
                        'MontantTTC' => $montantHT,
                        'MontantNET' => $montantHT,
                        'MagasinId' => $magasin[0],
                        'CodeMagasin' => $magasin[1],
                        'MvtStock' => 1,
                        'QteStock' => $lastData ? $lastData->QteStock + (float)$request->quantite : 0 + (float)$request->quantite,
                        'PrixRunit' => $request->prixUnitHT,
                        'CMUP' => ($valStock + $montantHT) / ($qteStock + $request->quantite),
                    ]);

                    $Total = Tlignedoc::where('EntetedocId', '=', $request->idEntete)->sum('MontantHT');

                    $UpdateTotal = Tentetedoc::find($request->idEntete);
                    $UpdateTotal->TotalHT = $Total;
                    $UpdateTotal->TotalTTC = $Total;
                    $UpdateTotal->save();

                    toastr()->success('Nouvelle ligne ajoutée au document !');
                    return back()->with(['successMessage' => "Nouvelle ligne ajoutée au document !", 'lotList' => []]);
                } catch (QueryException $ex) {
                    toastr()->error('Échec d\'ajout de la nouvelle ligne ! ' . $ex->getMessage());
                    return back()->with('errorMessage', "Échec d'ajout de la nouvelle ligne !" . $ex->getMessage());
                }
            }

            // cas lot
            if ($article['CodeSuivi'] == 2) {
                $data = [
                    'articleinfo' => $article,
                    'magasininfo' => $magasin,
                    'stockarticleinfo' => $stockArticle,
                    'montantHTinfo' => $montantHT,
                    'lastdatainfo' => $lastData
                ];

                $request->session()->put('open', true);
                $request->session()->put('datainfo', $data);
                $request->session()->put('qteLot', (float)$request->quantite);
                $request->session()->put('design', $request->designation);
                $request->session()->put('magasin', $magasin);
                $request->session()->put('article', $article->id);
                $request->session()->put('dateDoc', $request->dateDoc);
                $request->session()->put('idEntete', $request->idEntete);
                $request->session()->put('codeDomaine', $request->codeDomaine);
                $request->session()->put('prixUnitHT', $request->prixUnitHT);
                $request->session()->put('noDoc', $request->noDoc);
                $request->session()->put('typeDoc', $request->typeDoc);
                $request->session()->put('idEntete', (int)$request->idEntete);
                $request->session()->save();
                return back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
