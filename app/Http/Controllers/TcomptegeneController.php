<?php

namespace App\Http\Controllers;

use App\Models\Tcomptegene;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class TcomptegeneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('comptegeneral.index', [
            'ComptegeneList'=>Tcomptegene::all()->sortBy('NoCompte')
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'noCompte' => 'required|unique:Tcomptegene,NoCompte,',
            'intituleCompte' => 'required',
        ]);

        try {
            Tcomptegene::create([
                'NoCompte' => $request->noCompte,
                'IntituleCompte' => $request->intituleCompte,
            ]);
            toastr()->success('Le nouveau compte général [' .$request->noCompte .'] a été ajoutée avec succès !');
            return back()->with('successMessage', "Le nouveau compte géné [" .$request->noCompte ."] a été ajoutée avec succès !");

        } catch (QueryException $ex) {
            toastr()->error('Échec de création du compte général ! ' .$ex->getMessage());
            return back()->with('errorMessage', "Échec de création du compte général ! " .$ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tcomptegene  $tcomptegene
     * @return \Illuminate\Http\Response
     */
    public function show(Tcomptegene $tcomptegene)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tcomptegene  $tcomptegene
     * @return \Illuminate\Http\Response
     */
    public function edit(Tcomptegene $tcomptegene)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tcomptegene  $tcomptegene
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'noCompte2' => 'required|unique:Tcomptegene,NoCompte,'.$id,
            'intituleCompte2' => 'required',
        ]);
            //dd($request);

        try {
            $currentData = Tcomptegene::find($id);
            $currentData->NoCompte = $request->noCompte2;
            $currentData->IntituleCompte = $request->intituleCompte2;
            $currentData->save();

            toastr()->success('La modification du compte général a été effectuée avec succès !');
            return back()->with('successMessage', "La modification du compte géné a été effectuée avec succès !");

        } catch (QueryException $ex) {
            toastr()->error('Échec de modification du compte général ! ' .$ex->getMessage());
            return back()->with('errorMessage', "Échec de modification du compte général ! " .$ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tcomptegene  $tcomptegene
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Tcomptegene::destroy($id);
            toastr()->success('La suppression du compte général a été effectuée avec succès !');
            return back()->with('successMessage', "La suppression du compte géné a été effectuée avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de suppression du compte général ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de suppression du compte général ! " .$ex->getMessage());
        }
    }
}
