<?php

namespace App\Http\Controllers;

use App\Models\Ppays;
use App\Models\Pdevise;
use App\Models\Psociete;
use App\Models\Pville;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class PsocieteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $societe =  Psociete::all();
        if (sizeof($societe) == 1) {
            $societe = Psociete::all()[0];
        } else {
            $societe = null;
        }
        return view('societe.index', [
            'SocieteList' => $societe,
            'DeviseList' => Pdevise::all()->sortBy('CodeISO'),
            'PaysList' => Ppays::all()->sortBy("NomPays"),
            'VilleList' => Pville::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request->haveMcf) {
            $request->haveMcf = 1;
        } else {
            $request->haveMcf = 0;
        }
        $societe = Psociete::all();
        if (sizeof($societe) == 0) {
            try {

                if ($request->logo != null) {
                    $fileName = time() . '.' . $request->logo->extension();
                    $request->logo->move(public_path('uploads'), $fileName);


                    Psociete::create([
                        'TypeSociete' => $request->typeSociete,
                        'RaisonSociale' => $request->raisonSociale,
                        'Abrege' => $request->abrege,
                        'Capital' => $request->capital,
                        'Activite' => $request->activite,
                        'Contact1' => $request->contact1,
                        'Contact2' => $request->contact2,
                        'Fax' => $request->fax,
                        'Email' => $request->email,
                        'SiteWeb' => $request->siteWeb,
                        'Adresse' => $request->adresse,
                        'CodePostal' => $request->codePostal,
                        'Ville' => $request->ville,
                        'Region' => $request->region,
                        'Pays' => $request->pays,
                        'IdentifiantFiscal' => $request->identifiantFiscal,
                        'InfoFiscal1' => $request->infoFiscal1,
                        'InfoFiscal2' => $request->infoFiscal2,
                        'Complement' => $request->complement,
                        'logo' => $fileName,
                        'DeviseId' => $request->codeIso,
                        'HaveMCF' => $request->haveMcf,
                    ]);
                    toastr()->success('La nouvelle société [' . $request->abrege . '] a été ajoutée avec succès !');
                    return back()->with('successMessage', "La nouvelle société [" . $request->abrege . "] a été ajoutée avec succès !");
                } else {

                    Psociete::create([
                        'TypeSociete' => $request->typeSociete,
                        'RaisonSociale' => $request->raisonSociale,
                        'Abrege' => $request->abrege,
                        'Capital' => $request->capital,
                        'Activite' => $request->activite,
                        'Contact1' => $request->contact1,
                        'Contact2' => $request->contact2,
                        'Fax' => $request->fax,
                        'Email' => $request->email,
                        'SiteWeb' => $request->siteWeb,
                        'Adresse' => $request->adresse,
                        'CodePostal' => $request->codePostal,
                        'Ville' => $request->ville,
                        'Region' => $request->region,
                        'Pays' => $request->pays,
                        'IdentifiantFiscal' => $request->identifiantFiscal,
                        'InfoFiscal1' => $request->infoFiscal1,
                        'InfoFiscal2' => $request->infoFiscal2,
                        'Complement' => $request->complement,
                        'DeviseId' => $request->codeIso,
                        'HaveMCF' => $request->haveMcf,
                    ]);
                    toastr()->success('La  société [' . $request->abrege . '] a été ajoutée avec succès !');
                    return back()->with('successMessage', "La nouvelle société [" . $request->abrege . "] a été ajoutée avec succès !");
                }
            } catch (QueryException $ex) {
                toastr()->error('Échec de création de la société ! ' . $ex->getMessage());
                return back()->with('errorMessage', "Échec de création de la société ! " . $ex->getMessage());
            }
        } else {

            if ($request->logo) {
                $fileName = time() . '.' . $request->logo->extension();
                $request->logo->move(public_path('uploads'), $fileName);
                try {
                    $currentData =  Psociete::all()[0];
                    $currentData->TypeSociete = $request->typeSociete;
                    $currentData->RaisonSociale = $request->raisonSociale;
                    $currentData->Abrege = $request->abrege;
                    $currentData->Capital = $request->capital;
                    $currentData->Activite = $request->activite;
                    $currentData->Contact1 = $request->contact1;
                    $currentData->Contact2 = $request->contact2;
                    $currentData->Fax = $request->fax;
                    $currentData->Email = $request->email;
                    $currentData->SiteWeb = $request->siteWeb;
                    $currentData->Adresse = $request->adresse;
                    $currentData->CodePostal = $request->codePostal;
                    $currentData->Ville = $request->ville;
                    $currentData->Region = $request->region;
                    $currentData->Pays = $request->pays;
                    $currentData->IdentifiantFiscal = $request->identifiantFiscal;
                    $currentData->InfoFiscal1 = $request->infoFiscal1;
                    $currentData->InfoFiscal2 = $request->infoFiscal2;
                    $currentData->Complement = $request->complement;
                    $currentData->logo = $fileName;
                    $currentData->DeviseId = $request->codeIso;
                    $currentData->HaveMCF = $request->haveMcf;
                    $currentData->save();

                    toastr()->success('La modification des informations de la société [' . $request->abrege . '] ont éte avec succès !');
                    return back()->with('successMessage', "La nouvelle société [" . $request->abrege . "] a été ajoutée avec succès !");
                } catch (QueryException $ex) {
                    toastr()->error('Échec de création de la société ! ' . $ex->getMessage());
                    return back()->with('errorMessage', "Échec de modification de la société ! " . $ex->getMessage());
                }
            } else {
                try {
                    $currentData =  Psociete::all()[0];
                    $currentData->TypeSociete = $request->typeSociete;
                    $currentData->RaisonSociale = $request->raisonSociale;
                    $currentData->Abrege = $request->abrege;
                    $currentData->Capital = $request->capital;
                    $currentData->Activite = $request->activite;
                    $currentData->Contact1 = $request->contact1;
                    $currentData->Contact2 = $request->contact2;
                    $currentData->Fax = $request->fax;
                    $currentData->Email = $request->email;
                    $currentData->SiteWeb = $request->siteWeb;
                    $currentData->Adresse = $request->adresse;
                    $currentData->CodePostal = $request->codePostal;
                    $currentData->Ville = $request->ville;
                    $currentData->Region = $request->region;
                    $currentData->Pays = $request->pays;
                    $currentData->IdentifiantFiscal = $request->identifiantFiscal;
                    $currentData->InfoFiscal1 = $request->infoFiscal1;
                    $currentData->InfoFiscal2 = $request->infoFiscal2;
                    $currentData->Complement = $request->complement;
                    $currentData->DeviseId = $request->codeIso;
                    $currentData->HaveMCF = $request->haveMcf;
                    $currentData->save();

                    toastr()->success('La modification des informations de la société [' . $request->abrege . '] ont éte avec succès !');
                    return back()->with('successMessage', "La nouvelle société [" . $request->abrege . "] a été ajoutée avec succès !");
                } catch (QueryException $ex) {
                    toastr()->error('Échec de création de la société ! ' . $ex->getMessage());
                    return back()->with('errorMessage', "Échec de modification de la société ! " . $ex->getMessage());
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Psociete  $psociete
     * @return \Illuminate\Http\Response
     */
    public function show(Psociete $psociete)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Psociete  $psociete
     * @return \Illuminate\Http\Response
     */
    public function edit(Psociete $psociete)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Psociete  $psociete
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* $request->validate([
            'typeSociete2' => 'required|max:255',
            'raisonSociale2' => 'required',
            'abrege2' => 'required|max:255',
            'capital2' => 'required|max:255',
            'activite2' => 'required|max:255',
            'contact12' => 'required|max:255',
            'contact22' => 'required|max:255',
            'fax2' => 'required|max:255',
            'email2' => 'required|max:255',
            'siteWeb2' => 'required|max:255',
            'adresse2' => 'required|max:255',
            'codePostal2' => 'required|max:255',
            'ville2' => 'required|max:255',
            'region2' => 'required|max:255',
            'pays2' => 'required|max:255',
            'identifiantFiscal2' => 'required|max:255',
            'infoFiscal12' => 'required|max:255',
            'infoFiscal22' => 'required|max:255',
            'complement2' => 'required|max:255',
            'codeIso2' => 'required|max:255',
            'haveMcf2' => 'required|max:255',
        ]); */
        //dd($request);

        try {
            $currentData = Psociete::find($id);
            $currentData->TypeSociete = $request->typeSociete2;
            $currentData->RaisonSociale = $request->raisonSociale2;
            $currentData->Abrege = $request->abrege2;
            $currentData->Capital = $request->capital2;
            $currentData->Activite = $request->activite2;
            $currentData->Contact1 = $request->contact12;
            $currentData->Contact2 = $request->contact22;
            $currentData->Fax = $request->fax2;
            $currentData->Email = $request->email2;
            $currentData->SiteWeb = $request->siteWeb2;
            $currentData->Adresse = $request->adresse2;
            $currentData->CodePostal = $request->codePostal2;
            $currentData->Ville = $request->ville2;
            $currentData->Region = $request->region2;
            $currentData->Pays = $request->pays2;
            $currentData->IdentifiantFiscal = $request->identifiantFiscal2;
            $currentData->InfoFiscal1 = $request->infoFiscal12;
            $currentData->InfoFiscal2 = $request->infoFiscal22;
            $currentData->Complement = $request->complement2;
            $currentData->CodeISO = $request->codeIso2;
            $currentData->HaveMCF = $request->haveMcf2;
            $currentData->save();

            toastr()->success('La modification de la société a été effectuée avec succès !');
            return back()->with('successMessage', "La modification de la société a été effectuée avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de modification de la société ! ' . $ex->getMessage());
            return back()->with('errorMessage', "Échec de modification de la société ! " . $ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Psociete  $psociete
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Psociete::destroy($id);
            toastr()->success('La suppression de la société a été effectuée avec succès !');
            return back()->with('successMessage', "La suppression de la société a été effectuée avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de suppression de la société ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de suppression de la société ! " . $ex->getMessage());
        }
    }
}
