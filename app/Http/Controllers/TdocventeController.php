<?php

namespace App\Http\Controllers;

use App\Models\Ptypedoc;
use App\Models\Ttiers;
use App\Models\Tentetedoc;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class TdocventeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('documents/vente.index', [
            'DataList' => Tentetedoc::where('CodeDomaine', 1)->latest()->get(),
            'TiersList' => Ttiers::orderBy('Intitule')->get(),
            'TypeDocList' => Ptypedoc::where('CodeDomaine', 1)->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $nodocument = 'FA' . \Carbon\Carbon::now()->format('Ymdhis');
            Tentetedoc::create([
                'CodeDomaine' => 1,
                'RefDocument' => $request->refDocument,
                'DateDocument' => $request->dateDocument,
                'CodeTiers' => $request->codeTiers,
                'TypeDoc' => $request->typeDoc,
                'NoDocument' => $nodocument
            ]);
            dd(Tentetedoc::where('NoDocument', $nodocument)->get());
        } catch (QueryException $ex) {
            return back()->with('errorMessage', "Échec de création du document de vente ! " . $ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
