<?php

namespace App\Http\Controllers;

use App\Models\Ptaxe;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class PtaxeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('taxe.index', ['TaxeList'=>Ptaxe::all()->sortBy("CodeTaxe")]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'taux' => 'required|numeric',
            'codeTaxe' => 'required|unique:Ptaxe,CodeTaxe',
            'sens' => 'required|numeric'
        ]);

        try {
            Ptaxe::create([
                'CodeTaxe' => $request->codeTaxe,
                'Taux' => $request->taux,
                'Libelle' => $request->libelle,
                'Sens' => $request->sens,
            ]);
            toastr()->success('La taxe [' .$request->codeTaxe .'] a été ajouté avec succès !');
            return back()->with('successMessage', " La taxe [" .$request->codeTaxe ."] a été ajouté avec succès !");

        } catch(QueryException $ex){
            toastr()->error('Échec de la création de la taxe ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de la création de la taxe ! " .$ex->getMessage());

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ptaxe  $ptaxe
     * @return \Illuminate\Http\Response
     */
    public function show(Ptaxe $ptaxe)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ptaxe  $ptaxe
     * @return \Illuminate\Http\Response
     */
    public function edit(Ptaxe $ptaxe)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ptaxe  $ptaxe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'taux2' => 'required|numeric',
            'codeTaxe2' => 'required|unique:Ptaxe,CodeTaxe,'. $id,
            'sens2' => 'required|numeric'
        ]);


        try {
            $currentData = Ptaxe::find($id);
            $currentData->CodeTaxe = $request->codeTaxe2;
            $currentData->Taux = $request->taux2;
            $currentData->Libelle = $request->libelle2;
            $currentData->Sens = $request->sens2;
            $currentData->save();

            toastr()->success('La modification de la taxe a été effectuée avec succès !');
            return back()->with('successMessage', "La modification de la taxe a été effectuée avec succès !");

        } catch(QueryException $ex){
            toastr()->error('Échec de modification de la taxe ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de modification de la taxe ! " .$ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ptaxe  $ptaxe
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Ptaxe::destroy($id);
            toastr()->success('La suppression de la taxe a été effectuée avec succès !');
            return back()->with('successMessage', "La suppression de la taxe a été effectuée avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de suppression de la taxe ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de suppression de la taxe ! " .$ex->getMessage());
        }
    }
}
