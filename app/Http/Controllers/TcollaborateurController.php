<?php

namespace App\Http\Controllers;

use App\Models\Ppays;
use App\Models\Pville;
use Illuminate\Http\Request;
use App\Models\Tcollaborateur;
use Illuminate\Database\QueryException;

class TcollaborateurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('collaborateur.index', [
            'CollaborateurList'=>Tcollaborateur::all(),
            'VilleList'=>Pville::all()->sortBy('nomVille'),
            'PaysList' =>Ppays::all()->sortBy('NomPays')
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'matricule' => 'required|max:20|unique:Tcollaborateur,Matricule,',
        ]);

        try {
            Tcollaborateur::create([
                'Matricule' => $request->matricule,
                'Nom' => $request->nom,
                'Contact1' => $request->contact1,
                'Contact2' => $request->contact2,
                'Email' => $request->email,
                'Adresse' => $request->adresse,
                'Ville' => $request->ville,
                'Pays' => $request->pays,
            ]);
            toastr()->success('Le nouveau collaborateur [' .$request->matricule .'] a été ajoutée avec succès !');
            return back()->with('successMessage', "Le nouveau collaborateur [" .$request->matricule ."] a été ajoutée avec succès !");

        } catch (QueryException $ex) {
            toastr()->error('Échec de création du collaborateur ! ' .$ex->getMessage());
            return back()->with('errorMessage', "Échec de création du collaborateur ! " .$ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tcollaborateur  $tcollaborateur
     * @return \Illuminate\Http\Response
     */
    public function show(Tcollaborateur $tcollaborateur)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tcollaborateur  $tcollaborateur
     * @return \Illuminate\Http\Response
     */
    public function edit(Tcollaborateur $tcollaborateur)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tcollaborateur  $tcollaborateur
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'matricule2' => 'required|max:20|unique:Tcollaborateur,Matricule,'.$id,
        ]);
            //dd($request);

        try {
            $currentData = Tcollaborateur::find($id);
            $currentData->Matricule = $request->matricule2;
            $currentData->Nom = $request->nom2;
            $currentData->Contact1 = $request->contact12;
            $currentData->Contact2 = $request->contact22;
            $currentData->Email = $request->email2;
            $currentData->Adresse = $request->adresse2;
            $currentData->Ville = $request->ville2;
            $currentData->Pays = $request->pays2;
            $currentData->save();

            toastr()->success('La modification du collaborateur a été effectuée avec succès !');
            return back()->with('successMessage', "La modification du collaborateur a été effectuée avec succès !");

        } catch (QueryException $ex) {
            toastr()->error('Échec de modification du collaborateur ! ' .$ex->getMessage());
            return back()->with('errorMessage', "Échec de modification du collaborateur ! " .$ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tcollaborateur  $tcollaborateur
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Tcollaborateur::destroy($id);
            toastr()->success('La suppression du collaborateur a été effectuée avec succès !');
            return back()->with('successMessage', "La suppression du collaborateur a été effectuée avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de suppression du collaborateur ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de suppression du collaborateur ! " .$ex->getMessage());
        }
    }
}
