<?php

namespace App\Http\Controllers;

use App\Models\Ptypedoc;
use App\Models\Tarticle;
use App\Models\Tcollaborateur;
use App\Models\Tmagasin;
use App\Models\Tentetedoc;
use App\Models\Tlignedoc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Database\QueryException;

class TdocstockController extends Controller
{

    function getNoDocument($init)
    {
        $cmd=Tentetedoc::where('NoDocument','like', $init .'%')->orderBy('NoDocument', 'desc')->get();
        //dd(count($cmd));
        if (count($cmd)==0) {
            $myNum=$init ."00001";
        } else {
            //dd($cmd[0]->NoDocument);
            $temp=((int)substr($cmd[0]->NoDocument, strlen($init),5))+1;
            if ($temp<10) {
                $myNum=$init ."0000" .$temp;
            } elseif ($temp<100) {
                $myNum=$init ."000" .$temp;
            } elseif ($temp<1000) {
                $myNum=$init ."00" .$temp;
            } elseif ($temp<10000) {
                $myNum=$init ."00" .$temp;
            } elseif ($temp<100000) {
                $myNum=$init ."0" .$temp;
            } else {
                $myNum=$init .$temp;
            }
        }
        return $myNum;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('documents/stock.index', [
            'DataList'=>Tentetedoc::where('CodeDomaine', 2)->orderBy('NoDocument')->get(),
            'MagasinList'=>Tmagasin::orderBy('isPrincipal', 'desc')->get(),
            'CollaborateurList'=>Tcollaborateur::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->exists('magasinRecepteur'));
        //dd($request);
        if ($request->typeDocument==22) {
            $request->validate([
                'dateDocument' => 'required',
                'magasinEmetteur' => 'required',
                'magasinRecepteur' => 'required',
            ]);
            $Emetteur=explode("|", $request->magasinEmetteur);
            $Recepteur=explode("|", $request->magasinRecepteur);
        } else {
            $request->validate([
                'dateDocument' => 'required',
                'magasinEmetteur' => 'required',
            ]);
            $Emetteur=explode("|", $request->magasinEmetteur);
            $Recepteur=array_fill(0, 2, null);
        }
        //dd($Emetteur, $Recepteur);

        $initial=Ptypedoc::where([
                                    ['CodeDomaine','=', 2],
                                    ['TypeDoc','=',$request->typeDocument]
                                ])->first()->Initial .Carbon::now()->format('y');

        //$toto=$this->getNoDocument($initial);
        //dd('MonToto= ' .$toto);

        try {
            $MyDoc=Tentetedoc::create([
                'CodeDomaine' => 2,
                'TypeDoc' => $request->typeDocument,
                'NoDocument' => $this->getNoDocument($initial),
                'DateDocument' =>  $request->dateDocument,
                'RefDocument' => $request->refDocument,
                'CodeTiers' => 1,
                'Objet1' => $request->objet1,
                'Objet2' => $request->objet2,
                'MagasinEmetteurId' => $Emetteur[0],
                'MagasinEmetteur' => $Emetteur[1],
                'MagasinRecepteurId' => $Recepteur[0],
                'MagasinRecepteur' => $Recepteur[1],
                'CollaborateurId' => $request->collaborateur,
                'Etat'=> 0,
                'Statut'=> 0,
                'isComptabilise'=> 0,
                'TotalHT'=> 0,
                'TotalTTC'=> 0,
            ]);
            //dd($MyDoc->id);
            toastr()->success('[' .$this->getNoDocument($initial) .'] a été créé avec succès !');
            return redirect()->route('documents-stock.show', $MyDoc->id)->with('successMessage', "[" .$this->getNoDocument($initial) ."] a été créé avec succès !");

        } catch (QueryException $ex) {
            toastr()->error('Échec de création de article ! ' .$ex->getMessage());
            return back()->with('errorMessage', "Échec de création du document ! " .$ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('documents/stock.show', [
            'CurrentDoc' => Tentetedoc::findOrFail($id),
            'CurentDetail'=>Tlignedoc::where('EntetedocId', $id)->orderBy('id', 'desc')->get(),
            'MagasinList'=>Tmagasin::orderBy('isPrincipal', 'desc')->get(),
            'CollaborateurList'=>Tcollaborateur::all(),
            'ArticleList'=>DB::table('StockArticle')->get(),
            
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
