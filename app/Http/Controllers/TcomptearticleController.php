<?php

namespace App\Http\Controllers;

use App\Models\Tarticle;
use App\Models\Tcomptegene;
use Illuminate\Http\Request;
use App\Models\Tcatcomptable;
use App\Models\Tcomptearticle;
use Illuminate\Database\QueryException;

class TcomptearticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('comptearticle.index', [
            'ComptearticleList'=>Tcomptearticle::all(),
            'CatcomptableList' =>Tcatcomptable::all()->sortBy('Intitule'),
            'ArticleList' =>Tarticle::all()->sortBy('RefArticle'),
            'ComptegeneList' =>Tcomptegene::all()->sortBy('NoCompte'),
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'noCompte' => 'required',
            'intitule' => 'required',
            'refArticle' => 'required',
            //'noCompte' => 'required',
        ]);

        try {
            Tcomptearticle::create([
                'NoCompte' => $request->noCompte,
                'CatComptableId' => $request->intitule,
                'ArticleId' => $request->refArticle,
                //'CompteGeneId' => $request->noCompte,
            ]);
            toastr()->success('Le nouveau compte article [' .$request->noCompte .'] a été ajoutée avec succès !');
            return back()->with('successMessage', "Le nouveau compte article [" .$request->noCompte ."] a été ajoutée avec succès !");

        } catch (QueryException $ex) {
            toastr()->error('Échec de création du compte article ! ' .$ex->getMessage());
            return back()->with('errorMessage', "Échec de création du compte article ! " .$ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tcomptearticle  $tcomptearticle
     * @return \Illuminate\Http\Response
     */
    public function show(Tcomptearticle $tcomptearticle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tcomptearticle  $tcomptearticle
     * @return \Illuminate\Http\Response
     */
    public function edit(Tcomptearticle $tcomptearticle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tcomptearticle  $tcomptearticle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'noCompte2' => 'required',
            'intitule2' => 'required',
            'refArticle2' => 'required',
            //'noCompte2' => 'required',
        ]);
            //dd($request);

        try {
            $currentData = Tcomptearticle::find($id);
            $currentData->NoCompte = $request->noCompte2;
            $currentData->CatComptableId = $request->intitule2;
            $currentData->ArticleId = $request->refArticle2;
            //$currentData->CompteGeneId = $request->noCompte2;
            $currentData->save();

            toastr()->success('La modification du compte article a été effectuée avec succès !');
            return back()->with('successMessage', "La modification du compte article a été effectuée avec succès !");

        } catch (QueryException $ex) {
            toastr()->error('Échec de modification du compte article ! ' .$ex->getMessage());
            return back()->with('errorMessage', "Échec de modification du compte article ! " .$ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tcomptearticle  $tcomptearticle
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Tcomptearticle::destroy($id);
            toastr()->success('La suppression du compte article a été effectuée avec succès !');
            return back()->with('successMessage', "La suppression du compte article a été effectuée avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de suppression du compte article ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de suppression du compte article ! " .$ex->getMessage());
        }
    }
}
