<?php

namespace App\Http\Controllers;

use App\Models\Pdomaine;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class PdomaineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('domaine.index', ['DomaineList'=>Pdomaine::all()->sortBy("NomDomaine")]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'codeDomaine'=> 'required|numeric|unique:Pdomaine,CodeDomaine,',
            'nomDomaine' => 'required|max:255',
        ]);

        try {
            Pdomaine::create([
                'CodeDomaine' => $request->codeDomaine,
                'NomDomaine' => $request->nomDomaine,
            ]);
            toastr()->success('Le domaine [' .$request->nomDomaine .'] a été ajouté avec succès !');
            return back()->with('successMessage', " Le domaine[" .$request->nomDomaine ."] a été ajouté avec succès !");

        } catch(QueryException $ex){
            toastr()->error('Échec de la création du domaine ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de la création du domaine ! " .$ex->getMessage());

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pdomaine  $pdomaine
     * @return \Illuminate\Http\Response
     */
    public function show(Pdomaine $pdomaine)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pdomaine  $pdomaine
     * @return \Illuminate\Http\Response
     */
    public function edit(Pdomaine $pdomaine)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pdomaine  $pdomaine
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'codeDomaine2'=> 'required|numeric|unique:Pdomaine,CodeDomaine,'.$id,
            'nomDomaine2' => 'required|max:255',
        ]);


        try {
            $currentData = Pdomaine::find($id);
            $currentData->CodeDomaine = $request->codeDomaine2;
            $currentData->NomDomaine = $request->nomDomaine2;
            $currentData->save();

            toastr()->success('La modification du domaine a été effectuée avec succès !');
            return back()->with('successMessage', "La modification du domaine a été effectuée avec succès !");

        } catch(QueryException $ex){
            toastr()->error('Échec de modification du domaine ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de modification du domaine ! " .$ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pdomaine  $pdomaine
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Pdomaine::destroy($id);
            toastr()->success('La suppression du domaine a été effectuée avec succès !');
            return back()->with('successMessage', "La suppression du domaine a été effectuée avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de suppression du domaine ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de suppression du domaine ! " .$ex->getMessage());
        }
    }
}
