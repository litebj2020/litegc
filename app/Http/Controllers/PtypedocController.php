<?php

namespace App\Http\Controllers;

use App\Models\Pdomaine;
use App\Models\Ptypedoc;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class PtypedocController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd(Ptypedoc::all());
        return view('typedoc.index', [
            'TypedocList'=>Ptypedoc::all(),
            'DomaineList' =>Pdomaine::all()->sortBy('NomDomaine')
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nomDomaine' => 'required',
            'typeDoc' => 'required|numeric',
            'libelleType' => 'required|max:60',
            'initial' => 'required|max:5',
        ]);
        //dd($request);
        $domaine= explode("|", $request->nomDomaine);

        try {
            Ptypedoc::create([
                'DomaineId' => (int)$domaine[0],
                'CodeDomaine' => (int)$domaine[1],
                'TypeDoc' => $request->typeDoc,
                'LibelleType' => $request->libelleType,
                'Initial' => $request->initial,
            ]);
            toastr()->success('Le type du document [' .$request->typeDoc .'] a été ajoutée avec succès !');
            return back()->with('successMessage', "Le type du document [" .$request->typeDoc ."] a été ajoutée avec succès !");

        } catch (QueryException $ex) {
            toastr()->error('Échec de création du type du document! ' .$ex->getMessage());
            return back()->with('errorMessage', "Échec de création du type du document ! " .$ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ptypedoc  $ptypedoc
     * @return \Illuminate\Http\Response
     */
    public function show(Ptypedoc $ptypedoc)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ptypedoc  $ptypedoc
     * @return \Illuminate\Http\Response
     */
    public function edit(Ptypedoc $ptypedoc)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ptypedoc  $ptypedoc
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $request->validate([
            'nomDomaine2' => 'required',
            'typeDoc2' => 'required|numeric',
            'libelleType2' => 'required|max:60',
            'initial2' => 'required|max:5',
        ]);
        $domaine= explode("|", $request->nomDomaine2);
            //dd($request, (int)$domaine[0], (int)$domaine[1]);

        try {
            $currentData = Ptypedoc::find($id);
            $currentData->DomaineId = (int)$domaine[0];
            $currentData->CodeDomaine = (int)$domaine[1];
            $currentData->TypeDoc = $request->typeDoc2;
            $currentData->LibelleType = $request->libelleType2;
            $currentData->Initial = $request->initial2;
            $currentData->save();

            toastr()->success('La modification du type document a été effectuée avec succès !');
            return back()->with('successMessage', "La modification du type document a été effectuée avec succès !");

        } catch (QueryException $ex) {
            toastr()->error('Échec de modification du type document ! ' .$ex->getMessage());
            return back()->with('errorMessage', "Échec de modification du type document ! " .$ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ptypedoc  $ptypedoc
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Ptypedoc::destroy($id);
            toastr()->success('La suppression du type document a été effectuée avec succès !');
            return back()->with('successMessage', "La suppression du type document a été effectuée avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de suppression du type document ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de suppression du type document ! " .$ex->getMessage());
        }
    }
}
