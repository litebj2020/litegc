<?php

namespace App\Http\Controllers;

use App\Models\Tjournal;
use Illuminate\Http\Request;
use App\Models\Pmodereglement;
use Illuminate\Database\QueryException;

class PmodereglementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modereglement.index', [
            'ModereglementList'=>Pmodereglement::all(),
            'JournalList' =>Tjournal::where('TypeJournal', 2)
                                    ->orderBy('CodeJournal')
                                    ->get()
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'codeReglement' => 'required',
            'codeJournal' => 'required',
        ]);

        try {
            Pmodereglement::create([
                'LibelleReglement' => $request->libelleReglement,
                'CodeReglement' => $request->codeReglement,
                'JournalId' => $request->codeJournal,
            ]);
            toastr()->success('Le nouveau mode de reglement [' .$request->codeReglement .'] a été ajoutée avec succès !');
            return back()->with('successMessage', "Le nouveau mode de reglement [" .$request->codeReglement ."] a été ajoutée avec succès !");

        } catch (QueryException $ex) {
            toastr()->error('Échec de création du mode de reglement ! ' .$ex->getMessage());
            return back()->with('errorMessage', "Échec de création du mode de reglement ! " .$ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pmodereglement  $pmodereglement
     * @return \Illuminate\Http\Response
     */
    public function show(Pmodereglement $pmodereglement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pmodereglement  $pmodereglement
     * @return \Illuminate\Http\Response
     */
    public function edit(Pmodereglement $pmodereglement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pmodereglement  $pmodereglement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'codeReglement2' => 'required:Pmodereglement,CodeReglement,'. $id,
            'codeJournal2' => 'required',
        ]);
            //dd($request);

        try {
            $currentData = Pmodereglement::find($id);
            $currentData->LibelleReglement = $request->libelleReglement2;
            $currentData->CodeReglement = $request->codeReglement2;
            $currentData->JournalId = $request->codeJournal2;
            $currentData->save();

            toastr()->success('La modification du mode de reglement  a été effectuée avec succès !');
            return back()->with('successMessage', "La modification du mode de reglement  a été effectuée avec succès !");

        } catch (QueryException $ex) {
            toastr()->error('Échec de modification du mode de reglement  ! ' .$ex->getMessage());
            return back()->with('errorMessage', "Échec de modification du mode de reglement  ! " .$ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pmodereglement  $pmodereglement
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Pmodereglement::destroy($id);
            toastr()->success('La suppression du mode de reglement a été effectuée avec succès !');
            return back()->with('successMessage', "La suppression du mode de reglement a été effectuée avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de suppression du mode de reglement  ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de suppression du mode de reglement  ! " .$ex->getMessage());
        }
    }
}
