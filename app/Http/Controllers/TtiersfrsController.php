<?php

namespace App\Http\Controllers;

use App\Models\Ttiers;
use App\Models\Ppays;
use App\Models\Pville;
use App\Models\Tcomptegene;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class TtiersfrsController extends Controller
{

    /* public function showedLetters($name)
    {
        $temp=explode(" ", trim($name));

        if (count($temp)>1) {
            $letters = substr($temp[0],0,1) .substr($temp[1],0,1);
        } else {
            $letters = substr($temp[0],0,2);
        }
        return strtoupper($letters);
    } */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('fournisseur.index', [
            'DataList'=>Ttiers::where('TypeTiers', 0)->orderBy('CodeTiers')->get(),
            'CompteList'=>Tcomptegene::all()->sortBy('NoCompte'),
            'VilleList'=>Pville::all()->sortBy('NomVille'),
            'PaysList' =>Ppays::all()->sortBy('NomPays'),

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'codeTiers' => 'required|max:15|unique:Ttiers,CodeTiers,',
            'intitule' => 'required',
            'noCompte' => 'required',
        ]);

        try {
            Ttiers::create([
                'CodeTiers' => $request->codeTiers,
                'Intitule' => $request->intitule,
                'TypeTiers' => 0,
                'NoCompte' => $request->noCompte,
                'IdFiscal' => $request->idFiscal,
                'InfoFiscal1' => $request->infoFiscal1,
                'InfoFiscal2' => $request->infoFiscal2,
                'Contact1' => $request->contact1,
                'Contact2' => $request->contact2,
                'Email' => $request->email,
                'Adresse' => $request->adresse,
                'CodePostal' => $request->codePostal,
                'Region' => $request->region,
                'Ville' => $request->ville,
                'Pays' => $request->pays,
            ]);
            toastr()->success('Le nouveau tiers [' .$request->codeTiers .'] a été ajouté avec succès !');
            return back()->with('successMessage', "Le nouveau tiers [" .$request->codeTiers ."] a été ajouté avec succès !");

        } catch (QueryException $ex) {
            toastr()->error('Échec de création du tiers ! ' .$ex->getMessage());
            return back()->with('errorMessage', "Échec de création du tiers ! " .$ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('fournisseur.show', ['CurrentData' => Ttiers::findOrFail($id),
                                            'CompteList'=>Tcomptegene::all()->sortBy('NoCompte'),
                                            'VilleList'=>Pville::all()->sortBy('NomVille'),
                                            'PaysList' =>Ppays::all()->sortBy('NomPays')
                                        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'intitule' => 'required',
            'noCompte' => 'required',
        ]);
            //dd($request->idFiscal);

        try {
            $currentData = Ttiers::find($id);
            $currentData->Intitule = $request->intitule;
            $currentData->NoCompte = $request->noCompte;
            $currentData->IdFiscal = $request->idFiscal;
            $currentData->InfoFiscal1 = $request->infoFiscal1;
            $currentData->InfoFiscal2 = $request->infoFiscal2;
            $currentData->Contact1 = $request->contact1;
            $currentData->Contact2 = $request->contact2;
            $currentData->Email = $request->email;
            $currentData->Adresse = $request->adresse;
            $currentData->CodePostal = $request->codePostal;
            $currentData->Region = $request->region;
            $currentData->Ville = $request->ville;
            $currentData->Pays = $request->pays;
            $currentData->save();

            toastr()->success('La modification du tiers a été effectuée avec succès !');
            return back()->with('successMessage', "La modification du tiers a été effectuée avec succès !");

        } catch (QueryException $ex) {
            toastr()->error('Échec de modification du tiers ! ' .$ex->getMessage());
            return back()->with('errorMessage', "Échec de modification du tiers ! " .$ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Ttiers::destroy($id);
            toastr()->success('La suppression du tiers a été effectuée avec succès !');
            return redirect()->route('fournisseur.index')->with('successMessage', "La suppression du tiers a été effectuée avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de suppression du tiers ! ', $ex->getMessage());
            return redirect()->route('fournisseur.index')->with('errorMessage', "Échec de suppression du tiers ! " .$ex->getMessage());
        }
    }


    public static function countnobrefrs()
    {
        $nbre = Ttiers::where('TypeTiers', 0)->orderBy('CodeTiers')->get();
        $nombre = count($nbre);
        return $nombre ;
    }
}
