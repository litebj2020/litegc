<?php

namespace App\Http\Controllers;

use App\Models\Psuivistock;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class PsuivistockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('suivistock.index', ['SuivistockList' => Psuivistock::all()->sortBy("LibelleSuivi")]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'codeSuivi' => 'required|numeric|unique:Psuivistock,CodeSuivi',
            'libelleSuivi' => 'required|max:255|unique:Psuivistock,LibelleSuivi',
        ]);

        try {
            Psuivistock::create([
                'CodeSuivi' => $request->codeSuivi,
                'LibelleSuivi' => $request->libelleSuivi,
            ]);
            toastr()->success('Le suivi stock [' . $request->libelleSuivi . '] a été ajouté avec succès !');
            return back()->with('successMessage', " Le suivi stock [" . $request->libelleSuivi . "] a été ajouté avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de la création du suivi stock ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de la création du suivi stock ! " . $ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Psuivistock  $psuivistock
     * @return \Illuminate\Http\Response
     */
    public function show(Psuivistock $psuivistock)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Psuivistock  $psuivistock
     * @return \Illuminate\Http\Response
     */
    public function edit(Psuivistock $psuivistock)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Psuivistock  $psuivistock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'codeSuivi2' => 'required|max:1|numeric|unique:Psuivistock,LibelleSuivi,',
            'libelleSuivi2' => 'required|max:255|unique:Psuivistock,LibelleSuivi,'. $id,
        ]);

        //dd($request);

        try {
            $currentData = Psuivistock::find($id);
            $currentData->LibelleSuivi = $request->libelleSuivi2;
            $currentData->CodeSuivi = $request->codeSuivi2;
            $currentData->save();

            toastr()->success('La modification du suivi stock a été effectuée avec succès !');
            return back()->with('successMessage', "La modification du suivi stock a été effectuée avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de modification du suivi stock ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de modification du suivi stock ! " . $ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Psuivistock  $psuivistock
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Psuivistock::destroy($id);
            toastr()->success('La suppression du suivi stock a été effectuée avec succès !');
            return back()->with('successMessage', "La suppression du suivi stock a été effectuée avec succès !");
        } catch (QueryException $ex) {
            toastr()->error('Échec de suppression du suivi stock ! ', $ex->getMessage());
            return back()->with('errorMessage', "Échec de suppression du suivi stock ! " . $ex->getMessage());
        }
    }
}
