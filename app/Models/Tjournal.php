<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tjournal extends Model
{
    use HasFactory;
    protected $table= 'Tjournal';
    //protected $primaryKey = 'idParticipant';
    protected $fillable = [
        'CodeJournal',
        'LibelleJournal',
        'TypeJournal',
        'NoCompte',
    ];


    public function getDateFormat(){
        return 'Y-d-m H:i:s.v';
    }
}
