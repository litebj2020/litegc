<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tcatcomptable extends Model
{
    use HasFactory;

    protected $table= 'Tcatcomptable';
    //protected $primaryKey = 'idParticipant';
    protected $fillable = [
        'Intitule', 'DomaineId'

    ];

    public function pdomaines()
    {
        return $this->hasOne(Pdomaine::class);
    }

    public function getDateFormat(){
        return 'Y-d-m H:i:s.v';
    }

}
