<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ptypedoc extends Model
{
    use HasFactory;
    protected $table= 'Ptypedoc';

    protected $fillable = [
        'DomaineId',
        'CodeDomaine',
        'TypeDoc',
        'LibelleType',
        'Initial',

    ];

/*     public function pdomaines()
    {
        return $this->hasOne(Pdomaine::class);
    } */

    public function pdomaines()
    {
        return $this->belongsTo(Pdomaine::class, 'DomaineId');
    }

    public function getDateFormat(){
        return 'Y-d-m H:i:s.v';
    }
}
