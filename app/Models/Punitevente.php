<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Punitevente extends Model
{
    use HasFactory;
    protected $table= 'Punitevente';

    protected $fillable = [
        'LibelleUnite',
    ];

    public function getDateFormat(){
        return 'Y-d-m H:i:s.v';
    }
}
