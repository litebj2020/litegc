<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pdomaine extends Model
{
    use HasFactory;
    protected $table= 'Pdomaine';

    protected $fillable = [
        'CodeDomaine','NomDomaine',
    ];

    public function getDateFormat(){
        return 'Y-d-m H:i:s.v';
    }
}
