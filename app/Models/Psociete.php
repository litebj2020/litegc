<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Psociete extends Model
{
    use HasFactory;
    protected $table = 'Psociete';
    //protected $primaryKey = 'idParticipant';
    protected $fillable = [
        'TypeSociete',
        'RaisonSociale',
        'Abrege',
        'Capital',
        'Activite',
        'Contact1',
        'Contact2',
        'Fax',
        'Email',
        'SiteWeb',
        'Adresse',
        'CodePostal',
        'Ville',
        'Region',
        'Pays',
        'IdentifiantFiscal',
        'InfoFiscal1',
        'InfoFiscal2',
        'Complement',
        'Logo',
        'DeviseId',
        'HaveMCF',
    ];

    public function pdevises()
    {
        return $this->hasOne(Pdevise::class);
    }

    public function getDateFormat()
    {
        return 'Y-d-m H:i:s.v';
    }
}
