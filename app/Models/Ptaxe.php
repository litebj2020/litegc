<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ptaxe extends Model
{
    use HasFactory;
    protected $table= 'Ptaxe';

    protected $fillable = [
        'CodeTaxe',
        'Libelle',
        'Taux',
        'Sens',

    ];

    public function getDateFormat(){
        return 'Y-d-m H:i:s.v';
    }
}
