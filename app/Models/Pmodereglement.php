<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pmodereglement extends Model
{
    use HasFactory;

    protected $table= 'Pmodereglement';

    protected $fillable = [
        'LibelleReglement',
        'CodeReglement',
        'JournalId'
    ];



    public function tjournals()
    {
        return $this->belongsTo(Tjournal::class, 'JournalId');
    }

    public function getDateFormat(){
        return 'Y-d-m H:i:s.v';
    }
}
