<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tmagasin extends Model
{
    use HasFactory;
    protected $table= 'Tmagasin';
    //protected $primaryKey = 'idParticipant';
    protected $fillable = [
        'id',
        'Intitule',
        'CodeMagasin',
        'Contact',
        'Adresse',
        'Ville',
        'Pays',
        'isPrincipal',
        'CollaborateurId',
        'Responsable'

    ];

    public function tcollaborateurs()
    {
        return $this->belongsTo(Tcollaborateur::class,'CollaborateurId');
    }

    public function getDateFormat(){
        return 'Y-d-m H:i:s.v';
    }
}
