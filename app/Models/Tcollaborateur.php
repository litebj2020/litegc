<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tcollaborateur extends Model
{
    use HasFactory;

    protected $table= 'Tcollaborateur';
    //protected $primaryKey = 'idParticipant';
    protected $fillable = [
        'Matricule',
        'Nom',
        'Contact1',
        'Contact2',
        'Email',
        'Adresse',
        'Ville',
        'Pays'
        //'ProfilId',

    ];

    public function userss()
    {
        return $this->hasOne(User::class);
    }

    public function getDateFormat(){
        return 'Y-d-m H:i:s.v';
    }
}
