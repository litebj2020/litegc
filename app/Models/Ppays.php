<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ppays extends Model
{
    use HasFactory;
    protected $table= 'Ppays';

    protected $fillable = [
        'CodePays',
        'NomPays',
    ];

    public function getDateFormat(){
        return 'Y-d-m H:i:s.v';
    }
}
