<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ttiers extends Model
{
    use HasFactory;

    protected $table= 'Ttiers';

    protected $fillable = [
        'CodeTiers',
        'Intitule',
        'TypeTiers',
        'NoCompte',
        'Activite',
        'Qualite',
        'Categorie',
        'IdFiscal',
        'InfoFiscal1',
        'InfoFiscal2',
        'Contact1',
        'Contact2',
        'Email',
        'SiteWeb',
        'Adresse',
        'CodePostal',
        'Region',
        'Ville',
        'Pays'

    ];

    public function getDateFormat(){
        return 'Y-d-m H:i:s.v';
    }
}
