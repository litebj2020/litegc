<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tlottempon extends Model
{
    use HasFactory;

    protected $table = 'Tlottempon';

    protected $fillable = [
        'id',
        'LigneId',
        'ArticleId',
        'QteTotal',
        'NoLot',
        'NoSerie',
        'QteEntree',
        'DateFabrication',
        'DatePeremption',
        'ComplementSL',
        'EnteteId'
    ];
    public function getDateFormat()
    {
        return 'd/m/Y';
    }
}
