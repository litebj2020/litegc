<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Psuivistock extends Model
{
    use HasFactory;
    protected $table= 'Psuivistock';

    protected $fillable = [
        'CodeSuivi',
        'LibelleSuivi',
    ];

    public function getDateFormat(){
        return 'Y-d-m H:i:s.v';
    }
}
