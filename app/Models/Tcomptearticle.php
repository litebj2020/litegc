<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tcomptearticle extends Model
{
    use HasFactory;

    protected $table= 'Tcomptearticle';
    //protected $primaryKey = 'idParticipant';
    protected $fillable = [
        'CatComptableId',
        'ArticleId',
        'CompteGeneId',
        'NoCompte'

    ];



    public function tarticles()
    {
        return $this->hasMany(Tarticle::class);
    }

    public function tcomptegenes()
    {
        return $this->hasMany(Tcomptegene::class);
    }


    public function tcatcompables()
    {
        return $this->belongsTo(Tcatcomptable::class, 'CatComptableId');
    }

    public function getDateFormat(){
        return 'Y-d-m H:i:s.v';
    }

}
