<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tlignedoc extends Model
{
    use HasFactory;

    protected $table = 'Tlignedoc';
    protected $fillable = [
        'EntetedocId',
        'CodeDomaine',
        'TypeDoc',
        'NoDocument',
        'DateDocument',
        'ArticleId',
        'RefArticle',
        'Designation',
        'Complement',
        'Quantite',
        'PrixUnitHT',
        'PrixUnitTTC',
        'Taxation',
        'Taxe1',
        'Taxe2',
        'Taxe3',
        'Taxe4',
        'Taxe5',
        'Remise',
        'MontantHT',
        'MontantTTC',
        'MontantNET',
        'MagasinId',
        'CodeMagasin',
        'MvtStock',
        'PoidsBrut',
        'PoidsNet',
        'QteStock',
        'PrixRunit',
        'CMUP',
        'Frais',
        'NoSerie',
        'NoLot',
        'ComplementSL',
        'DateFabrication',
        'DatePeremption',
        'NoDoc01',
        'NoDoc02',
        'NoDoc03',
        'QteDoc01',
        'QteDoc02',
        'QteDoc03',
        'DateDoc01',
        'DateDoc02',
        'DateDoc03',
    ];

    public function tentetedocs()
    {
        return $this->belongsTo(Tentetedoc::class, 'EntetedocId');
    }

    public function tarticles()
    {
        return $this->belongsTo(Tarticle::class, 'ArticleId');
    }
    public function tmagasins()
    {
        return $this->belongsTo(Tmagasin::class, 'MagasinId');
    }

    public function getDateFormat()
    {
        return 'Y-d-m H:i:s.v';
    }
}
