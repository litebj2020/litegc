<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tfamilleart extends Model
{
    use HasFactory;
    protected $table = 'Tfamilleart';
    //protected $primaryKey = 'idParticipant';
    protected $fillable = [
        'CodeFamille',
        'LibelleFamille',
        'SuiviId',
        'CodeSuivi',
        'StockNegatif',
        'UniteId',
        'CptVente',
        'CptAchat',
        'CptStock',
    ];

    public function psuivistocks()
    {
        return $this->belongsTo(Psuivistock::class, 'SuiviId');
    }

    public function puniteventes()
    {
        return $this->belongsTo(Punitevente::class, 'UniteId');
    }

    public function getDateFormat()
    {
        return 'Y-d-m H:i:s.v';
    }
}
