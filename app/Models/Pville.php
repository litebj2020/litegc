<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pville extends Model
{
    use HasFactory;

    protected $table= 'Pville';

    protected $fillable = [
        'NomVille',
        'PaysId',
    ];

    public function ppayss()
    {
        return $this->belongsTo(Ppays::class, 'PaysId');
    }

    public function getDateFormat(){
        return 'Y-d-m H:i:s.v';
    }
}
