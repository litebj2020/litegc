<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tcomptegene extends Model
{
    use HasFactory;

    protected $table= 'Tcomptegene';
    //protected $primaryKey = 'idParticipant';
    protected $fillable = [
        'NoCompte',
        'IntituleCompte'

    ];



    public function getDateFormat(){
        return 'Y-d-m H:i:s.v';
    }
}
