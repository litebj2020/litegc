<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tarticle extends Model
{
    use HasFactory;
    protected $table= 'Tarticle';

    protected $fillable = [
        'RefArticle',
        'Designation',
        'FamilleId',
        'CptVente',
        'CptAchat',
        'CptStock',
        'SuiviId',
        'CodeSuivi',
        'Type',
        'Marque',
        'Nature',
        'CodeBarre',
        'Actif',
        'StockNegatif',
        'UniteId',
        'PrixVenteHT',
        'PrixVenteTTC',
        'PrixAchatHT',
        'PrixAchatTTC',
        'DernierAchat',
        'PoidsBrut',
        'PoidsNet',
    ];

    public function tfamillearts()
    {
        return $this->belongsTo(Tfamilleart::class, 'FamilleId');
    }

    public function psuivistocks()
    {
        return $this->belongsTo(Psuivistock::class, 'SuiviId');
    }

    public function puniteventes()
    {
        return $this->belongsTo(Punitevente::class, 'UniteId');
    }

    public function getDateFormat(){
        return 'Y-d-m H:i:s.v';
    }
}
