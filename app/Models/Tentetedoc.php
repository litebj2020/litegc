<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tentetedoc extends Model
{
    use HasFactory;

    protected $table = 'Tentetedoc';
    //protected $primaryKey = 'idParticipant';
    protected $fillable = [
        'CodeDomaine',
        'TypeDoc',
        'NoDocument',
        'DateDocument',
        'RefDocument',
        'CodeTiers',
        'CollaborateurId',
        'NoDocOriginal',
        'Objet1',
        'Objet2',
        'CatComptableId',
        'MagasinEmetteurId',
        'MagasinEmetteur',
        'MagasinRecepteurId',
        'MagasinRecepteur',
        'Etat',
        'Statut',
        'isValide',
        'isComptabilise',
        'isNormalise',
        'TauxTaxeAIB',
        'TotalHT',
        'TotalTTC',
        'MontantRegle',
        'ModeReglementId',
        'ResteApayer',
        'NoDoc01',
        'NoDoc02',
        'NoDoc03',
        'DateDoc01',
        'DateDoc02',
        'DateDoc03'
    ];

    public function pdomaines()
    {
        return $this->hasOne(Pdomaine::class);
    }

    public function tcollaborateurs()
    {
        return $this->hasOne(Tcollaborateur::class);
    }

    public function pmodereglements()
    {
        return $this->hasOne(Pmodereglement::class);
    }

    public function getDateFormat()
    {
        return 'Y-d-m H:i:s.v';
    }
}
