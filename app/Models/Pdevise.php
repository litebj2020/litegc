<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pdevise extends Model
{
    use HasFactory;
    protected $table= 'Pdevise';

    protected $fillable = [
        'CodeISO',
        'Designation',
        'Symbole',
        'Principal',
    ];

    public function getDateFormat(){
        return 'Y-d-m H:i:s.v';
    }
}
