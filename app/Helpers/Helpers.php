<?php

use App\Models\Tentetedoc;

function initials($name)
{
    $temp=explode(" ", trim($name));
    if (count($temp)>1) {
        $letters = substr($temp[0],0,1) .substr($temp[1],0,1);
    } else {
        $letters = substr($temp[0],0,2);
    }
    return strtoupper($letters);
}

