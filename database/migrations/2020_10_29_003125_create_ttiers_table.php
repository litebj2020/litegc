<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTtiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Ttiers', function (Blueprint $table) {
            $table->id();
            $table->string('CodeTiers')->unique();
            $table->string('Intitule');
            $table->integer('TypeTiers');
            $table->integer('NoCompte');
            $table->string('Activite')->nullable();
            $table->string('Qualite')->nullable();
            $table->string('Categorie')->nullable();
            $table->string('IdFiscal')->nullable();
            $table->string('InfoFiscal1')->nullable();
            $table->string('InfoFiscal2')->nullable();
            $table->string('Contact1')->nullable();
            $table->string('Contact2')->nullable();
            $table->string('Email')->nullable();
            $table->string('SiteWeb')->nullable();
            $table->string('Adresse')->nullable();
            $table->string('CodePostal')->nullable();
            $table->string('Region')->nullable();
            $table->string('Ville')->nullable();
            $table->string('Pays')->nullable();
            $table->string('createdby')->nullable();
            $table->string('updatedby')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Ttiers');
    }
}
