<?php

use App\Models\Tmagasin;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTentetedocsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Tentetedoc', function (Blueprint $table) {
            $table->id();
            $table->integer('CodeDomaine');
            $table->integer('TypeDoc');
            $table->string('NoDocument')->unique();
            $table->date('DateDocument');
            $table->string('RefDocument')->nullable();
            $table->string('CodeTiers');
            $table->foreignId('MagasinEmetteurId')->nullable()->constrained('Tmagasin');
            $table->string('MagasinEmetteur')->nullable();
            $table->foreignId('MagasinRecepteurId')->nullable()->constrained('Tmagasin');
            $table->string('MagasinRecepteur')->nullable();
            $table->foreignId('CollaborateurId')->nullable()->constrained('Tcollaborateur');
            $table->string('NoDocOriginal')->nullable();
            $table->string('Objet1')->nullable();
            $table->string('Objet2')->nullable();
            $table->foreignId('CatComptableId')->nullable()->constrained('Tcatcomptable');
            $table->integer('Etat')->nullable();
            $table->integer('Statut')->nullable();
            $table->integer('isValide')->nullable();
            $table->integer('isComptabilise')->nullable();
            $table->integer('isNormalise')->nullable();
            $table->integer('TauxTaxeAIB')->nullable();
            $table->decimal('TotalHT', 12, 0)->nullable();
            $table->decimal('TotalTTC', 12, 0)->nullable();
            $table->decimal('MontantRegle', 12, 0)->nullable();
            $table->integer('ModeReglementId')->nullable();
            $table->decimal('ResteApayer', 12, 0)->nullable();
            $table->string('NoDoc01')->nullable();
            $table->string('NoDoc02')->nullable();
            $table->string('NoDoc03')->nullable();
            $table->date('DateDoc01')->nullable();
            $table->date('DateDoc02')->nullable();
            $table->date('DateDoc03')->nullable();
            $table->string('createdby')->nullable();
            $table->string('updatedby')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Tentetedoc');
    }
}
