<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTmagasinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Tmagasin', function (Blueprint $table) {
            $table->id();
            $table->string('Intitule');
            $table->string('CodeMagasin')->unique();
            $table->string('Contact')->nullable();
            $table->string('Adresse')->nullable();
            $table->string('Ville')->nullable();
            $table->string('Pays')->nullable();
            $table->integer('isPrincipal');
            $table->foreignId('CollaborateurId')->nullable()->constrained('Tcollaborateur');
            $table->string('Responsable')->nullable();

            $table->string('createdby')->nullable();
            $table->string('updatedby')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Tmagasin');
    }
}
