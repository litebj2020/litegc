<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTjournalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Tjournal', function (Blueprint $table) {
            $table->id();
            $table->string('CodeJournal')->unique();
            $table->string('LibelleJournal')->nullable();
            $table->integer('TypeJournal');//0=Achat; 1=Vente; 2=Trésorerie; 3=Opérations diverses; 4-Situation
            $table->string('NoCompte')->nullable();
            $table->string('createdby')->nullable();
            $table->string('updatedby')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Tjournal');
    }
}
