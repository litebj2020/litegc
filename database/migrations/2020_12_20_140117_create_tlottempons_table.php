<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTlottemponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Tlottempon', function (Blueprint $table) {
            $table->id();
            $table->integer('LigneId')->nullable();
            $table->integer('EnteteId')->nullable();
            $table->integer('ArticleId')->nullable();
            $table->float('QteTotal')->nullable();
            $table->string('NoLot')->nullable();
            $table->string('NoSerie')->nullable();
            $table->float('QteEntree')->nullable();
            $table->date('DateFabrication')->nullable();
            $table->date('DatePeremption')->nullable();
            $table->string('ComplementSL')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Tlottempon');
    }
}
