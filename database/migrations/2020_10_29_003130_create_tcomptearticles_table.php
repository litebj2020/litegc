<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTcomptearticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Tcomptearticle', function (Blueprint $table) {
            $table->id();
            $table->foreignId('CatComptableId')->constrained('Tcatcomptable')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreignId('ArticleId')->constrained('Tarticle')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            /* $table->foreignId('CompteGeneId')->constrained('Tcomptegene')
                ->onDelete('cascade')
                ->onUpdate('cascade'); */

            $table->string('NoCompte');

            $table->string('createdby')->nullable();
            $table->string('updatedby')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Tcomptearticle');
    }
}
