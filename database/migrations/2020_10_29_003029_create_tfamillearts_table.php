<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTfamilleartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Tfamilleart', function (Blueprint $table) {
            $table->id();

            $table->string('CodeFamille')->unique();
            $table->string('LibelleFamille');
            $table->foreignId('SuiviId')->constrained('Psuivistock');
            $table->integer('CodeSuivi');
            $table->integer('StockNegatif');
            $table->foreignId('UniteId')->constrained('Punitevente');
            $table->string('CptVente')->nullable();
            $table->string('CptAchat')->nullable();
            $table->string('CptStock')->nullable();

            $table->string('createdby')->nullable();
            $table->string('updatedby')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Tfamilleart');
    }
}
