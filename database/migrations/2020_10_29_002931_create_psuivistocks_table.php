<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePsuivistocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Psuivistock', function (Blueprint $table) {
            $table->id();
            $table->integer('CodeSuivi');
            $table->string('LibelleSuivi');
            $table->string('createdby')->nullable();
            $table->string('updatedby')->nullable();
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Psuivistock');
    }
}
