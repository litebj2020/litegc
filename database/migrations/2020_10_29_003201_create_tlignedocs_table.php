<?php

use App\Models\Tmagasin;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;

class CreateTlignedocsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Tlignedoc', function (Blueprint $table) {
            $table->id();
            $table->foreignId('EntetedocId')->constrained('Tentetedoc')->onDelete('cascade');;
            $table->integer('CodeDomaine');
            $table->integer('TypeDoc');
            $table->string('NoDocument');
            $table->dateTime('DateDocument');
            $table->foreignId('ArticleId')->nullable()->constrained('Tarticle');
            $table->string('RefArticle')->nullable();
            $table->string('Designation', 120)->nullable();
            $table->string('Complement')->nullable();
            $table->float('Quantite')->default(1);
            $table->decimal('PrixUnitHT', 12, 4)->default(0);
            $table->decimal('PrixUnitTTC', 12, 4)->default(0);
            $table->string('Taxation', 15)->nullable();
            $table->foreignId('Taxe1')->nullable()->constrained('Ptaxe');
            $table->foreignId('Taxe2')->nullable()->constrained('Ptaxe');
            $table->foreignId('Taxe3')->nullable()->constrained('Ptaxe');
            $table->foreignId('Taxe4')->nullable()->constrained('Ptaxe');
            $table->foreignId('Taxe5')->nullable()->constrained('Ptaxe');
            $table->float('Remise')->nullable();
            $table->decimal('MontantHT', 12, 0)->default(0);
            $table->decimal('MontantTTC', 12, 0)->default(0);
            $table->decimal('MontantNET', 12, 0)->default(0);
            $table->foreignId('MagasinId')->nullable()->constrained('Tmagasin');
            $table->string('CodeMagasin', 15)->nullable();
            $table->integer('MvtStock')->nullable();
            $table->float('PoidsBrut')->default(0);
            $table->float('PoidsNet')->default(0);
            $table->float('QteStock')->default(0);
            $table->decimal('ValeurStock', 12, 4)->default(0);
            $table->decimal('PrixRunit', 12, 4)->default(0);
            $table->decimal('CMUP', 12, 4)->default(0);
            $table->decimal('Frais', 12, 4)->default(0);
            $table->string('NoSerie', 60)->nullable();
            $table->string('NoLot', 60)->nullable();
            $table->string('ComplementSL', 120)->nullable();
            $table->date('DateFabrication')->default(Carbon::createFromFormat('d/m/Y H:i:s', '01/01/1800 00:00:00')) ;
            $table->date('DatePeremption')->default(Carbon::createFromFormat('d/m/Y H:i:s', '01/01/1800 00:00:00')) ;
            $table->string('NoDoc01', 15)->nullable();
            $table->string('NoDoc02', 15)->nullable();
            $table->string('NoDoc03', 15)->nullable();
            $table->float('QteDoc01')->default(0);
            $table->float('QteDoc02')->default(0);
            $table->float('QteDoc03')->default(0);
            $table->date('DateDoc01')->default(Carbon::createFromFormat('d/m/Y H:i:s', '01/01/1800 00:00:00'));
            $table->date('DateDoc02')->default(Carbon::createFromFormat('d/m/Y H:i:s', '01/01/1800 00:00:00'));
            $table->date('DateDoc03')->default(Carbon::createFromFormat('d/m/Y H:i:s', '01/01/1800 00:00:00'));
            $table->string('createdby')->nullable();
            $table->string('updatedby')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Tlignedoc');
    }
}
