<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePdomainesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Pdomaine', function (Blueprint $table) {
            $table->id();
            $table->integer('CodeDomaine')->unique();
            $table->string('NomDomaine')->nullable();
            $table->string('createdby')->nullable();
            $table->string('updatedby')->nullable();
            $table->timestamps();
        });
    }

        /*
            php artisan migrate:refresh --path=database/migrations/2020_10_29_002800_create_pdomaines_table.php
        */

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('Pdomaine');
    }
}
