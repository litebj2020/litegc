<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePsocietesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Psociete', function (Blueprint $table) {
            $table->id();
            $table->string('TypeSociete')->nullable();
            $table->string('RaisonSociale')->nullable();
            $table->string('Abrege')->nullable();
            $table->decimal('Capital', 12, 0)->nullable();
            $table->string('Activite')->nullable();
            $table->string('Contact1')->nullable();
            $table->string('Contact2')->nullable();
            $table->string('Fax')->nullable();
            $table->string('Email')->nullable();
            $table->string('SiteWeb')->nullable();
            $table->string('Adresse')->nullable();
            $table->string('CodePostal')->nullable();
            $table->string('Ville')->nullable();
            $table->string('Region')->nullable();
            $table->string('Pays')->nullable();
            $table->string('IdentifiantFiscal')->nullable();
            $table->string('InfoFiscal1')->nullable();
            $table->string('InfoFiscal2')->nullable();
            $table->string('Complement')->nullable();
            $table->string('Logo')->nullable();
            $table->foreignId('DeviseId')->nullable()->constrained('Pdevise');
            $table->integer('HaveMCF')->default(0);
            $table->string('createdby')->nullable();
            $table->string('updatedby')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Psociete');
    }
}
