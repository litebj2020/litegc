<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePtypedocsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Ptypedoc', function (Blueprint $table) {
            $table->id();
            $table->foreignId('DomaineId')->constrained('Pdomaine');
            $table->integer('CodeDomaine');
            $table->integer('TypeDoc');
            $table->string('LibelleType');
            $table->string('Initial');
            $table->string('createdby')->nullable();
            $table->string('updatedby')->nullable();
            $table->timestamps();
        });
    }

    /*
    php artisan migrate:refresh --path=database/migrations/2020_10_29_002819_create_ptypedocs_table.php
    */

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('Ptypedoc');
    }
}
