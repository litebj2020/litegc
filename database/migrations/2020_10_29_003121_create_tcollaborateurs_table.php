<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTcollaborateursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Tcollaborateur', function (Blueprint $table) {
            $table->id();
            $table->string('Matricule')->unique();
            $table->string('Nom')->nullable();
            $table->string('Contact1')->nullable();
            $table->string('Contact2')->nullable();
            $table->string('Email')->nullable();
            $table->string('Adresse')->nullable();
            $table->string('Ville')->nullable();
            $table->string('Pays')->nullable();
            //$table->foreignId('DomaineId')->constrained('Pdomaine');
            $table->string('createdby')->nullable();
            $table->string('updatedby')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Tcollaborateur');
    }
}
