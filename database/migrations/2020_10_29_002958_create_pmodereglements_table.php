<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePmodereglementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Pmodereglement', function (Blueprint $table) {
            $table->id();
            $table->string('LibelleReglement')->nullable();
            $table->string('CodeReglement')->nullable();
            $table->foreignId('JournalId')->constrained('Tjournal');
            $table->string('createdby')->nullable();
            $table->string('updatedby')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Pmodereglement');
    }
}
