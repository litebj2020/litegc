<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTarticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Tarticle', function (Blueprint $table) {
            $table->id();
            $table->string('RefArticle')->unique();
            $table->string('Designation');
            $table->foreignId('FamilleId')->constrained('Tfamilleart');
            $table->string('CptVente')->nullable();
            $table->string('CptAchat')->nullable();
            $table->string('CptStock')->nullable();
            $table->foreignId('SuiviId')->constrained('Psuivistock');
            $table->integer('CodeSuivi');
            $table->string('Type')->nullable();
            $table->string('Marque')->nullable();
            $table->string('Nature')->nullable();
            $table->string('CodeBarre')->nullable();
            $table->integer('Actif');
            $table->integer('StockNegatif');
            $table->foreignId('UniteId')->constrained('Punitevente');
            $table->decimal('PrixVenteHT',12,4)->nullable();
            $table->decimal('PrixVenteTTC',12,4)->nullable();
            $table->decimal('PrixAchatHT',12,4)->nullable();
            $table->decimal('PrixAchatTTC',12,4)->nullable();
            $table->decimal('DernierAchat',12,4)->nullable();
            $table->float('PoidsBrut')->nullable();
            $table->float('PoidsNet')->nullable();
            $table->string('createdby')->nullable();
            $table->string('updatedby')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Tarticle');
    }
}
