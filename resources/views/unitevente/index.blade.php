@extends('layouts.admin')
@section('title')
Liste des unités de ventes | Lite - Gestion commerciale
@endsection
@section('navtitle')
Paramètres généraux - Unités de ventes
@endsection

@section('content')
<div class="padding">
    <p class="btn-groups" style="text-align:right">
        <a style="text-align:left; float: left;" class="btn btn-rounded white" data-tt="tooltip" title="Actualiser"
            href="{{ route('unitevente.index') }}">
            <i class="fa fa-refresh"></i>
        </a>
        <button class="btn btn-rounded primary" data-toggle="modal" data-target="#bottom">
            <i class="fa fa-plus"></i> Ajouter
        </button>
        <button class="btn btn-outline btn-rounded white b-black text-black">
            <i class="fa fa-print"></i> Imprimer
        </button>
        <button class="btn btn-outline btn-rounded b-warning text-warning">
            <i class="fa fa-file-import"></i> Importer
        </button>
        <button class="btn btn-outline btn-rounded b-success text-success">
            <i class="fa fa-file-export"></i> Exporter
        </button>
    </p>

    @if (session()->has('successMessage'))
        <div class="alert alert-success" role="alert">
            {{ session('successMessage') }}
        </div>
    @endif
    @if (session()->has('errorMessage'))
        <div class="alert alert-danger" role="alert">
            {{ session('errorMessage') }}
        </div>
    @endif
    <div class="table-responsive">
        <table id="dtable" class="table table-sm v-middle p-0 m-0 box">
            <thead>
                <tr>
                    <th>UNITÉS DE VENTE</th>
                    <th style="text-align:right"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($UniteventeList as $item)
                    <tr>
                        <td class="">{{ $item->LibelleUnite }}</td>
                        <td class="" style="text-align:right">
                            <a id="btnEdit" class="btn btn-sm black edit" data-toggle="modal" data-target="#bottom2" title="Modifier"
                            data-tt="tooltip"
                            data-id="{{ $item->id }}"
                            data-libelleunite="{{ $item->LibelleUnite }}"
                            href="{{$item->id}}"><i class="fa fa-pencil text-default"></i></a>
                            <a id="btnDelete" class="btn btn-sm text-danger" data-toggle="modal" data-target="#m-a-a" data-id='{{$item->id}}' title="Supprimer" data-tt="tooltip" href="{{$item->id}}"><i class="fa fa-trash text-danger"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!--Modal-->
    <div class="modal fade" id="bottom" data-backdrop="true" aria-hidden="true">
        <div class="modal-dialog modal-bottom white b-t" style="height:75%">
            <div class="d-flex flex-column h-100">
                <form id="Register" action="{{ route('unitevente.store') }}" method="POST">
                    @csrf
                    <div class="p-3 b-b d-flex no-shrink">
                        <h5 class="h5 m-0">Ajout d'un pays</h5>
                        <div class="ml-auto">
                            <button id="formSubmit" type="submit" class="btn btn-rounded primary">
                                Enrégistrer
                            </button>
                            <button data-dismiss="modal" class="btn btn-rounded danger">
                                Annuler
                            </button>
                        </div>

                    </div>
                    <div class="scroll-y">
                        <!--Contenu du modal-->
                        <div class="box">
                            <div class="box-body offset-md-2 col-md-8">

                                <div class="form-row">
                                    <div class="form-group col-md-9">
                                        <label for="libelleUnite">Unité de vente</label>
                                        <input type="text" id="libelleUnite" name="libelleUnite"
                                            class="form-control @error('libelleUnite') is-invalid @enderror"
                                            placeholder="L'Unité de vente" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('libelleUnite') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
                <div style="bottom:0;" class="mt-auto">
                    <div class="p-3 b-t">
                        <a href="#" class="text-muted">
                            <i class="fa fa-fw fa-twitter"></i> Twitter</a>
                        <a href="#" class="text-muted m-h">
                            <i class="fa fa-fw fa-facebook"></i> Facebook</a>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <!--Modal->Modification-->
    <div class="modal fade" id="bottom2" data-backdrop="true" aria-hidden="true">
        <div class="modal-dialog modal-bottom white b-t" style="height:75%">
            <div class="d-flex flex-column h-100">
                <form id="editForm" action="" method="POST">
                    @csrf
                    @method('put')
                    <div class="p-3 b-b d-flex no-shrink">
                        <h5 class="h5 m-0">Modification d'unité de vente</h5>
                        <div class="ml-auto">
                            <button id="formSubmit" type="submit" class="btn btn-rounded primary">
                                Enrégistrer
                            </button>
                            <button data-dismiss="modal" class="btn btn-rounded danger">
                                Annuler
                            </button>
                        </div>

                    </div>
                    <div class="scroll-y">
                        <!--Contenu du modal-->
                        <div class="box">
                            <div class="box-body offset-md-2 col-md-8">

                                <div class="form-row">
                                    <input type="hidden" name="puniteventeid" id="puniteventeid">
                                    <div class="form-group col-md-9">
                                        <label for="libelleUnite2">L'Unité de vente</label>
                                        <input type="text" id="libelleUnite2" name="libelleUnite2" value="{{ old('libelleUnite2') }}"
                                            class="form-control @error('libelleUnite2') is-invalid @enderror"
                                            placeholder="L'Unité de vente" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('libelleUnite2') <span class="text-danger" role="alert"> {{ $message }}</span>@enderror
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </form>
                <div style="bottom:0;" class="mt-auto">
                    <div class="p-3 b-t">
                        <a href="#" class="text-muted">
                            <i class="fa fa-fw fa-twitter"></i>Twitter</a>
                        <a href="#" class="text-muted m-h">
                            <i class="fa fa-fw fa-facebook"></i>Facebook</a>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- Modal->Suppression-->
    <div id="m-a-a" class="modal black-overlay" data-backdrop="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="deleteForm" action="" method="POST">
                    @csrf
                    @method('delete')
                    <div class="modal-header">
                        <h5 class="modal-title">Confirmation de suppression</h5>
                    </div>
                    <div class="modal-body text-center p-lg">
                        {{-- <input type="text" id="id" name='id'> --}}
                        <p>Êtes-vous sûr de vouloir supprimer cet élément ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Non</button>
                        <button type="submit" class="btn danger p-x-md" >Oui, Supprimer !</button>
                    </div>
                </form>
            </div>
            <!-- Modal-content -->
        </div>
    </div>
</div>

@endsection

@section('scripts')


    <script>
        //Set tooltip
        $(document).ready(function () {
            $("[data-tt=tooltip]").tooltip();
            table= $('#dtable').DataTable();
        });

        //For update
        $('body').on('click', '#btnEdit', function(){
            //for row data
            $('#libelleUnite2').val($(this).attr('data-libelleunite'));
            var rowId=$(this).attr('data-id');
            document.forms.editForm.action = '/unitevente/'+rowId;
        });

        //For delete
        $(document).on('click','#btnDelete',function(){
            let id = $(this).attr('data-id');
            console.log(id);
            $('#id').val(id);
            document.forms.deleteForm.action = '/unitevente/'+id;
        });
    </script>

    @if($errors->has('libelleUnite'))
        <script>
            $(function () {
                $('#bottom').modal({
                    show: true
                });
            });
        </script>
    @endif

    @if($errors->has('libelleUnite2'))
        <script>
            $(function () {
                $('#bottom2').modal({
                    show: true
                });
            });
        </script>
    @endif
@endsection
