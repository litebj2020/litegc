@extends('layouts.admin')
@section('title')
Liste des clients | Lite - Gestion commerciale
@endsection
@section('navtitle')
Éléments de base - Clients
@endsection

@section('content')
<div class="padding">
    <p class="btn-groups" style="text-align:right">
        <a style="text-align:left; float: left;" class="btn btn-rounded white" data-tt="tooltip" title="Actualiser"
            href="{{ route('client.index') }}">
            <i class="fa fa-refresh"></i>
        </a>
        <button class="btn btn-rounded primary" data-toggle="modal" data-target="#bottom">
            <i class="fa fa-plus"></i> Ajouter
        </button>
        <button class="btn btn-outline btn-rounded white b-black text-black">
            <i class="fa fa-print"></i> Imprimer
        </button>
        <button class="btn btn-outline btn-rounded b-warning text-warning">
            <i class="fa fa-file-import"></i> Importer
        </button>
        <button class="btn btn-outline btn-rounded b-success text-success">
            <i class="fa fa-file-export"></i> Exporter
        </button>
    </p>

    @if (session()->has('successMessage'))
        <div class="alert alert-success" role="alert">
            {{ session('successMessage') }}
        </div>
    @endif
    @if (session()->has('errorMessage'))
        <div class="alert alert-danger" role="alert">
            {{ session('errorMessage') }}
        </div>
    @endif
    <div id="content" class="app-content box-shadow-0" role="main">
        <div class="content-main d-flex flex" id="content-main">
            <!-- ############ Main START-->
            <div class="d-flex flex" data-plugin="user">
                <div class="fade aside aside-sm b-r" id="content-aside">
                    <div class="modal-dialog d-flex flex-column w-md light lt" id="user-nav">
                        <div class="navbar white no-radius box-shadow pos-rlt">
                            <span class="text-md">Groups</span>
                        </div>
                        <div class="scrollable hover">
                            <div class="sidenav mt-2">
                                <nav class="nav-border b-primary" data-nav>
                                    <ul class="nav">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="#" data-toggle="tab" data-target="#tab-1">
                                                <span class="nav-badge">
                                                    <b class="badge badge-sm badge-pill warn"></b>
                                                </span>
                                                <span class="nav-text">CLIENTS</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <!-- / -->
                        <!-- footer -->
                        <div class="p-2 mt-auto p-3">
                            <div class="input-group">
                                <input type="text" class="form-control form-control-sm" id="newField" placeholder="New group"
                                    required>
                                <span class="input-group-append">
                                    <button class="btn btn-default btn-sm no-shadow" type="button" id="newBtn">
                                        <i class="fa fa-plus text-muted"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <!-- / -->
                    </div>
                </div>
                <div class="d-flex flex" id="content-body">
                    <div class="d-flex flex-column flex" id="user-list">
                        <div class="navbar white no-radius box-shadow pos-rlt">
                            <form class="flex">
                                <div class="input-group">
                                    <input type="text" class="form-control form-control-sm search" placeholder="Search"
                                        required>
                                    <span class="input-group-append">
                                        <button class="btn btn-default btn-sm no-shadow" type="button"><i
                                                class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </form>
                            <button class="btn btn-sm white ml-1 sort" data-sort="item-title" data-toggle="tooltip"
                                title="Sort">
                                <i class="fa fa-sort"></i>
                            </button>
                            <a data-toggle="modal" data-target="#content-aside" data-modal class="ml-1 d-md-none">
                                <span class="btn btn-sm btn-icon primary">
                                    <i class="fa fa-th"></i>
                                </span>
                            </a>
                        </div>
                        <div class="d-flex flex scroll-y">
                            <div class="d-flex flex-column flex white lt" >
                                <div class="scroll-y">
                                    <div class="tab-pane active" id="tab-1" >
                                        @foreach ($DataList as $item)
                                        <div class="list-item " data-id="item-5">
                                            <span class="w-40 avatar circle blue-grey" style="background: {{ \App\Http\Controllers\TtierscltController::coloreleatoire()}}">
                                                <i class="on b-white avatar-right"></i>
                                                {{ \App\Http\Controllers\TtierscltController::showedLetters($item->Intitule) }}
                                            </span>
                                            <div class="list-body">
                                                <a href="{{ route('client.show',$item->id) }}" class="item-title _500">{{ $item->Intitule }}</a>
                                                <div class="item-except text-sm text-muted h-1x">
                                                    {{ $item->Email }}
                                                </div>
                                                <div class="item-tag tag hide">
                                                    Team
                                                </div>
                                            </div>
                                            <div>
                                                <div class="item-action dropdown">
                                                    <a href="#" data-toggle="dropdown" class="text-muted">
                                                        <i class="fa fa-fw fa-ellipsis-v"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right text-color" role="menu">
                                                        <a class="dropdown-item" href="{{ route('client.show',$item->id) }}">
                                                            <i class="fa fa-eye text-primary"></i>
                                                            Afficher
                                                        </a>
                                                        <a id="btnDelete" data-toggle="modal" data-target="#m-a-a" class="dropdown-item" data-id='{{$item->id}}' href="{{$item->id}}">
                                                            <i class="fa fa-trash text-danger"></i>
                                                            Supprimer
                                                        </a>
                                                        <a class="dropdown-item">
                                                            <i class="fa fa-print text-black"></i>
                                                            Imprimer
                                                        </a>
                                                        <a class="dropdown-item">
                                                            <i class="fa fa-copy text-black"></i>
                                                            Dupliquer
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        @endforeach
                                    </div>

                                    <div class="no-result hide">
                                        <div class="p-4 text-center">
                                            No Results
                                        </div>
                                    </div>
                                </div>
                                <div class="p-3 b-t mt-auto">
                                    <div class="d-flex align-items-center">
                                        <div class="flex">
                                            <div class="pagination pagination-xs">
                                            </div>
                                        </div>
                                        <div>
                                            <span class="text-muted">Total: {{ \App\Http\Controllers\TtierscltController::countnobre()}}</span>
                                            <span id="count"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="d-flex flex-column b-l" style="width: 50px">
                                <div class="scrollable hover">
                                    <div class="text-center text-sm py-3 d-flex flex-column" id="filter">
                                        <a href="#">A</a>
                                        <a href="#">B</a>
                                        <a href="#">C</a>
                                        <a href="#">D</a>
                                        <a href="#">E</a>
                                        <a href="#">F</a>
                                        <a href="#">G</a>
                                        <a href="#">H</a>
                                        <a href="#">I</a>
                                        <a href="#">J</a>
                                        <a href="#">K</a>
                                        <a href="#">L</a>
                                        <a href="#">M</a>
                                        <a href="#">N</a>
                                        <a href="#">O</a>
                                        <a href="#">P</a>
                                        <a href="#">Q</a>
                                        <a href="#">R</a>
                                        <a href="#">S</a>
                                        <a href="#">T</a>
                                        <a href="#">U</a>
                                        <a href="#">V</a>
                                        <a href="#">W</a>
                                        <a href="#">X</a>
                                        <a href="#">Y</a>
                                        <a href="#">Z</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ############ Main END-->
        </div>
    </div>
    <!--Modal-->
    <div class="modal fade" id="bottom" data-backdrop="true" aria-hidden="true">
        <div class="modal-dialog modal-bottom white b-t" style="height:75%">
            <div class="d-flex flex-column h-100">
                <form id="addForm" action="{{ route('client.store') }}" method="POST">
                    @csrf
                    <div class="p-3 b-b d-flex no-shrink">
                        <h5 class="h5 m-0">Ajout d'un client</h5>
                        <div class="ml-auto">
                            <button id="formSubmit" type="submit" class="btn btn-rounded primary">
                                Enrégistrer
                            </button>
                            <button data-dismiss="modal" class="btn btn-rounded danger">
                                Annuler
                            </button>
                        </div>

                    </div>
                    <div class="scroll-y">
                        <!--Contenu du modal-->
                        <div class="box">
                            <div class="box-body offset-md-2 col-md-8">

                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label for="codeTiers">Code client</label>
                                        <input type="text" id="codeTiers" name="codeTiers"
                                            class="form-control @error('codeTiers') is-invalid @enderror"
                                            placeholder="Code du client" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('codeTiers') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-9">
                                        <label for="intitule">Intitulé</label>
                                        <input type="text" id="intitule" name="intitule"
                                            class="form-control @error('intitule') is-invalid @enderror"
                                            placeholder="Intitulé du client" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('intitule') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label for="noCompte">Compte collectif</label>
                                        <select id="select2-single" type="text" name="noCompte"
                                            class="form-control select2 @error('noCompte') is-invalid @enderror" data-plugin="select2" style="width: 100%">
                                            <option value="">Sélectionnez le compte</option>
                                            @foreach ($CompteList as $item)
                                            <option value="{{ $item->NoCompte }}">{{ $item->NoCompte . ' | ' . $item->IntituleCompte}}</option>
                                            @endforeach
                                        </select>
                                        @error('noCompte') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="idFiscal">Identifiant fiscal</label>
                                        <input type="text" id="idFiscal" name="idFiscal"
                                            class="form-control @error('idFiscal') is-invalid @enderror"
                                            placeholder="Identifiant fiscal">
                                        @error('idFiscal') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="infoFiscal1">Régistre</label>
                                        <input type="text" id="infoFiscal1" name="infoFiscal1"
                                            class="form-control @error('infoFiscal1') is-invalid @enderror"
                                            placeholder="Régistre" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('infoFiscal1') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="infoFiscal2">Autre identifiant</label>
                                        <input type="text" id="infoFiscal2" name="infoFiscal2"
                                            class="form-control @error('infoFiscal2') is-invalid @enderror"
                                            placeholder="Identifiant complémentaire (Optionnel)" >
                                        @error('infoFiscal2') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="contact1">Téléphone</label>
                                        <input type="text" id="contact1" name="contact1"
                                            class="form-control @error('contact1') is-invalid @enderror"
                                            placeholder="N° de téléphone" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('contact1') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="contact2">Fax</label>
                                        <input type="text" id="contact2" name="contact2"
                                            class="form-control @error('contact2') is-invalid @enderror"
                                            placeholder="Fax" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('contact2') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="email">E-mail</label>
                                        <input type="email" id="email" name="email"
                                            class="form-control @error('email') is-invalid @enderror"
                                            placeholder="Adresse e-mail">
                                        @error('email') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-10">
                                        <label for="adresse">Adresse</label>
                                        <input type="text" id="adresse" name="adresse"
                                            class="form-control @error('adresse') is-invalid @enderror"
                                            placeholder="Adresse du client">
                                        @error('adresse') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="codePostal">Code postal</label>
                                        <input type="text" id="codePostal" name="codePostal"
                                            class="form-control @error('codePostal') is-invalid @enderror"
                                            placeholder="Code postal">
                                        @error('codePostal') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="region">Région</label>
                                        <input type="text" id="region" name="region"
                                            class="form-control @error('region') is-invalid @enderror"
                                            placeholder="Région ou Département du client" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('region') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="ville">Ville</label>
                                        <select id="select2-single" type="text" name="ville"
                                            class="form-control select2 @error('ville') is-invalid @enderror" data-plugin="select2" style="width: 100%"">
                                            <option value="">Sélectionnez la ville</option>
                                            @foreach ($VilleList as $item)
                                            <option value="{{ $item->NomVille }}">{{ $item->NomVille }}</option>
                                            @endforeach
                                        </select>
                                        @error('ville') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="pays">Pays</label>
                                        <select id="select2-single" type="text" name="pays"
                                            class="form-control select2 @error('pays') is-invalid @enderror" data-plugin="select2" style="width: 100%">
                                            <option value="">Sélectionnez le pays</option>
                                            @foreach ($PaysList as $item)
                                            <option value="{{ $item->NomPays }}">{{ $item->CodePays . ' | ' . $item->NomPays}}</option>
                                            @endforeach
                                        </select>
                                        @error('pays') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
                <div style="bottom:0;" class="mt-auto">
                    <div class="p-3 b-t">
                        <a href="#" class="text-muted">
                            <i class="fa fa-fw fa-twitter"></i> Twitter</a>
                        <a href="#" class="text-muted m-h">
                            <i class="fa fa-fw fa-facebook"></i> Facebook</a>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- Modal->Suppression-->
    <div id="m-a-a" class="modal black-overlay" data-backdrop="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="deleteForm" action="" method="POST">
                    @csrf
                    @method('delete')
                    <div class="modal-header">
                        <h5 class="modal-title">Confirmation de suppression</h5>
                    </div>
                    <div class="modal-body text-center p-lg">
                        {{-- <input type="text" id="id" name='id'> --}}
                        <p>Êtes-vous sûr de vouloir supprimer cet élément ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Non</button>
                        <button type="submit" class="btn danger p-x-md" >Oui, Supprimer !</button>
                    </div>
                </form>
            </div>
            <!-- Modal-content -->
        </div>
    </div>
</div>

@endsection

@section('scripts')


    <script>
        // Add the following code if you want the name of the file appear on select
        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });

        $(document).on("click", ".browse", function() {
            var file = $(this).parents().find(".file");
            file.trigger("click");
        });
        $('input[type="file"]').change(function(e) {
            var fileName = e.target.files[0].name;
            $("#file").val(fileName);

            var reader = new FileReader();
            reader.onload = function(e) {
                // get loaded data and render thumbnail.
                document.getElementById("preview").src = e.target.result;
                document.getElementsByClassName('preview')[0].style.display = 'block';
            };
            // read the image file as a data URL.
            reader.readAsDataURL(this.files[0]);
        });


        //Set tooltip and initialize datatable
        $(document).ready(function () {
            $("[data-tt=tooltip]").tooltip();
            table= $('#dtable').DataTable();
        });

        //For delete
        $(document).on('click','#btnDelete',function(){
            let id = $(this).attr('data-id');
            console.log(id);
            $('#id').val(id);
            document.forms.deleteForm.action = '/client/'+id;
        });

        $(function () {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#addForm").validate({
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                codeTiers: "required",
                intitule: "required",
                noCompte: "required",
            },
            // Specify validation error messages
            messages: {
                codeTiers: "Veuillez renseigner le code du client",
                intitule: "Veuillez renseigner l'intitulé du client",
                noCompte: "Veuillez renseigner le numéro du compte collectif",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function (form) {

                form.submit();
            }
        });
    });
    </script>

    @if($errors->has('codeTiers') || $errors->has('intitule') || $errors->has('noCompte') || $errors->has('email'))
        <script>
            $(function () {
                $('#bottom').modal({
                    show: true
                });
            });
        </script>
    @endif
@endsection
