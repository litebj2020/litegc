@extends('layouts.admin')
@section('title')
Modes de reglement | Lite - Gestion commerciale
@endsection
@section('navtitle')
Paramètres généraux - Modes de règlement
@endsection

@section('content')
<div class="padding">
    <p class="btn-groups" style="text-align:right">
        <a style="text-align:left; float: left;" class="btn btn-rounded white" data-tt="tooltip" title="Actualiser"
            href="{{ route('modereglement.index') }}">
            <i class="fa fa-refresh"></i>
        </a>
        <button class="btn btn-rounded primary" data-toggle="modal" data-target="#bottom">
            <i class="fa fa-plus"></i> Ajouter
        </button>
        <button class="btn btn-outline btn-rounded white b-black text-black">
            <i class="fa fa-print"></i> Imprimer
        </button>
        <button class="btn btn-outline btn-rounded b-warning text-warning">
            <i class="fa fa-file-import"></i> Importer
        </button>
        <button class="btn btn-outline btn-rounded b-success text-success">
            <i class="fa fa-file-export"></i> Exporter
        </button>
    </p>

    @if (session()->has('successMessage'))
        <div class="alert alert-success" role="alert">
            {{ session('successMessage') }}
        </div>
    @endif
    @if (session()->has('errorMessage'))
        <div class="alert alert-danger" role="alert">
            {{ session('errorMessage') }}
        </div>
    @endif
    <div class="table-responsive">
        <table id="dtable" class="table table-sm v-middle p-0 m-0 box">
            <thead>
                <tr>
                    <th class="">ID</th>
                    <th class="d-none">ID</th>
                    <th>INTITULÉ</th>
                    <th>CODE</th>
                    <th>CODE JOURNAL</th>
                    <th style="text-align:right"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($ModereglementList as $item)

                    <tr>
                        <td >{{ $item->id }}</td>
                        <td class="d-none">{{ $item->JournalId }}</td>
                        <td class="">{{ $item->LibelleReglement }}</td>
                        <td class="">{{ $item->CodeReglement  }}</td>
                        <td class="">{{ $item->tjournals->CodeJournal }}</td>
                        <td class="" style="text-align:right">
                            <a id="btnEdit" class="btn btn-sm black edit" data-toggle="modal" data-target="#bottom2" title="Modifier" data-tt="tooltip" href="{{$item->id}}"><i class="fa fa-pencil default"></i></a>
                            <a id="btnDelete" class="btn btn-sm text-danger" data-toggle="modal" data-target="#m-a-a" data-id='{{$item->id}}' title="Supprimer" data-tt="tooltip" href="{{$item->id}}"><i class="fa fa-trash text-danger"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!--Modal-->
    <div class="modal fade" id="bottom" data-backdrop="true" aria-hidden="true">
        <div class="modal-dialog modal-bottom white b-t" style="height:50%">
            <div class="d-flex flex-column h-100">
                <form id="addForm" action="{{ route('modereglement.store') }}" method="POST">
                    @csrf
                    <div class="p-3 b-b d-flex no-shrink">
                        <h5 class="h5 m-0">Ajout d'un mode de reglement</h5>
                        <div class="ml-auto">
                            <button id="formSubmit" type="submit" class="btn btn-rounded primary">
                                Enrégistrer
                            </button>
                            <button data-dismiss="modal" class="btn btn-rounded danger">
                                Annuler
                            </button>
                        </div>

                    </div>
                    <div class="scroll-y">
                        <!--Contenu du modal-->
                        <div class="box">
                            <div class="box-body offset-md-2 col-md-8">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="libelleReglement">Intitulé du mode</label>
                                        <input type="text" id="libelleReglement" name="libelleReglement" value="{{ old('libelleReglement') }}"
                                            class="form-control @error('libelleReglement') is-invalid @enderror"
                                            placeholder="Reglement" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('libelleReglement') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="codeReglement">Code</label>
                                        <input type="text" id="codeReglement" name="codeReglement" value="{{ old('codeReglement') }}"
                                            class="form-control codereglement @error('codeReglement') is-invalid @enderror"
                                            placeholder="Code du reglement" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('codeReglement') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="codeJournal">Journal</label>
                                        <select id="select2-single" name="codeJournal" class="form-control select2 @error('codeJournal') is-invalid @enderror" data-plugin="select2"   style="width: 100%">
                                            <option value="">Veuillez selectionner</option>
                                            @foreach ($JournalList as $item)
                                            <option value="{{ $item->id }}">{{ $item->CodeJournal . ' | ' . $item->LibelleJournal}}</option>
                                            @endforeach
                                        </select>
                                        @error('codeJournal') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </form>
                <div style="bottom:0;" class="mt-auto">
                    <div class="p-3 b-t">
                        <a href="#" class="text-muted">
                            <i class="fa fa-fw fa-twitter"></i> Twitter</a>
                        <a href="#" class="text-muted m-h">
                            <i class="fa fa-fw fa-facebook"></i> Facebook</a>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <!--Modal->Modification-->
    <div class="modal fade" id="bottom2" data-backdrop="true" aria-hidden="true">
        <div class="modal-dialog modal-bottom white b-t" style="height:50%">
            <div class="d-flex flex-column h-100">
                <form id="ff" action="" method="POST">
                    @csrf
                    @method('put')
                    <div class="p-3 b-b d-flex no-shrink">
                        <h5 class="h5 m-0">Modification de la ville</h5>
                        <div class="ml-auto">
                            <button id="formSubmit" type="submit" class="btn btn-rounded primary">
                                Enrégistrer
                            </button>
                            <button data-dismiss="modal" class="btn btn-rounded danger">
                                Annuler
                            </button>
                        </div>

                    </div>
                    <div class="scroll-y">
                        <!--Contenu du modal-->
                        <div class="box">
                            <div class="box-body offset-md-2 col-md-8">

                                <div class="form-row">
                                    <input type="hidden" name="journalid" id="journalid">
                                    <div class="form-group col-md-6">
                                        <label for="libelleReglement2">Intitulé du mode</label>
                                        <input type="text" id="libelleReglement2" name="libelleReglement2" value="{{ old('libelleReglement2') }}"
                                            class="form-control @error('libelleReglement2') is-invalid @enderror"
                                            placeholder="Reglement" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('libelleReglement2') <span class="text-danger" role="alert"> {{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="codeReglement2">Code</label>
                                        <input type="text" id="codeReglement2" name="codeReglement2" value="{{ old('codeReglement2') }}"
                                            class="form-control codereglement @error('codeReglement2') is-invalid @enderror"
                                            placeholder="Code du reglement" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('codeReglement2') <span class="text-danger" role="alert"> {{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="codeJournal2">Journal</label>
                                        <select id="select2-single" name="codeJournal2" class="form-control select2  codeJournal2 @error('codeJournal2') is-invalid @enderror" data-plugin="select2"   style="width: 100%">
                                            <option value="">Veuillez selectionner</option>
                                            @foreach ($JournalList as $item)
                                            <option value="{{ $item->id }}">{{ $item->CodeJournal . ' | ' . $item->LibelleJournal}}</option>
                                            @endforeach
                                        </select>
                                        @error('codeJournal2') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>

                                </div>
                                <div class="form-row">
                                    {{-- <input type="hidden" name="paysid" id="paysid"> --}}

                                </div>

                            </div>
                        </div>
                    </div>

                </form>
                <div style="bottom:0;" class="mt-auto">
                    <div class="p-3 b-t">
                        <a href="#" class="text-muted">
                            <i class="fa fa-fw fa-twitter"></i>Twitter</a>
                        <a href="#" class="text-muted m-h">
                            <i class="fa fa-fw fa-facebook"></i>Facebook</a>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- Modal->Suppression-->
    <div id="m-a-a" class="modal black-overlay" data-backdrop="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="deleteForm" action="" method="POST">
                    @csrf
                    @method('delete')
                    <div class="modal-header">
                        <h5 class="modal-title">Confirmation de suppression</h5>
                    </div>
                    <div class="modal-body text-center p-lg">
                        {{-- <input type="text" id="id" name='id'> --}}
                        <p>Êtes-vous sûr de vouloir supprimer cet élément ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Non</button>
                        <button type="submit" class="btn danger p-x-md" >Oui, Supprimer !</button>
                    </div>
                </form>
            </div>
            <!-- Modal-content -->
        </div>
    </div>
</div>

@endsection

@section('scripts')


    <script>
        //Set tooltip
        $(document).ready(function () {
            $("[data-tt=tooltip]").tooltip();
            table= $('#dtable').DataTable();
            $(".codereglement").alphanum({
            allow              : '._-/',
            disallow           : '&é(èçà)ù^$%!:²',
            allowSpace         : false,
            allowNumeric       : true,
            allowUpper         : true,
            allowLower         : true,
            allowCaseless      : true,
           //allowLatin         : true,
           //allowOtherCharSets : true,
            //forceUpper         : true,
           // forceLower         : false,
            maxLength          : 4
        });
        });

        //For update
        $('body').on('click', '#btnEdit', function(){
            //to get currently clicked row object
            var row= $(this).parents('tr')[0];
            //for row data
            $('#libelleReglement2').val(table.row(row).data()[2]);
            $('#codeReglement2').val(table.row(row).data()[3]);
            var num = table.row(row).data()[1]
            $(".codeJournal2  > option[value=" + num + "]").attr("selected",true);
            var rowId=table.row(row).data()[0];
            document.forms.editForm.action = '/modereglement/'+rowId;
        });

        //For delete
        $(document).on('click','#btnDelete',function(){
            let id = $(this).attr('data-id');
            console.log(id);
            $('#id').val(id);
            document.forms.deleteForm.action = '/modereglement/'+id;
        });


        $(function () {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#addForm").validate({
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                libelleReglement: "required",
                codeReglement: "required",
                codeJournal: "required",
            },
            // Specify validation error messages
            messages: {
                libelleReglement: "Veuillez renseigner le libellé du reglemant",
                codeReglement: "Veuillez renseigner le code du reglement",
                codeJournal: "Veuillez renseigner le code du journal",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function (form) {

                form.submit();
            }
        });

        $("#ff").validate({

            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                libelleReglement2: "required",
                codeReglement2: "required",
                codeJournal2: "required",
            },
            // Specify validation error messages
            messages: {
                libelleReglement2: "Veuillez renseigner le libellé du reglemant",
                codeReglement2: "Veuillez renseigner le code du reglement",
                codeJournal2: "Veuillez renseigner le code du journal",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function (form) {
                form.submit();
            }
        });
    });
    </script>

    @if($errors->has('libelleReglement') || $errors->has('codeReglement') || $errors->has('codeJournal'))
        <script>
            $(function () {
                $('#bottom').modal({
                    show: true
                });
            });
        </script>
    @endif

    @if($errors->has('libelleReglement2') || $errors->has('codeReglement2') || $errors->has('codeJournal2'))
        <script>
            $(function () {
                $('#bottom2').modal({
                    show: true
                });
            });
        </script>
    @endif
@endsection
