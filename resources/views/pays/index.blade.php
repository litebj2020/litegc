@extends('layouts.admin')
@section('title')
    Liste des pays | Lite - Gestion commerciale
@endsection
@section('navtitle')
    Paramètres généraux - Pays
@endsection

@section('content')
    <div class="padding">
        <p class="btn-groups" style="text-align:right">
            <a style="text-align:left; float: left;" class="btn btn-rounded white" data-tt="tooltip" title="Actualiser"
                href="{{ route('pays.index') }}">
                <i class="fa fa-refresh"></i>
            </a>
            <button class="btn btn-rounded primary" data-toggle="modal" data-target="#bottom">
                <i class="fa fa-plus"></i> Ajouter
            </button>
            <button class="btn btn-outline btn-rounded white b-black text-black">
                <i class="fa fa-print"></i> Imprimer
            </button>
            <button class="btn btn-outline btn-rounded b-warning text-warning">
                <i class="fa fa-file-import"></i> Importer
            </button>
            <button class="btn btn-outline btn-rounded b-success text-success">
                <i class="fa fa-file-export"></i> Exporter
            </button>
        </p>

        @if (session()->has('successMessage'))
            <div class="alert alert-success" role="alert">
                {{ session('successMessage') }}
            </div>
        @endif
        @if (session()->has('errorMessage'))
            <div class="alert alert-danger" role="alert">
                {{ session('errorMessage') }}
            </div>
        @endif
        <div class="table-responsive">
            <table id="dtable" class="table table-sm v-middle p-0 m-0 box">
                <thead>
                    <tr>
                        <th>NOM DU PAYS</th>
                        <th>CODE</th>
                        <th style="text-align:right"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($PaysList as $item)
                        <tr>
                            <td class="">{{ $item->NomPays }}</td>
                            <td class="">{{ $item->CodePays }}</td>
                            <td class="" style="text-align:right">
                                <a id="btnEdit" class="btn btn-sm black edit" data-toggle="modal" data-target="#bottom2"
                                    title="Modifier" data-tt="tooltip" data-id="{{ $item->id }}"
                                    data-nompays="{{ $item->NomPays }}" data-codepays="{{ $item->CodePays }}"
                                    href="{{ $item->id }}"><i class="fa fa-pencil text-default"></i></a>
                                <a id="btnDelete" class="btn btn-sm text-danger" data-toggle="modal" data-target="#m-a-a"
                                    data-id='{{ $item->id }}' title="Supprimer" data-tt="tooltip" href="{{ $item->id }}"><i
                                        class="fa fa-trash text-danger"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!--Modal-->
        <div class="modal fade" id="bottom" data-backdrop="true" aria-hidden="true">
            <div class="modal-dialog modal-bottom white b-t" style="height:50%">
                <div class="d-flex flex-column h-100">
                    <form id="addForm" action="{{ route('pays.store') }}" method="POST">
                        @csrf
                        <div class="p-3 b-b d-flex no-shrink">
                            <h5 class="h5 m-0">Ajout d'un pays</h5>
                            <div class="ml-auto">
                                <button id="formSubmit" type="submit" class="btn btn-rounded primary">
                                    Enrégistrer
                                </button>
                                <button data-dismiss="modal" class="btn btn-rounded danger">
                                    Annuler
                                </button>
                            </div>

                        </div>
                        <div class="scroll-y">
                            <!--Contenu du modal-->
                            <div class="box">
                                <div class="box-body offset-md-2 col-md-8">

                                    <div class="form-row">
                                        <div class="form-group col-md-9">
                                            <label for="nomPays">Nom du pays</label>
                                            <input type="text" id="nomPays" name="nomPays"
                                                class="form-control @error('nomPays') is-invalid @enderror"
                                                placeholder="Nom du pays" onkeyup="this.value = this.value.toUpperCase();">
                                            @error('nomPays') <span class="text-danger" role="alert"> {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="codePays">Code</label>
                                            <input type="text" id="codePays" name="codePays"
                                                class="form-control codepays @error('codePays') is-invalid @enderror"
                                                placeholder="Code pays (03 caractères maximum)"
                                                onkeyup="this.value = this.value.toUpperCase();">
                                            @error('codePays') <span class="text-danger" role="alert"> {{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                    <div style="bottom:0;" class="mt-auto">
                        <div class="p-3 b-t">
                            <a href="#" class="text-muted">
                                <i class="fa fa-fw fa-twitter"></i> Twitter</a>
                            <a href="#" class="text-muted m-h">
                                <i class="fa fa-fw fa-facebook"></i> Facebook</a>

                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!--Modal->Modification-->
        <div class="modal fade" id="bottom2" data-backdrop="true" aria-hidden="true">
            <div class="modal-dialog modal-bottom white b-t" style="height:75%">
                <div class="d-flex flex-column h-100">
                    <form id="editForm" action="" method="POST">
                        @csrf
                        @method('put')
                        <div class="p-3 b-b d-flex no-shrink">
                            <h5 class="h5 m-0">Modification du pays</h5>
                            <div class="ml-auto">
                                <button id="formSubmit" type="submit" class="btn btn-rounded primary">
                                    Enrégistrer
                                </button>
                                <button data-dismiss="modal" class="btn btn-rounded danger">
                                    Annuler
                                </button>
                            </div>

                        </div>
                        <div class="scroll-y">
                            <!--Contenu du modal-->
                            <div class="box-body offset-md-2 col-md-7">
                                <div class="form-row">
                                    <input type="hidden" name="paysid" id="paysid">
                                    <div class="form-group col-md-8">
                                        <label for="nomPays2">Nom du pays</label>
                                        <input type="text" id="nomPays2" name="nomPays2" value="{{ old('nomPays2') }}"
                                            class="form-control @error('nomPays2') is-invalid @enderror"
                                            placeholder="Nom du pays" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('nomPays2') <span class="text-danger" role="alert">
                                            {{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="codePays2">Code</label>
                                        <input type="text" id="codePays2" name="codePays2" value="{{ old('codePays2') }}"
                                            class="form-control codepays @error('codePays2') is-invalid @enderror"
                                            placeholder="Code pays (03 caractères maximum)"
                                            onkeyup="this.value = this.value.toUpperCase();">
                                        @error('codePays2') <span class="text-danger" role="alert">
                                            {{ $message }}</span>@enderror
                                    </div>
                                </div>

                            </div>

                        </div>

                    </form>
                    <div style="bottom:0;" class="mt-auto">
                        <div class="p-3 b-t">
                            <a href="#" class="text-muted">
                                <i class="fa fa-fw fa-twitter"></i>Twitter</a>
                            <a href="#" class="text-muted m-h">
                                <i class="fa fa-fw fa-facebook"></i>Facebook</a>

                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!-- Modal->Suppression-->
        <div id="m-a-a" class="modal black-overlay" data-backdrop="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="deleteForm" action="" method="POST">
                        @csrf
                        @method('delete')
                        <div class="modal-header">
                            <h5 class="modal-title">Confirmation de suppression</h5>
                        </div>
                        <div class="modal-body text-center p-lg">
                            {{-- <input type="text" id="id" name='id'>
                            --}}
                            <p>Êtes-vous sûr de vouloir supprimer cet élément ?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Non</button>
                            <button type="submit" class="btn danger p-x-md">Oui, Supprimer !</button>
                        </div>
                    </form>
                </div>
                <!-- Modal-content -->
            </div>
        </div>
    </div>

@endsection

@section('scripts')


    <script>
        //Set tooltip
        $(document).ready(function() {
            $("[data-tt=tooltip]").tooltip();
            table = $('#dtable').DataTable();
            $(".codepays").alphanum({
            allow              : '._-/',
            disallow           : '&é(èçà)ù^$%!:²',
            allowSpace         : false,
            allowNumeric       : true,
            allowUpper         : true,
            allowLower         : true,
            allowCaseless      : true,
           //allowLatin         : true,
           //allowOtherCharSets : true,
            //forceUpper         : true,
           // forceLower         : false,
            maxLength          : 3
        });
        });

        //For update
        $('body').on('click', '#btnEdit', function() {
            //for row data
            $('#nomPays2').val($(this).attr('data-nompays'));
            $('#codePays2').val($(this).attr('data-codepays'));
            var rowId = $(this).attr('data-id');
            document.forms.editForm.action = '/pays/' + rowId;
        });

        //For delete
        $(document).on('click', '#btnDelete', function() {
            let id = $(this).attr('data-id');
            $('#id').val(id);
            document.forms.deleteForm.action = '/pays/' + id;
        });

        $(function() {
            // Initialize form validation on the registration form.
            // It has the name attribute "registration"
            $("#addForm").validate({
                // Specify validation rules
                rules: {
                    // The key name on the left side is the name attribute
                    // of an input field. Validation rules are defined
                    // on the right side
                    nomPays: "required",
                    codePays: {
                        required: true,
                        maxlength: 3
                    }
                },
                // Specify validation error messages
                messages: {
                    nomPays: "Veuillez renseigner le pays",
                    codePays: {
                        required: "Veuillez renseigner le code du  pays",
                        maxlength: "Le code du pays doit etre 3 caratères au maximum"
                    },
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function(form) {

                    form.submit();
                }
            });

            $("#editForm").validate({

                // Specify validation rules
                rules: {
                    // The key name on the left side is the name attribute
                    // of an input field. Validation rules are defined
                    // on the right side
                    nomPays2: "required",
                    codePays2: {
                        required: true,
                        maxlength: 3
                    }
                },
                // Specify validation error messages
                messages: {
                    nomPays2: "Veuillez renseigner le pays",
                    codePays2: {
                        required: "Veuillez renseigner le code du  pays",
                        maxlength: "Le code du pays doit etre 3 caratères au maximum"
                    },
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });

    </script>

    @if ($errors->has('codePays') || $errors->has('nomPays'))
        <script>
            $(function() {
                $('#bottom').modal({
                    show: true
                });
            });

        </script>
    @endif

    @if ($errors->has('codePays2') || $errors->has('nomPays2'))
        <script>
            $(function() {
                $('#bottom2').modal({
                    show: true
                });
            });

        </script>
    @endif
@endsection
