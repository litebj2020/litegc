@extends('layouts.admin')
@section('title')
Liste des collaborateurs | Lite - Gestion commerciale
@endsection
@section('navtitle')
Paramètres généraux - Collaborateurs
@endsection

@section('content')
<div class="padding">
    <p class="btn-groups" style="text-align:right">
        <a style="text-align:left; float: left;" class="btn btn-rounded white" data-tt="tooltip" title="Actualiser"
            href="{{ route('collaborateur.index') }}">
            <i class="fa fa-refresh"></i>
        </a>
        <button class="btn btn-rounded primary" data-toggle="modal" data-target="#bottom">
            <i class="fa fa-plus"></i> Ajouter
        </button>
        <button class="btn btn-outline btn-rounded white b-black text-black">
            <i class="fa fa-print"></i> Imprimer
        </button>
        <button class="btn btn-outline btn-rounded b-warning text-warning">
            <i class="fa fa-file-import"></i> Importer
        </button>
        <button class="btn btn-outline btn-rounded b-success text-success">
            <i class="fa fa-file-export"></i> Exporter
        </button>
    </p>

    @if (session()->has('successMessage'))
        <div class="alert alert-success" role="alert">
            {{ session('successMessage') }}
        </div>
    @endif
    @if (session()->has('errorMessage'))
        <div class="alert alert-danger" role="alert">
            {{ session('errorMessage') }}
        </div>
    @endif
    <div class="table-responsive">
        <table id="dtable" class="table table-sm v-middle p-0 m-0 box">
            <thead>
                <tr>
                    <th>MATRICULE</th>
                    <th>NOM</th>
                    <th>CONTACT1</th>
                    <th>CONTACT2</th>
                    <th>EMAIL</th>
                    <th>ADRESSE</th>
                    <th>VILLE</th>
                    <th>PAYS</th>
                    <th style="text-align:right"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($CollaborateurList as $item)
                    <tr>
                        <td class="">{{ $item->Matricule }}</td>
                        <td class="">{{ $item->Nom }}</td>
                        <td class="">{{ $item->Contact1 }}</td>
                        <td class="">{{ $item->Contact2 }}</td>
                        <td class="">{{ $item->Email }}</td>
                        <td class="">{{ $item->Adresse }}</td>
                        <td class="">{{ $item->Ville }}</td>
                        <td class="">{{ $item->Pays }}</td>
                        <td class="" style="text-align:right">
                            <a id="btnEdit" class="btn btn-sm black edit" data-toggle="modal" data-target="#bottom2"
                            data-matricule = {{ $item->Matricule }} data-nom = {{ $item->Nom }} data-contact1 = {{ $item->Contact1 }}
                            data-contact2 = {{ $item->Contact2 }} data-email = {{ $item->Email }} data-adresse = {{ $item->Adresse }}
                            data-ville = {{ $item->Ville }} data-pays = {{ $item->Pays }}
                             title="Modifier" data-tt="tooltip" data-id="{{$item->id}}"><i class="fa fa-pencil text-default"></i></a>
                            <a id="btnDelete" class="btn btn-sm text-danger" data-toggle="modal" data-target="#m-a-a" data-id='{{$item->id}}' title="Supprimer" data-tt="tooltip" href="{{$item->id}}"><i class="fa fa-trash text-danger"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!--Modal-->
    <div class="modal fade" id="bottom" data-backdrop="true" aria-hidden="true">
        <div class="modal-dialog modal-bottom white b-t" style="height:75%">
            <div class="d-flex flex-column h-100">
                <form id="addForm" action="{{ route('collaborateur.store') }}" method="POST">
                    @csrf
                    <div class="p-3 b-b d-flex no-shrink">
                        <h5 class="h5 m-0">Ajout d'un pays</h5>
                        <div class="ml-auto">
                            <button id="formSubmit" type="submit" class="btn btn-rounded primary">
                                Enrégistrer
                            </button>
                            <button data-dismiss="modal" class="btn btn-rounded danger">
                                Annuler
                            </button>
                        </div>

                    </div>
                    <div class="scroll-y">
                        <!--Contenu du modal-->
                        <div class="box">
                            <div class="box-body offset-md-2 col-md-8">

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="matricule">Matricule</label>
                                        <input type="text" id="matricule" name="matricule"
                                            class="form-control codecollaborateur @error('matricule') is-invalid @enderror"
                                            placeholder="Matricule" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('matricule') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="nom">Nom</label>
                                        <input type="text" id="nom" name="nom"
                                            class="form-control @error('nom') is-invalid @enderror"
                                            placeholder="Nom" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('nom') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="contact1">Premier contact</label>
                                        <input type="text" id="contact1" name="contact1"
                                            class="form-control @error('contact1') is-invalid @enderror"
                                            placeholder="Premier contact" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('contact1') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="contact2">Deuxième contact</label>
                                        <input type="text" id="contact2" name="contact2"
                                            class="form-control @error('contact2') is-invalid @enderror"
                                            placeholder="Deuxième contact" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('contact2') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="email">Email</label>
                                        <input type="email" id="email" name="email"
                                            class="form-control @error('email') is-invalid @enderror"
                                            placeholder="Email" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('email') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="adresse">Adresse</label>
                                        <input type="text" id="adresse" name="adresse"
                                            class="form-control @error('adresse') is-invalid @enderror"
                                            placeholder="Adresse" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('adresse') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="ville">Ville</label>
                                        <select id="select2-single" type="text" name="ville"
                                            class="form-control select2 @error('ville') is-invalid @enderror" data-plugin="select2" style="width: 100%"">
                                            <option value="">Sélectionnez la ville</option>
                                            @foreach ($VilleList as $item)
                                            <option value="{{ $item->NomVille }}">{{ $item->NomVille }}</option>
                                            @endforeach
                                        </select>
                                        @error('ville') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="pays">Pays</label>
                                        <select id="select2-single" type="text" name="pays"
                                            class="form-control select2 @error('pays') is-invalid @enderror" data-plugin="select2" style="width: 100%">
                                            <option value="">Sélectionnez le pays</option>
                                            @foreach ($PaysList as $item)
                                            <option value="{{ $item->NomPays }}">{{ $item->CodePays . ' | ' . $item->NomPays}}</option>
                                            @endforeach
                                        </select>
                                        @error('pays') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
                <div style="bottom:0;" class="mt-auto">
                    <div class="p-3 b-t">
                        <a href="#" class="text-muted">
                            <i class="fa fa-fw fa-twitter"></i> Twitter</a>
                        <a href="#" class="text-muted m-h">
                            <i class="fa fa-fw fa-facebook"></i> Facebook</a>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <!--Modal->Modification-->
    <div class="modal fade" id="bottom2" data-backdrop="true" aria-hidden="true">
        <div class="modal-dialog modal-bottom white b-t" style="height:75%">
            <div class="d-flex flex-column h-100">
                <form id="ff" action="" method="POST">
                    @csrf
                    @method('put')
                    <div class="p-3 b-b d-flex no-shrink">
                        <h5 class="h5 m-0">Modification du pays</h5>
                        <div class="ml-auto">
                            <button id="formSubmit" type="submit" class="btn btn-rounded primary">
                                Enrégistrer
                            </button>
                            <button data-dismiss="modal" class="btn btn-rounded danger">
                                Annuler
                            </button>
                        </div>

                    </div>
                    <div class="scroll-y">
                        <!--Contenu du modal-->
                        <div class="box">
                            <div class="box-body offset-md-2 col-md-8">

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="matricule2">Matricule</label>
                                        <input type="text" id="matricule2" name="matricule2" value="{{ old('matricule2') }}"
                                            class="form-control codecollaborateur @error('matricule2') is-invalid @enderror"
                                            placeholder="Matricule" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('matricule2') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="nom2">Nom</label>
                                        <input type="text" id="nom2" name="nom2" value="{{ old('nom2') }}"
                                            class="form-control @error('nom2') is-invalid @enderror"
                                            placeholder="Nom" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('nom2') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="contact12">Premier contact</label>
                                        <input type="text" id="contact12" name="contact12" value="{{ old('contact12') }}"
                                            class="form-control @error('contact12') is-invalid @enderror"
                                            placeholder="Premier contact" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('contact12') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="contact22">Deuxième contact</label>
                                        <input type="text" id="contact22" name="contact22" value="{{ old('contact22') }}"
                                            class="form-control @error('contact22') is-invalid @enderror"
                                            placeholder="Deuxième contact" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('contact22') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="email2">Email</label>
                                        <input type="email" id="email2" name="email2" value="{{ old('email2') }}"
                                            class="form-control @error('email2') is-invalid @enderror"
                                            placeholder="Email" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('email2') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="adresse2">Adresse</label>
                                        <input type="text" id="adresse2" name="adresse2" value="{{ old('adresse2') }}"
                                            class="form-control @error('adresse2') is-invalid @enderror"
                                            placeholder="Adresse" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('adresse2') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="ville2">Ville</label>
                                        <select id="ville2" name="ville2"
                                            class="form-control select2 @error('ville2') is-invalid @enderror" data-plugin="select2" style="width: 100%"">
                                            <option value="">Sélectionnez la ville</option>
                                            @foreach ($VilleList as $item)
                                            <option value="{{ $item->NomVille }}">{{ $item->NomVille }}</option>
                                            @endforeach
                                        </select>
                                        @error('ville2') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="pays2">Pays</label>
                                        <select id="pays2" name="pays2"
                                            class="form-control select2 @error('pays2') is-invalid @enderror" data-plugin="select2" style="width: 100%">
                                            <option value="">Sélectionnez le pays</option>
                                            @foreach ($PaysList as $item)
                                            <option value="{{ $item->NomPays }}">{{ $item->CodePays . ' | ' . $item->NomPays}}</option>
                                            @endforeach
                                        </select>
                                        @error('pays2') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
                <div style="bottom:0;" class="mt-auto">
                    <div class="p-3 b-t">
                        <a href="#" class="text-muted">
                            <i class="fa fa-fw fa-twitter"></i>Twitter</a>
                        <a href="#" class="text-muted m-h">
                            <i class="fa fa-fw fa-facebook"></i>Facebook</a>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- Modal->Suppression-->
    <div id="m-a-a" class="modal black-overlay" data-backdrop="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="deleteForm" action="" method="POST">
                    @csrf
                    @method('delete')
                    <div class="modal-header">
                        <h5 class="modal-title">Confirmation de suppression</h5>
                    </div>
                    <div class="modal-body text-center p-lg">
                        {{-- <input type="text" id="id" name='id'> --}}
                        <p>Êtes-vous sûr de vouloir supprimer cet élément ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Non</button>
                        <button type="submit" class="btn danger p-x-md" >Oui, Supprimer !</button>
                    </div>
                </form>
            </div>
            <!-- Modal-content -->
        </div>
    </div>
</div>

@endsection

@section('scripts')


    <script>
        //Set tooltip
        $(document).ready(function () {
            $("[data-tt=tooltip]").tooltip();
            table= $('#dtable').DataTable();
            $(".codecollaborateur").alphanum({
            allow              : '._-/',
            disallow           : '&é(èçà)ù^$%!:²',
            allowSpace         : false,
            allowNumeric       : true,
            allowUpper         : true,
            allowLower         : true,
            allowCaseless      : true,
           //allowLatin         : true,
           //allowOtherCharSets : true,
            //forceUpper         : true,
           // forceLower         : false,
            maxLength          : 15
        });
        });

        //For update
        $('body').on('click', '#btnEdit', function(){

            let id = $(this).attr('data-id');
            let matricule = $(this).attr('data-matricule');
            let nom = $(this).attr('data-nom');
            let contact1 = $(this).attr('data-contact1');
            let contact2 = $(this).attr('data-contact2');
            let email = $(this).attr('data-email');
            let adresse = $(this).attr('data-adresse');
            let ville = $(this).attr('data-ville');
            let pays = $(this).attr('data-pays');

            $('#matricule2').val(matricule);
            $('#nom2').val(nom);
            $('#contact12').val(contact1);
            $('#contact22').val(contact2);
            $('#email2').val(email);
            $('#adresse2').val(adresse);
            $('#ville2').val(ville);
            $('#pays2').val(pays);
            $('#pays2').select2().trigger('change');
            $('#ville2').select2().trigger('change');

            document.forms.editForm.action = '/collaborateur/' + id;
        });

        //For delete
        $(document).on('click','#btnDelete',function(){
            let id = $(this).attr('data-id');
            console.log(id);
            $('#id').val(id);
            document.forms.deleteForm.action = '/collaborateur/'+id;
        });

        $(function () {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#addForm").validate({
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                matricule: "required",
                nom: "required",
                contact1: "required",
                contact2: "required",
                email: "required",
                adresse: "required",
                ville: "required",
                pays: "required",
            },
            // Specify validation error messages
            messages: {
                matricule: "Veuillez renseigner le matricule",
                nom: "Veuillez renseigner le nom",
                contact1: "Veuillez renseigner le contact",
                contact2: "Veuillez renseigner le complément",
                email: "Veuillez renseigner l'adresse mail",
                adresse: "Veuillez renseigner l'adresse",
                ville: "Veuillez renseigner la ville",
                pays: "Veuillez renseigner le pays",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function (form) {

                form.submit();
            }
        });

        $("#ff").validate({

            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                matricule2: "required",
                nom2: "required",
                contact12: "required",
                contact22: "required",
                email2: "required",
                adresse2: "required",
                ville2: "required",
                pays2: "required",
            },
            // Specify validation error messages
            messages: {
                matricule2: "Veuillez renseigner le matricule",
                nom2: "Veuillez renseigner le nom",
                contact12: "Veuillez renseigner le contact",
                contact22: "Veuillez renseigner le complément",
                email2: "Veuillez renseigner l'adresse mail",
                adresse2: "Veuillez renseigner l'adresse",
                ville2: "Veuillez renseigner la ville",
                pays2: "Veuillez renseigner le pays",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function (form) {
                form.submit();
            }
        });
    });
    </script>

    @if($errors->has('matricule') || $errors->has('nom') || $errors->has('contact1') || $errors->has('contact2') || $errors->has('email') || $errors->has('adresse') || $errors->has('ville') || $errors->has('pays'))
        <script>
            $(function () {
                $('#bottom').modal({
                    show: true
                });
            });
        </script>
    @endif

    @if($errors->has('matricule2') || $errors->has('nom2') || $errors->has('contact12') || $errors->has('contact22') || $errors->has('email2') || $errors->has('adresse2') || $errors->has('ville2') || $errors->has('pays2'))
        <script>
            $(function () {
                $('#bottom2').modal({
                    show: true
                });
            });
        </script>
    @endif
@endsection
