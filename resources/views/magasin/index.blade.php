@extends('layouts.admin')
@section('title')
    Liste des magasins | Lite - Gestion commerciale
@endsection
@section('navtitle')
    Paramètres généraux - Magasin
@endsection

@section('content')
    <div class="padding">
        <p class="btn-groups" style="text-align:right">
            <a style="text-align:left; float: left;" class="btn btn-rounded white" data-tt="tooltip" title="Actualiser"
                href="{{ route('magasin.index') }}">
                <i class="fa fa-refresh"></i>
            </a>
            <button class="btn btn-rounded primary" data-toggle="modal" data-target="#bottom">
                <i class="fa fa-plus"></i> Ajouter
            </button>
            <button class="btn btn-outline btn-rounded white b-black text-black">
                <i class="fa fa-print"></i> Imprimer
            </button>
            <button class="btn btn-outline btn-rounded b-warning text-warning">
                <i class="fa fa-file-import"></i> Importer
            </button>
            <button class="btn btn-outline btn-rounded b-success text-success">
                <i class="fa fa-file-export"></i> Exporter
            </button>
        </p>

        @if (session()->has('successMessage'))
            <div class="alert alert-success" role="alert">
                {{ session('successMessage') }}
            </div>
        @endif
        @if (session()->has('errorMessage'))
            <div class="alert alert-danger" role="alert">
                {{ session('errorMessage') }}
            </div>
        @endif
        <div class="table-responsive">
            <table id="dtable" class="table table-sm v-middle p-0 m-0 box">
                <thead>
                    <tr>
                        <th>CODE</th>
                        <th>INTITULE</th>
                        <th>PAYS</th>
                        <th>VILLE</th>
                        <th>ADRESSE</th>
                        <th>GESTIONAIRE</th>
                        <th>CONTACT</th>
                        <th style="text-align:right"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($MagasinList as $item)
                        <tr>
                            <td class="">{{ $item->CodeMagasin }}</td>
                            <td class="">{{ $item->Intitule }}
                                @if ($item->isPrincipal == 1)
                                    <span style="float:right" class="badge success pos-rlt mr-2">
                                        <b class="arrow top b-success pull-in"></b>PRINCIPAL
                                    </span>
                                @endif
                            </td>
                            <td class="">{{ $item->Pays }}</td>
                            <td class="">{{ $item->Ville }}</td>
                            <td class="">{{ $item->Adresse }}</td>
                            <td class="">{{ $item->tcollaborateurs->Nom }}</td>
                            <td class="">{{ $item->Contact }}</td>
                            <td class="" style="text-align:right">
                                <a id="btnEdit" data-id="{{ $item->id }}" data-intitule="{{ $item->Intitule }}"
                                    data-codeMagasin="{{ $item->CodeMagasin }}" data-isPrincipal="{{ $item->isPrincipal }}"
                                    data-Pays="{{ $item->Pays }}" data-Ville="{{ $item->Ville }}"
                                    data-collaborateur="{{ $item->tcollaborateurs->id }}"
                                    data-Adresse="{{ $item->Adresse }}" data-Contact="{{ $item->Contact }}"
                                    class="btn btn-sm black edit" data-toggle="modal" data-target="#bottom2"
                                    title="Modifier" data-tt="tooltip" href="{{ $item->id }}">
                                    <i class="fa fa-pencil default"></i>
                                </a>
                                <a id="btnDelete" class="btn btn-sm text-danger" data-toggle="modal" data-target="#m-a-a"
                                    data-id='{{ $item->id }}' title="Supprimer" data-tt="tooltip" href="{{ $item->id }}"><i
                                        class="fa fa-trash text-danger"></i></a>

                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!--Modal-->
        <div class="modal fade" id="bottom" data-backdrop="true" aria-hidden="true">
            <div class="modal-dialog modal-bottom white b-t" style="height:75%">
                <div class="d-flex flex-column h-100">
                    <form id="addForm" action="{{ route('magasin.store') }}" method="POST">
                        @csrf
                        <div class="p-3 b-b d-flex no-shrink">
                            <h5 class="h5 m-0">Ajout d'un magasin</h5>
                            <div class="ml-auto">
                                <button id="formSubmit" type="submit" class="btn btn-rounded primary">
                                    Enrégistrer
                                </button>
                                <button data-dismiss="modal" class="btn btn-rounded danger">
                                    Annuler
                                </button>
                            </div>

                        </div>
                        <div class="scroll-y">
                            <!--Contenu du modal-->
                            <div class="box">
                                <div class="box-body offset-md-2 col-md-8">

                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <label for="codeMagasin">CODE</label>
                                            <input type="text" id="codeMagasin" name="codeMagasin"
                                                class="form-control codemagasin @error('codeMagasin') is-invalid @enderror"
                                                placeholder="Code du magasin (15 caractères maximum)"
                                                onkeyup="this.value = this.value.toUpperCase();">
                                            @error('codeMagasin') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-9">
                                            <label for="intitule">INTITULE</label>
                                            <input type="text" id="libelleJournal" name="intitule"
                                                class="form-control @error('intitule') is-invalid @enderror"
                                                placeholder="Intitule du magasin"
                                                onkeyup="this.value = this.value.toUpperCase();">
                                            @error('intitule') <span class="text-danger" role="alert"> {{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="adresse">ADRESSE</label>
                                            <input type="text" id="adresse" name="adresse"
                                                class="form-control @error('adresse') is-invalid @enderror"
                                                placeholder="Adresse du magasin "
                                                onkeyup="this.value = this.value.toUpperCase();">
                                            @error('adresse') <span class="text-danger" role="alert"> {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="contact">CONTACT</label>
                                            <input type="text" id="contact" name="contact"
                                                class="form-control @error('contact') is-invalid @enderror"
                                                placeholder="Contact du magasin "
                                                onkeyup="this.value = this.value.toUpperCase();">
                                            @error('contact') <span class="text-danger" role="alert"> {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="nomPays">PAYS</label>
                                            <select id="select2-single" name="nomPays"
                                                class="form-control select2 @error('nomPays') is-invalid @enderror"
                                                data-plugin="select2" style="width: 100%">
                                                <option value="">Veuillez selectionner</option>
                                                @foreach ($PaysList as $item)
                                                    <option value="{{ $item->NomPays }}">
                                                        {{ $item->CodePays . ' | ' . $item->NomPays }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('nomPays') <span class="text-danger" role="alert"> {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="ville">VILLE</label>
                                            <select id="select2-single" name="ville"
                                                class="form-control select2 @error('ville') is-invalid @enderror"
                                                data-plugin="select2" style="width: 100%">
                                                <option value="">Veuillez selectionner</option>
                                                @foreach ($VillesList as $item)
                                                    <option value="{{ $item->NomVille }}">{{ $item->NomVille }}</option>
                                                @endforeach
                                            </select>
                                            @error('ville') <span class="text-danger" role="alert"> {{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="collaborateur">GESTIONAIRE</label>
                                            <select id="collaborateur" name="collaborateur"
                                                class="form-control select2 @error('collaborateur') is-invalid @enderror"
                                                data-plugin="select2" style="width: 100%">
                                                <option value="">Veuillez selectionner</option>
                                                @foreach ($CollaborateurList as $item)
                                                    <option value="{{ $item->id }}">{{ $item->Nom }}</option>
                                                @endforeach
                                            </select>
                                            @error('ville') <span class="text-danger" role="alert"> {{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <label class="md-switch">
                                            <input type="checkbox" id="principal" name="principal">
                                            <i class="blue"></i>
                                            MAGASIN PRINCIPAL
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                    <div style="bottom:0;" class="mt-auto">
                        <div class="p-3 b-t">
                            <a href="#" class="text-muted">
                                <i class="fa fa-fw fa-twitter"></i> Twitter</a>
                            <a href="#" class="text-muted m-h">
                                <i class="fa fa-fw fa-facebook"></i> Facebook</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--Modal->Modification-->
        <div class="modal fade" id="bottom2" data-backdrop="true" aria-hidden="true">
            <div class="modal-dialog modal-bottom white b-t" style="height:75%">
                <div class="d-flex flex-column h-100">
                    <form id="ff" action="" method="POST">
                        @csrf
                        @method('put')
                        <div class="p-3 b-b d-flex no-shrink">
                            <h5 class="h5 m-0">Modification du Magasin </h5>
                            <div class="ml-auto">
                                <button id="formSubmit" type="submit" class="btn btn-rounded primary">
                                    Enrégistrer
                                </button>
                                <button data-dismiss="modal" class="btn btn-rounded danger">
                                    Annuler
                                </button>
                            </div>

                        </div>
                        <div class="scroll-y">
                            <!--Contenu du modal-->
                            <div class="box">
                                <div class="box-body offset-md-2 col-md-8">
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="codeMagasin2">CODE</label>
                                            <input type="text" id="codeMagasin2" name="codeMagasin2" readonly
                                                class="form-control codemagasin @error('codeMagasin2') is-invalid @enderror"
                                                placeholder="Code du magasin (03 caractères maximum)"
                                                onkeyup="this.value = this.value.toUpperCase();">
                                            @error('codeMagasin2') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-8">
                                            <label for="intitule2">INTITULE</label>
                                            <input type="text" id="intitule2" name="intitule2"
                                                class="form-control @error('intitule2') is-invalid @enderror"
                                                placeholder="Intitule du magasin"
                                                onkeyup="this.value = this.value.toUpperCase();">
                                            @error('intitule2') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="adresse2">ADRESSE</label>
                                            <input type="text" id="adresse2" name="adresse2"
                                                class="form-control @error('adresse2') is-invalid @enderror"
                                                placeholder="adresse du magasin "
                                                onkeyup="this.value = this.value.toUpperCase();">
                                            @error('adresse2') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="contact2">CONTACT</label>
                                            <input type="text" id="contact2" name="contact2"
                                                class="form-control @error('contact2') is-invalid @enderror"
                                                placeholder="contact du magasin "
                                                onkeyup="this.value = this.value.toUpperCase();">
                                            @error('contact2') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="nomPays2">PAYS</label>
                                            <select id="nomPays2" name="nomPays2"
                                                class="form-control select2 @error('nomPays2') is-invalid @enderror"
                                                data-plugin="select2" style="width: 100%">
                                                <option value="">Veuillez selectionner</option>
                                                @foreach ($PaysList as $item)
                                                    <option value="{{ $item->NomPays }}">
                                                        {{ $item->CodePays . ' | ' . $item->NomPays }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('nomPays2') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="ville2">VILLE</label>
                                            <select id="ville2" name="ville2"
                                                class="form-control select2 @error('ville2') is-invalid @enderror"
                                                data-plugin="select2" style="width: 100%">
                                                <option value="">Veuillez selectionner</option>
                                                @foreach ($VillesList as $item)
                                                    <option value="{{ $item->NomVille }}">{{ $item->NomVille }}</option>
                                                @endforeach
                                            </select>
                                            @error('ville2') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="collaborateur2">GESTIONNAIRE</label>
                                            <select id="collaborateur2" name="collaborateur2"
                                                class="form-control select2 @error('collaborateur2') is-invalid @enderror"
                                                data-plugin="select2" style="width: 100%">
                                                <option value="">Veuillez selectionner</option>
                                                @foreach ($CollaborateurList as $item)
                                                    <option value="{{ $item->id }}">{{ $item->Nom }}</option>
                                                @endforeach
                                            </select>
                                            @error('collaborateur2') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="form-row">
                                        <label class="md-switch">
                                            <input type="checkbox" id="principal2" name="principal2">
                                            <i class="blue"></i>
                                            MAGASIN PRINCIPAL
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                    <div style="bottom:0;" class="mt-auto">
                        <div class="p-3 b-t">
                            <a href="#" class="text-muted">
                                <i class="fa fa-fw fa-twitter"></i>Twitter</a>
                            <a href="#" class="text-muted m-h">
                                <i class="fa fa-fw fa-facebook"></i>Facebook</a>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal->Suppression-->
        <div id="m-a-a" class="modal black-overlay" data-backdrop="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="deleteForm" action="" method="POST">
                        @csrf
                        @method('delete')
                        <div class="modal-header">
                            <h5 class="modal-title">Confirmation de suppression</h5>
                        </div>
                        <div class="modal-body text-center p-lg">
                            <p>Êtes-vous sûr de vouloir supprimer cet élément ?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Non</button>
                            <button type="submit" class="btn danger p-x-md">Oui, Supprimer !</button>
                        </div>
                    </form>
                </div>
                <!-- Modal-content -->
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        //Set tooltip
        $(document).ready(function() {
            $("[data-tt=tooltip]").tooltip();
            table = $('#dtable').DataTable();
            $(".codemagasin").alphanum({
            allow              : '._-/',
            disallow           : '&é(èçà)ù^$%!:²',
            allowSpace         : false,
            allowNumeric       : true,
            allowUpper         : true,
            allowLower         : true,
            allowCaseless      : true,
           //allowLatin         : true,
           //allowOtherCharSets : true,
            //forceUpper         : true,
           // forceLower         : false,
            maxLength          : 15
        });
        });

        //For update
        $('body').on('click', '#btnEdit', function() {

            let id = $(this).attr('data-id');
            let codeMagasin = $(this).attr('data-codeMagasin');
            let intitule = $(this).attr('data-intitule');
            let isPrincipal = $(this).attr('data-isPrincipal');
            let Pays = $(this).attr('data-Pays');
            let Ville = $(this).attr('data-Ville');
            let collaborateur = $(this).attr('data-collaborateur');
            let Contact = $(this).attr('data-Contact');
            let Adresse = $(this).attr('data-Adresse');


            $('#codeMagasin2').val(codeMagasin);
            $('#intitule2').val(intitule);
            $('#contact2').val(Contact);
            $('#adresse2').val(Adresse);

            $('#nomPays2').val(Pays);
            $('#nomPays2').select2().trigger('change');
            $('#ville2').val(Ville);
            $('#ville2').select2().trigger('change');
            $('#collaborateur2').val(collaborateur);
            $('#collaborateur2').select2().trigger('change');

            if (isPrincipal != 0) {
                $('#principal2').prop('checked', true);
            }
            document.forms.editForm.action = '/magasin/' + id;
        });

        $(function() {
            // Initialize form validation on the registration form.
            // It has the name attribute "registration"
            $("#addForm").validate({
                // Specify validation rules
                rules: {
                    // The key name on the left side is the name attribute
                    // of an input field. Validation rules are defined
                    // on the right side
                    
                    nomPays: "required",
                    contact: "required",
                    collaborateur: "required",
                    intitule: "required",
                    adresse: "required",
                    ville: "required",
                    codeMagasin: {
                        required: true,
                        maxlength: 3
                    }
                },
                // Specify validation error messages
                messages: {
                    nomPays: "Veuillez choisir le pays",
                    contact: "Veuillez renseigner le contact",
                    collaborateur: "Veuillez choisir le collaborateur",
                    intitule: "Veuillez choisir l'intitule",
                    adresse: "Veuillez choisir l'adresse",
                    ville: "Veuillez choisir la ville",
                    codeMagasin: {
                        required: "Veuillez renseigner le code du magasin",
                        maxlength: "Le code doit etre 3 caratères au maximum"
                    },
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function(form) {

                    form.submit();
                }
            });

            $("#editForm").validate({

                // Specify validation rules
                rules: {
                    // The key name on the left side is the name attribute
                    // of an input field. Validation rules are defined
                    // on the right side
                     
                    libelle2: "required",
                    taux2: "required",
                    codeTaxe2: {
                        required: true,
                        maxlength: 3
                    },
                    sens2: {
                        required: true,
                        number: true
                    }
                },
                // Specify validation error messages
                messages: {
                    libelle2: "Veuillez renseigner le champs",
                    codeTaxe2: {
                        required: "Veuillez renseigner le code",
                        maxlength: "Le code doit etre 3 caratères au maximum"
                    },
                    taux2: {
                        required: "Veuillez renseigner le sens",
                        number: "Le sens doit etre numerique"
                    },
                    sens2: {
                        required: "Veuillez renseigner le sens",
                        number: "Le sens doit etre numerique"
                    },
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });
        //For delete
        $(document).on('click', '#btnDelete', function() {
            let id = $(this).attr('data-id');
            $('#id').val(id);
            document.forms.deleteForm.action = '/magasin/' + id;
        });

        $(function () {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#addForm").validate({
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                intitule: "required",
                codeMagasin: {
                    required: true,
                    maxlength: 15
                }
            },
            // Specify validation error messages
            messages: {
                intitule: "Veuillez renseigner l'intitulé",
                codeMagasin: {
                    required: "Veuillez renseigner le code du  magasin",
                    maxlength: "Le code du magasin ne doit pas dépasser 15 caratères maximum"
                },
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function (form) {

                form.submit();
            }
        });

        $("#ff").validate({

            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                codeMagasin2: {
                    required: true,
                    maxlength: 15
                }
            },
            // Specify validation error messages
            messages: {
                codeMagasin2: {
                    required: "Veuillez renseigner le code du  magasin",
                    maxlength: "Le code du magasin ne doit pas dépasser 15 caratères maximum"
                },
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function (form) {
                form.submit();
            }
        });
    });

    </script>

    @if ($errors->has('codeMagasin') || $errors->has('intitule') || $errors->has('adresse') || $errors->has('nomPays') || $errors->has('ville') || $errors->has('principal'))
        <script>
            $(function() {
                $('#bottom').modal({
                    show: true
                });
            });

        </script>
    @endif

    @if ($errors->has('libelleJournal2') || $errors->has('codeJournal2') || $errors->has('typeJournal2') || $errors->has('noCompte2'))
        <script>
            $(function() {
                $('#bottom2').modal({
                    show: true
                });
            });

        </script>
    @endif
@endsection
