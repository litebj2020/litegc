@extends('layouts.admin')
@section('title')
Document de stock | Lite - Gestion commerciale
@endsection
@section('navtitle')
@if ( $CurrentDoc->TypeDoc ==20)
Entrée de stock : [<span class="text-primary">{{$CurrentDoc->NoDocument}}</span>] dans {{$CurrentDoc->MagasinEmetteur}}
@endif
@if ( $CurrentDoc->TypeDoc ==21)
Sortie de stock : [<span class="text-primary">{{$CurrentDoc->NoDocument}}</span>] de {{$CurrentDoc->MagasinEmetteur}}
@endif
@if ( $CurrentDoc->TypeDoc ==22)
Transfert de stock : [<span class="text-primary">{{$CurrentDoc->NoDocument}}</span>] de {{$CurrentDoc->MagasinEmetteur}}
vers {{$CurrentDoc->MagasinEmetteur}}
@endif
@endsection

@section('content')
<div class="padding">
    @if (session()->has('successMessage'))
    <div class="alert alert-success alert-dismissible" role="alert" >
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <span>{{ session('successMessage') }}</span>

    </div>

    @endif
    @if (session()->has('errorMessage'))
    <div class="alert alert-danger alert-dismissible" role="alert" >
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <span>{{ session('errorMessage') }}</span>
    </div>
    @endif
    <div class="row">
        <div class="col-lg-12">
            <div class="box">

                <div class="box-body">
                    <form id="enteteSave" action="{{ route('documents-stock.update', $CurrentDoc->id) }}" method="POST">
                        @csrf
                        @method('put')
                        <div class="b-b nav-active-bg">
                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#" data-toggle="tab"
                                        data-target="#tab1">Informations</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-toggle="tab" data-target="#tab2">Suppléments</a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content p-3 mb-3">
                            <div class="tab-pane animate fadeIn active text-muted" id="tab1">
                                <div class="form-row">
                                    <div class="form-group col-md-2">
                                        <label for="NoDocument">N° Document</label>
                                        <input type="text" id="NoDocument" name="NoDocument"
                                            value="{{$CurrentDoc->NoDocument}}"
                                            class="form-control @error('NoDocument') is-invalid @enderror" readonly>
                                        @error('NoDocument') <span class="text-danger" role="alert">
                                            {{ $message }}</span>
                                        @enderror
                                    </div>

                                    <div class="form-group col-md-2">
                                        <label for="dateDocument">Date</label>
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                                <span class="input-group-text"><i class=" fa fa-calendar"></i></span>
                                            </span><input type="dateDocument" name="dateDocument" class="form-control"
                                                @error('dateDocument') is-invalid @enderror
                                                value="{{\Carbon\Carbon::parse($CurrentDoc->DateDocument)->format('d/m/Y')}}"
                                                data-plugin="datepicker"
                                                data-option="{autoclose: true, todayBtn: 'linked', daysOfWeekHighlighted: [6,0], format:'dd/mm/yyyy', weekStart:1}"
                                                placeholder="Date">
                                            @error('dateDocument') <span class="text-danger" role="alert">
                                                {{ $message }}</span>@enderror
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="magasinEmetteur">Magasin émetteur</label>
                                        <select id="magasinEmetteur" name="magasinEmetteur"
                                            class="form-control select2 @error('magasinEmetteur') is-invalid @enderror"
                                            data-plugin="select2" style="width: 100%">
                                            @foreach ($MagasinList as $item)
                                            <option
                                                value="{{ $CurrentDoc->MagasinEmetteur.'|'.$CurrentDoc->MagasinEmetteur }}">
                                                {{ $item->Intitule }}
                                            </option>
                                            @endforeach
                                        </select>
                                        @error('magasinEmetteur') <span class="text-danger" role="alert">
                                            {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="magasinRecepteur">Magasin récepteur</label>
                                        <select id="magasinRecepteur" name="magasinRecepteur"
                                            class="form-control select2 recepteur @error('magasinRecepteur') is-invalid @enderror"
                                            data-plugin="select2" style="width: 100%" disabled>
                                            @foreach ($MagasinList as $item)
                                            <option value="{{ $CurrentDoc->MagasinRecepteur }}">
                                                {{ $item->Intitule }}
                                            </option>
                                            @endforeach
                                        </select>
                                        @error('magasinRecepteur') <span class="text-danger" role="alert">
                                            {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="etat">État</label>
                                        <select id="select2-single" name="etat"
                                            class="form-control select2 @error('etat') is-invalid @enderror"
                                            data-plugin="select2" style="width: 100%" onchange="activeMagasin(this);">
                                            <option {{0 == $CurrentDoc->Etat ? 'selected' : ''}} value="0">SAISI
                                            </option>
                                            <option {{1 == $CurrentDoc->Etat ? 'selected' : ''}} value="1">TRAITÉ
                                            </option>
                                            <option {{2 == $CurrentDoc->Etat ? 'selected' : ''}} value="2">BOUCLÉ
                                            </option>
                                        </select>
                                        @error('etat') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane animate fadeIn text-muted" id="tab2">
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label for="refDocument">Référence document</label>
                                        <input type="text" id="refDocument" name="refDocument"
                                            value="{{$CurrentDoc->RefDocument}}"
                                            class="form-control @error('refDocument') is-invalid @enderror"
                                            placeholder="Référence">
                                        @error('refDocument') <span class="text-danger" role="alert">
                                            {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="collaborateur">Collaborateur</label>
                                        <select id="collaborateur" name="collaborateur"
                                            class="form-control select2 @error('collaborateur') is-invalid @enderror"
                                            data-plugin="select2" style="width: 100%">
                                            @foreach ($CollaborateurList as $item)
                                            <option {{$item->id == $CurrentDoc->CollaborateurId ? 'selected' : ''}}
                                                value="{{ $item->id }}">
                                                {{ $item->Nom }}
                                            </option>
                                            @endforeach
                                        </select>
                                        @error('collaborateur') <span class="text-danger" role="alert">
                                            {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="objet1">Objet</label>
                                        <input type="textarea" id="objet1" name="objet1" value="{{$CurrentDoc->Objet1}}"
                                            class="form-control @error('objet1') is-invalid @enderror"
                                            placeholder="Objet du mouvement de stock">
                                        @error('objet1') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">

                                    <div class="form-group col-md-6">
                                        <label for="objet2">Complément</label>
                                        <input type="text" id="objet2" name="objet2" value="{{$CurrentDoc->Objet2}}"
                                            class="form-control @error('objet2') is-invalid @enderror"
                                            placeholder="Informations complémentaires">
                                        @error('objet2') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>

                <div class="box-divider m-0"></div>
                <div class="box-header">
                    <div>
                        <button class="btn btn-rounded white" data-tt="tooltip" title="Retour à la liste"
                            href="{{ route('documents-stock.index') }}">
                            <i class="fa fa-arrow-left text-black"></i>
                        </button>
                        <button class="btn btn-outline btn-rounded white b-black text-black">
                            <i class="fa fa-print"></i> Imprimer
                        </button>
                        <button class="btn btn-outline btn-rounded b-warning text-warning">
                            <i class="fa fa-copy"></i> Dupliquer
                        </button>

                        <div class="btn-group">
                            <button class="btn white">Plus d'actions</button>
                            <button class="btn white dropdown-toggle" data-toggle="dropdown"></button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#"><i class="fa fa-print text-black"></i>Imprimer ce
                                    document</a>
                                <a class="dropdown-item" href="#"><i class="fa fa-copy text-black"></i>Dupliquer le
                                    document</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item danger text-white" data-toggle="modal" data-target="#m-a-a"><i
                                        class="fa fa-trash text-white"></i>Supprimer ce document</a>
                            </div>
                        </div>
                        <button type="submit" form="enteteSave" class="btn btn-rounded primary"
                            style="text-align:right; float: right;">Valider</button>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <div class="row" style="display: none">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h2>Détail du document</h2>
                    <small>Insérez ci-dessous les lignes détaillées du document</small>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-body">
                    <form role="form" id="InsertLine" action="{{ route('documents-stock-ligne.store') }}" method="POST">
                        @csrf
                        <input type="hidden" class="form-control" id="idEntete" value="{{$CurrentDoc->id}}"
                            name="idEntete">
                        <input type="hidden" class="form-control" id="codeDomaine" value="{{$CurrentDoc->CodeDomaine}}"
                            name="codeDomaine">
                        <input type="hidden" class="form-control" id="typeDoc" value="{{$CurrentDoc->TypeDoc}}"
                            name="typeDoc">
                        <input type="hidden" class="form-control" id="noDoc" value="{{$CurrentDoc->NoDocument}}"
                            name="noDoc">
                        <input type="hidden" class="form-control" id="dateDoc"
                            value="{{\Carbon\Carbon::parse($CurrentDoc->DateDocument)->format('d/m/Y')}}"
                            name="dateDoc">
                        <input type="hidden" class="form-control" id="emetteurId"
                            value="{{$CurrentDoc->MagasinEmetteurId . '|' . $CurrentDoc->MagasinEmetteur}}"
                            name="emetteurId">
                        <input type="hidden" class="form-control" id="recepteurId"
                            value="{{$CurrentDoc->MagasinRecepteurId . '|' . $CurrentDoc->MagasinRecepteur}}"
                            name="recepteurId">
                        <input type="hidden" class="form-control" id="idArticle" name="idArticle">
                        <div class="form-row">
                            <div class="col-md-1.5">
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text"><a data-toggle="modal" data-target="#user-l"
                                                data-tt="tooltip" title="Liste des articles"><i
                                                    class=" fa fa-indent"></i></a></span>
                                    </span><input type="text" class="form-control" id="refArticle" name="refArticle"
                                        placeholder="Référence article" readonly>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control" id="designation" name="designation"
                                    placeholder="Désignation">
                            </div>
                            <div class="col-md-1">
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text">QT</span>
                                    </span><input type="text" class="form-control numeric" id="quantite" name="quantite"
                                        placeholder="Quantité">
                                </div>
                            </div>
                            <div class="col-md-1.5">
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text">PU HT</span>
                                    </span><input type="text" class="form-control numeric" id="prixUnitHT"
                                        name="prixUnitHT" placeholder="PU HT">
                                </div>
                            </div>
                            <div class="col-md-1.5">
                                <input type="text" class="form-control" id="complement" name="complement"
                                    placeholder="Complément">
                            </div>
                            <div class="ml-auto">
                                <button class="btn btn-rounded" data-toggle="modal" data-target="#bottom2">
                                    <i class="fa fa-times text-danger"></i>
                                </button>
                                <button style="text-align:right; float: right" type="submit" form="InsertLine"
                                    class="btn btn-rounded white" id="insertLot">Insérer</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table id="dtable" class="table table-sm v-middle p-0 m-0 box">
            <thead>
                <tr class="">
                    <th class="">RÉFÉRENCE</th>
                    <th class="w-50">DÉSIGNATION</th>
                    <th class="text-center">QUANTITÉ</th>
                    <th class="text-right">PRIX UNIT. HT</th>
                    <th class="text-right">MONTANT HT</th>
                    <th class=""></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($CurentDetail as $item)
                <tr class="">
                    <td class="">{{ $item->RefArticle }}</td>
                    <td class="">{{ $item->Designation }}</td>
                    <td class="text-center">{{number_format($item->Quantite, 3, ',', ' ')}}</td>
                    <td class="text-right">{{number_format($item->PrixUnitHT, 2, ',', ' ')}}</td>
                    <td class="text-right">{{number_format($item->MontantHT, 0, ',', ' ')}}</td>
                    <td class="text-right">
                        <a id="btnEdit" class="btn btn-sm black edit" data-toggle="modal" data-target="#bottom2"
                            data-refarticle="{{$item->RefArticle}}" data-designation="{{$item->Designation}}"
                            data-quantite="{{$item->Quantite}}" data-prixunitht="{{$item->PrixUnitHT}}" title="Modifier"
                            data-tt="tooltip" data-id="{{$item->id}}"><i class="fa fa-pencil default"></i></a>
                        <a id="btnDelete" class="btn btn-sm text-danger" data-toggle="modal" data-target="#m-a-a"
                            data-id='{{$item->id}}' title="Supprimer" data-tt="tooltip" href="{{$item->id}}"><i
                                class="fa fa-trash text-danger"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="row" style="
        position: -webkit-sticky;
        position: sticky;
        bottom: 0;
      ">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <small>TOTAL HT : <span class="text-primary">{{$CurrentDoc->TotalHT}}</span></small>
                </div>
            </div>
        </div>

    </div>

    <div class="modal fade" id="user-l" data-backdrop="true" aria-hidden="true">
        <div class="modal-dialog modal-left white b-r" style="width: 30%">
            <div class="d-flex flex-column h-100">
                <div class="p-3 b-b d-flex no-shrink">
                    <h5 class="h5 m-0">Articles</h5>
                    <a data-dismiss="modal" class="ml-auto">
                        <span class="text-lg l-h-1x">&times;</span>
                    </a>
                </div>
                <div class="scroll-y">
                    <div class="list-group no-radius no-borders">
                        <div class="table-responsive">
                            <table id="dtable2" class="table table-sm v-middle p-0 m-0 box">
                                <thead>
                                    <tr>
                                        <th>RÉFERENCE</th>
                                        <th>DÉSIGNATION</th>
                                        <th>FAMILLE</th>
                                        <th>QTE STOCK</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($ArticleList as $item)
                                    <tr class='clickable-row' data-href="{{$item->id}}" data-id="{{$item->id}}"
                                        data-ref="{{$item->RefArticle}}" data-design="{{$item->Designation}}"
                                        data-puht="{{(float)$item->CoutMoyen}}">
                                        <td class=""><a data-dismiss="modal">{{ $item->RefArticle }}</a></td>
                                        <td class=""><a data-dismiss="modal">{{ $item->Designation }}</a></td>
                                        <td class=""><a data-dismiss="modal">{{ $item->CodeFamille}}</a></td>
                                        <td class="" style="text-align:right"><a
                                                data-dismiss="modal">{{number_format($item->QteStock, 3, ',', ' ')}}</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="p-3 b-t">
                    <a href="#" class="text-muted">
                        <i class="fa fa-fw fa-twitter"></i> Twitter</a>
                    <a href="#" class="text-muted m-h">
                        <i class="fa fa-fw fa-facebook"></i> Facebook</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal->Suppression-->
    <div id="m-a-a" class="modal black-overlay" data-backdrop="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="deleteForm" action="{{ route('documents-stock.destroy', $CurrentDoc->id) }}" method="POST">
                    @csrf
                    @method('delete')
                    <div class="modal-header">
                        <h5 class="modal-title">Confirmation de suppression</h5>
                    </div>
                    <div class="modal-body text-center p-lg">

                        <p>Êtes-vous sûr de vouloir supprimer cet élément ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Non</button>
                        <button type="submit" class="btn danger p-x-md">Oui, Supprimer !</button>
                    </div>
                </form>
            </div>
            <!-- Modal-content -->
        </div>
    </div>
</div>



@endsection

@section('scripts')

@if (session('open') == true )

<div id="modal-lot" class="modal black-overlay" data-backdrop="false">
    <div class="modal-dialog modal-dialog-scrollable modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">GESTION DES LOTS : {{session('ref')}}</h5>
            </div>

            <div class="card-body">
                <div class="form-group">
                    <div class="box">
                        <div class="box-header dark">
                            <h3>Information du lot</h3>
                        </div>
                        <div class="box-body light lt">
                            <div class="row">

                                <div class="col-sm-6 col-md-4 col-lg-4">
                                    <div class="box list-item">
                                        <span class="avatar w-40 text-center circle pink">
                                            <span class="fa fa-file"></span>
                                        </span>
                                        <div class="list-body">
                                            <h4 class="m-0 text-md">
                                                <a href="#"> <span id="qteTotal"></span>
                                                    <span id="test" class="text-sm">à
                                                        saisir</span></a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-4 col-lg-4">
                                    <div class="box list-item">
                                        <span class="avatar w-40 text-center circle primary">
                                            <span class="fa fa-file"></span>
                                        </span>
                                        <div class="list-body">
                                            <h4 class="m-0 text-md">
                                                <a href="#"> <span id="reste"></span>
                                                    <span class="text-sm">déjà saisie</span></a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-4 col-lg-4">
                                    <div class="box list-item success">
                                        <span class="avatar w-40 text-center lt">
                                            <span class="fa fa-file"></span>
                                        </span>
                                        <div class="list-body">
                                            <h4 class="m-0 text-md">
                                                <a href="#"> <span id="done"></span>
                                                    <span class="text-sm">en
                                                        attente de saisir</span></a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-12">
                    <form action="" method="POST" id="myLotForm">
                        @csrf
                        <div class="form-row">
                            <input type="hidden" name="qteTotal" value="{{session('qteLot')}}" id="qte">
                            <input type="hidden" name="ref" value="{{session('article')}}" id="ref">
                            <input type="hidden" name="enteteId" value="{{session('idEntete')}}" id="enteteId">
                            <div class="form-group col-md-3">
                                <input type="text" class="form-control form-control-sm" id="noLot"
                                    placeholder="N° du lot" name="noLot" id="noLot" required>
                            </div>
                            <div class="form-group col-md-2">
                                <input type="number" class="form-control form-control-sm numeric" id="qteEntree"
                                    name="qteEntree" placeholder="Qté entrée" required>
                            </div>
                            <div class="form-group col-md-2">
                                <div class="input-group">
                                    <input type="date" name="datePeremption" class="form-control form-control-sm"
                                        placeholder="Date péremption" id="datePeremption" required>

                                </div>
                            </div>
                            <div class="form-group col-md-2">
                                <div class="input-group">
                                    <input type="date" name="dateProduction" id="DateFabrication"
                                        class="form-control form-control-sm" data-plugin="datepicker"
                                        data-option="{autoclose: true, todayBtn: 'linked', daysOfWeekHighlighted: [6,0], format:'dd/mm/yyyy', weekStart:1}"
                                        placeholder="Date production" required>

                                </div>
                            </div>
                            <div class="form-group col-md-2">
                                <input type="text" class="form-control form-control-sm" id="complementSl"
                                    placeholder="Complément" name="complement">
                            </div>
                            <div class="form-group ">
                                <button type="submit" class="btn btn-sm btn-rounded primary" title="Insérer"
                                    id="insertDataLot"><i class="fa fa-check "></i></button>
                            </div>

                        </div>
                    </form>

                </div>

            </div>
            <div class="modal-body  p-lg">
                <div id="myMsg" class="col-md-12  " style="display: none;">
                    <div class="alert alert-success alert-sm alert-dismissible float-right col-4 float-right">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> <span id="delemsg"><span>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="lotTable" class="table table-sm v-middle p-0 m-0 ">
                        <thead>
                            <tr>
                                <th>N° Lot</th>
                                <th>Quantité entrée</th>
                                <th>Date péremption</th>
                                <th>Date production</th>
                                <th>Complément</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">Aucun élement</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <a href="{{ route('savelotdata')}}" id="validation" class="btn success p-x-md btn-sm">Valider</a>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#modal-lot').modal('show');
        resteLot();
        get_company_data();
    });
</script>

@endif

<script>
    $(document).ready(function () {
        //Set tooltip and initialize datatable
        $("[data-tt=tooltip]").tooltip();
        $(".numeric").numeric({ decimal : ".",  negative : false, scale: 4 });
        $(".coderef").alphanum({maxLength: 12});

        //Select Article
        $(".clickable-row").click(function() {
            //window.location = $(this).data("href");
            $('#idArticle').val($(this).attr('data-id'));
            $('#refArticle').val($(this).attr('data-ref'));
            $('#designation').val($(this).attr('data-design'));
            $('#quantite').val(1);
            $('#prixUnitHT').val($(this).attr('data-puht'));
            document.getElementById("designation").focus();
        });

        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});

        //delete line
        $('body').on('click','#deleteLotTemp',function(){
            var url = '/deletelotline/'+ $(this).attr('data-id');
            console.log(url);
            $.ajax({
                url: url,
                type:'GET',
                data: {id: $(this).attr('data-id') }
            }).done(function(data){
                console.log(data);
               $("#delemsg").html(data.msg);
               $("#myMsg").show();
               get_company_data();
               resteLot();
            });
        });

        $('body').on('click', '#insertDataLot', function (event) {
            event.preventDefault()
            var noLot = $("#noLot").val();
            var qteEntree = $("#qteEntree").val();
            var datePeremption = $("#datePeremption").val();
            var DateFabrication = $("#DateFabrication").val();
            var complement = $("#complementSl").val();
            var qteTotal = $("#qte").val();
            var enteteId = $("#enteteId").val();
            var ref = $("#ref").val();
            $.ajax({
                url: '/addlot',
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    noLot: noLot,
                    qteEntree: qteEntree,
                    datePeremption: datePeremption,
                    qteTotal: qteTotal,
                    dateProduction: DateFabrication,
                    complement: complement,
                    enteteId: enteteId,
                    ref: ref
                },
                dataType: 'json',
                success: function (data) {
                    $('#myLotForm').trigger("reset");
                    get_company_data()
                    resteLot();
                },
                error: function (data) {
                    console.log('Error......');
                }
            });

        });

    });
    //Get all company
    function get_company_data() {
        $.ajax({
            url: '/addlot',
            type:'GET',
            data: { }
        }).done(function(data){

            table_data_row(data)
        });
    }

    //Company table row
    function table_data_row(data) {
        var	rows = '';
        $.each( data, function( key, value ) {
            rows = rows + '<tr>';
            rows = rows + '<td>'+value.NoLot+'</td>';
            rows = rows + '<td>'+value.QteEntree+'</td>';
            rows = rows + '<td>'+value.DatePeremption+'</td>';
            rows = rows + '<td>'+value.DateFabrication+'</td>';
            rows = rows + '<td>'+value.ComplementSL+'</td>';
            rows = rows + '<td><a id="deleteLotTemp" class="btn btn-sm text-danger" data-toggle="modal" data-target="#m-a-a" data-id="'+value.id+'" title="Supprimer" data-tt="tooltip" href=""><i class="fa fa-trash text-danger"></i></a></td>';
            rows = rows + '</tr>';
        });
        $("#lotTable > tbody").html(rows);
    }
    //recuperation du reste
    function resteLot(){
        $.ajax({
            url: '/addlot/create ',
            type:'GET',
            data: { }
        }).done(function(data){
            let done = data.qteTotal - data.reste;
            $("#reste").html(data.reste);
            $("#qteTotal").html(data.qteTotal);
            $("#done").html(done);
            $("#qteEntree").attr({
            "max" : data.reste
            });
            if (done == 0) {
            $("#insertDataLot").attr('disabled',true);
            $("#validation").attr('disabled',false);
            }else{
            $("#insertDataLot").attr('disabled',false);
            $("#validation").attr('disabled',true);
            }
        });
    }


</script>

@endsection
