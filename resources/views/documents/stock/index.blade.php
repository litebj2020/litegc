@extends('layouts.admin')
@section('title')
Liste des documents de stock | Lite - Gestion commerciale
@endsection
@section('navtitle')
Documents de stock
@endsection

@section('content')
<div class="padding">
    <p class="btn-groups" style="text-align:right">
        <a style="text-align:left; float: left;" class="btn btn-rounded white" data-tt="tooltip" title="Actualiser"
            href="{{ route('documents-stock.index') }}">
            <i class="fa fa-refresh"></i>
        </a>
        <button class="btn btn-rounded primary" data-toggle="modal" data-target="#top">
            <i class="fa fa-plus"></i> Ajouter
        </button>
        <button class="btn btn-outline btn-rounded white b-black text-black">
            <i class="fa fa-print"></i> Imprimer
        </button>
        <button class="btn btn-outline btn-rounded b-warning text-warning">
            <i class="fa fa-file-import"></i> Importer
        </button>
        <button class="btn btn-outline btn-rounded b-success text-success">
            <i class="fa fa-file-export"></i> Exporter
        </button>
    </p>

    @if (session()->has('successMessage'))
        <div class="alert alert-success" role="alert">
            {{ session('successMessage') }}
        </div>
    @endif
    @if (session()->has('errorMessage'))
        <div class="alert alert-danger" role="alert">
            {{ session('errorMessage') }}
        </div>
    @endif
    <div class="table-responsive">
        <table id="dtable" class="table table-sm v-middle p-0 m-0 box">
            <thead>
                <tr>
                    <th></th>
                    <th>TYPE</th>
                    <th>STATUT</th>
                    <th>ÉTAT</th>
                    <th>N° PIECE</th>
                    <th>RÉFÉRENCE</th>
                    <th>DATE</th>
                    <th>MAGASIN ÉMETTEUR</th>
                    <th>MAGASIN RÉCEPTEUR</th>
                    <th>TOTAL HT</th>
                    <th style="text-align:right"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($DataList as $item)
                    <tr>
                        @if ( $item->TypeDoc ==20)
                            <td class=""><i class="fa fa-arrow-circle-down text-success"></td>
                        @endif
                        @if ( $item->TypeDoc ==21)
                            <td class=""><i class=" fa fa-arrow-circle-up text-danger"></span></td>
                        @endif
                        @if ( $item->TypeDoc ==22)
                            <td class=""><i class=" fa fa-arrow-circle-right text-primary"></td>
                        @endif

                        @if ( $item->TypeDoc ==20)
                            <td class=""><span class="badge dark text-u-c">ME</span></td>
                        @endif
                        @if ( $item->TypeDoc ==21)
                            <td class=""><span class="badge dark text-u-c">MS</span></span></td>
                        @endif
                        @if ( $item->TypeDoc ==22)
                            <td class=""><span class="badge dark text-u-c">MT</span></td>
                        @endif

                        <td class="">{{ $item->Statut }}</td>
                        <td class="">{{ $item->Etat }}</td>
                        <td class="">{{ $item->NoDocument }}</td>
                        <td class="">{{ $item->RefDocument }}</td>
                        <td class="">{{ \Carbon\Carbon::parse($item->DateDocument)->format('d/m/Y') }}</td>
                        <td class="">{{ $item->MagasinEmetteur }}</td>
                        <td class="">{{ $item->MagasinRecepteur }}</td>
                        <td class="">{{ $item->Total }}</td>
                        <td class="" style="text-align:right">
                            <a id="btnOpen" class="btn btn-sm text-primary" title="Ouvrir" data-tt="tooltip" href="{{ route('documents-stock.show',$item->id) }}"><i class="fa fa-folder-open text-primary"></i></a>
                            <a id="btnDelete" class="btn btn-sm text-danger" data-toggle="modal" data-target="#m-a-a" data-id='{{$item->id}}' title="Supprimer" data-tt="tooltip" href="{{$item->id}}"><i class="fa fa-trash text-danger"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!--Modal-->
    <div class="modal fade" id="top" data-backdrop="true" aria-hidden="true">
        <div class="modal-dialog modal-top white b-b" style="height:35%">
            <div class="d-flex flex-column h-100">
                <div class="p-3 b-b d-flex no-shrink">
                    <h5 class="h5 m-0">Nouveau document de stock</h5>
                    <a data-dismiss="modal" class="ml-auto">
                        <span class="text-lg l-h-1x">&times;</span>
                    </a>
                </div>
                <div class="scroll-y">
                    <form id="createDoc" action="{{ route('documents-stock.store') }}" method="POST">
                        @csrf
                        <div class="box-body offset-md-2 col-md-8">

                            <div class="form-row">
                                <div class="form-group col-md-2">
                                    <label for="typeDocument">Type de journal</label>
                                            <select id="select2-single" name="typeDocument"
                                                class="form-control select2 journal @error('typeDocument') is-invalid @enderror"
                                                data-plugin="select2" style="width: 100%" onchange="activeMagasin(this);">
                                                <option value="20">ENTRÉE EN STOCK</option>
                                                <option value="21">SORTIE DE STOCK</option>
                                                <option value="22">TRANSFERT DE STOCK</option>
                                            </select>
                                            @error('typeDocument') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                </div>

                                <div class="form-group col-md-2">
                                    <label for="dateDocument">Date</label>
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text"><i class=" fa fa-calendar"></i></span>
                                        </span><input type="dateDocument" name="dateDocument" class="form-control" @error('dateDocument')
                                    is-invalid @enderror value="{{\Carbon\Carbon::now()->format('d/m/Y')}}" data-plugin="datepicker" data-option="{autoclose: true, todayBtn: 'linked', daysOfWeekHighlighted: [6,0], format:'dd/mm/yyyy', weekStart:1}"
                                            placeholder="Date">
                                        @error('dateDocument') <span class="text-danger" role="alert">
                                            {{ $message }}</span>@enderror
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="refDocument">Référence document</label>
                                    <input type="text" id="refDocument" name="refDocument"
                                        class="form-control @error('refDocument') is-invalid @enderror"
                                        placeholder="Référence">
                                    @error('refDocument') <span class="text-danger" role="alert"> {{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="magasinEmetteur">Magasin émetteur</label>
                                    <select id="magasinEmetteur" name="magasinEmetteur"
                                        class="form-control select2 @error('magasinEmetteur') is-invalid @enderror"
                                        data-plugin="select2" style="width: 100%">
                                        @foreach ($MagasinList as $item)
                                            <option value="{{ $item->id}}|{{$item->CodeMagasin }}">
                                                {{ $item->Intitule }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @error('magasinEmetteur') <span class="text-danger" role="alert"> {{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-2 ">
                                    <label for="magasinRecepteur">Magasin récepteur</label>
                                    <select id="magasinRecepteur" name="magasinRecepteur"
                                        class="form-control select2 recepteur @error('magasinRecepteur') is-invalid @enderror"
                                        data-plugin="select2" style="width: 100%" disabled>
                                        @foreach ($MagasinList as $item)
                                            <option value="{{ $item->id}}|{{$item->CodeMagasin }}">
                                                {{ $item->Intitule }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @error('magasinRecepteur') <span class="text-danger" role="alert"> {{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-2 ">
                                    <label for="collaborateur">Collaborateur</label>
                                    <select id="collaborateur" name="collaborateur"
                                        class="form-control select2 recepteur @error('collaborateur') is-invalid @enderror"
                                        data-plugin="select2" style="width: 100%">
                                        @foreach ($CollaborateurList as $item)
                                            <option value="{{ $item->id }}">
                                                {{ $item->Nom }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @error('collaborateur') <span class="text-danger" role="alert"> {{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-2" style="display: none">
                                    <label for="NoDocument">N° Document</label>
                                    <input type="text" id="NoDocument" name="NoDocument"
                                        class="form-control @error('NoDocument') is-invalid @enderror" readonly>
                                    @error('NoDocument') <span class="text-danger" role="alert"> {{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row">


                                <div class="form-group col-md-6">
                                    <label for="objet1">Objet</label>
                                    <input type="textarea" id="objet1" name="objet1"
                                        class="form-control @error('objet1') is-invalid @enderror"
                                        placeholder="Objet du mouvement de stock">
                                    @error('objet1') <span class="text-danger" role="alert"> {{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="objet2">Complément</label>
                                    <input type="text" id="objet2" name="objet2"
                                        class="form-control @error('objet2') is-invalid @enderror"
                                        placeholder="Informations complémentaires">
                                    @error('objet2') <span class="text-danger" role="alert"> {{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="mt-auto">
                    <div class="p-3 b-t"  style="text-align: right">
                            <button id="formSubmit" type="submit" form="createDoc" class="btn btn-rounded primary">
                                Créer
                            </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal->Suppression-->
    <div id="m-a-a" class="modal black-overlay" data-backdrop="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="deleteForm" action="" method="POST">
                    @csrf
                    @method('delete')
                    <div class="modal-header">
                        <h5 class="modal-title">Confirmation de suppression</h5>
                    </div>
                    <div class="modal-body text-center p-lg">
                        <p>Êtes-vous sûr de vouloir supprimer cet élément ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Non</button>
                        <button type="submit" class="btn danger p-x-md" >Oui, Supprimer !</button>
                    </div>
                </form>
            </div>
            <!-- Modal-content -->
        </div>
    </div>
</div>

@endsection

@section('scripts')


    <script>
        //Set tooltip
        $(document).ready(function () {
            $("[data-tt=tooltip]").tooltip();
            table= $('#dtable').DataTable();
        });

        //For update
        $('body').on('click', '#btnEdit', function(){
            //for row data
            $('#libelleUnite2').val($(this).attr('data-libelleunite'));
            var rowId=$(this).attr('data-id');
            document.forms.editForm.action = '/unitevente/'+rowId;
        });

        //For delete
        $(document).on('click','#btnDelete',function(){
            let id = $(this).attr('data-id');
            console.log(id);
            $('#id').val(id);
            document.forms.deleteForm.action = '/unitevente/'+id;
        });

        function activeMagasin(that) {
            //console.log(that.value);
            if (that.value == 22) {
                document.getElementsByName('magasinRecepteur')[0].disabled = false;
            } else {
                document.getElementsByName('magasinRecepteur')[0].disabled = true;
            }
        }
    </script>

    @if($errors->has('typeDocument') || $errors->has('dateDocument') || $errors->has('magasinEmetteur') || $errors->has('collaborateur'))
        <script>
            $(function () {
                $('#bottom').modal({
                    show: true
                });
            });
        </script>
    @endif
@endsection
