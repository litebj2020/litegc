@extends('layouts.admin')
@section('title')
    Liste des documents de vente | Lite - Gestion commerciale
@endsection
@section('navtitle')
    Documents de vente
@endsection

@section('content')
    <div class="padding">
        <p class="btn-groups" style="text-align:right">
            <a style="text-align:left; float: left;" class="btn btn-rounded white" data-tt="tooltip" title="Actualiser"
                href="{{ route('documents-vente.index') }}">
                <i class="fa fa-refresh"></i>
            </a>
            <button class="btn btn-rounded primary" data-toggle="modal" data-target="#top">
                <i class="fa fa-plus"></i> Ajouter
            </button>
            <button class="btn btn-outline btn-rounded white b-black text-black">
                <i class="fa fa-print"></i> Imprimer
            </button>
            <button class="btn btn-outline btn-rounded b-warning text-warning">
                <i class="fa fa-file-import"></i> Importer
            </button>
            <button class="btn btn-outline btn-rounded b-success text-success">
                <i class="fa fa-file-export"></i> Exporter
            </button>
        </p>

        @if (session()->has('successMessage'))
            <div class="alert alert-success" role="alert">
                {{ session('successMessage') }}
            </div>
        @endif
        @if (session()->has('errorMessage'))
            <div class="alert alert-danger" role="alert">
                {{ session('errorMessage') }}
            </div>
        @endif
        <div class="table-responsive">
            <table id="dtable" class="table table-sm v-middle p-0 m-0 box">
                <thead>
                    <tr>

                        <th>N° PIECE</th>
                        <th>RÉFÉRENCE</th>
                        <th>DATE</th>
                        <th>CODETIERS</th>
                        <th>STATUT</th>
                        <th>ÉTAT</th>
                        <th>TOTAL TTC</th>
                        <th style="text-align:right"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($DataList as $item)
                        <tr>
                            <td class="">{{ $item->NoDocument }}</td>
                            <td class="">{{ $item->RefDocument }}</td>
                            <td class="">{{ $item->DateDocument }}</td>
                            <td class="">{{ $item->CodeTiers }}</td>
                            <td class="">{{ $item->Statut }}</td>
                            <td class="">{{ $item->Etat }}</td>
                            <td class="">{{ $item->TotalTTC }}</td>
                            <td class="" style="text-align:right">
                                <a id="btnEdit" class="btn btn-sm black edit" data-toggle="modal" data-target="#bottom2"
                                    title="Modifier" data-tt="tooltip" data-id="{{ $item->id }}" href="{{ $item->id }}"><i
                                        class="fa fa-pencil text-default"></i></a>
                                <a id="btnDelete" class="btn btn-sm text-danger" data-toggle="modal" data-target="#m-a-a"
                                    data-id='{{ $item->id }}' title="Supprimer" data-tt="tooltip" href="{{ $item->id }}"><i
                                        class="fa fa-trash text-danger"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!--Modal-->
        <div class="modal fade" id="top" data-backdrop="true" aria-hidden="true">
            <div class="modal-dialog modal-top white b-b" style="height:50%">
                <div class="d-flex flex-column h-100">
                    <div class="p-3 b-b d-flex no-shrink">
                        <h5 class="h5 m-0">Nouveau document de vente</h5>
                        <a data-dismiss="modal" class="ml-auto">
                            <span class="text-lg l-h-1x">&times;</span>
                        </a>
                    </div>
                    <div class="scroll-y">
                        <form id="createDoc" action="{{ route('documents-vente.store') }}" method="POST">
                            @csrf
                            <div class="box-body offset-md-2 col-md-12">
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="refDocument">N° document</label>
                                        <input type="text" id="refDocument" name="refDocument"
                                            class="form-control @error('refDocument') is-invalid @enderror"
                                            placeholder="Référence">
                                        @error('refDocument') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="dateDocument">Date</label>
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                                <span class="input-group-text"><i class=" fa fa-calendar"></i></span>
                                            </span><input type="dateDocument" name="dateDocument" class="form-control"
                                                @error('dateDocument') is-invalid @enderror
                                                value="{{ \Carbon\Carbon::now()->format('d/m/Y') }}"
                                                data-plugin="datepicker"
                                                data-option="{autoclose: true, todayBtn: 'linked', daysOfWeekHighlighted: [6,0], format:'dd/mm/yyyy', weekStart:1}"
                                                placeholder="Date">
                                            @error('dateDocument') <span class="text-danger" role="alert">
                                                {{ $message }}</span>@enderror
                                        </div>
                                    </div>

                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="typeDoc">Type de document</label>
                                        <select id="typeDoc" name="typeDoc" required
                                            class="form-control select2 @error('typeDoc') is-invalid @enderror"
                                            data-plugin="select2" style="width: 100%">
                                            <option value="">Veuillez selectionner</option>
                                            @foreach ($TypeDocList as $item)
                                                <option value="{{ $item->TypeDoc }}">
                                                    {{ $item->LibelleType }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('typeDoc') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="codeTiers">Client</label>
                                        <select id="codeTiers" name="codeTiers" required
                                            class="form-control select2 @error('codeTiers') is-invalid @enderror"
                                            data-plugin="select2" style="width: 100%">
                                            <option value="">Veuillez selectionner</option>
                                            @foreach ($TiersList as $item)
                                                <option value="{{ $item->CodeTiers }}">
                                                    {{ $item->Intitule }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('codeTiers') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>

                                </div>

                            </div>
                        </form>
                    </div>
                    <div class="mt-auto">
                        <div class="p-3 b-t" style="text-align: right">
                            <button id="formSubmit" type="submit" form="createDoc" class="btn btn-rounded primary">
                                Créer
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal->Suppression-->
        <div id="m-a-a" class="modal black-overlay" data-backdrop="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="deleteForm" action="" method="POST">
                        @csrf
                        @method('delete')
                        <div class="modal-header">
                            <h5 class="modal-title">Confirmation de suppression</h5>
                        </div>
                        <div class="modal-body text-center p-lg">
                            {{-- <input type="text" id="id" name='id'>
                            --}}
                            <p>Êtes-vous sûr de vouloir supprimer cet élément ?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Non</button>
                            <button type="submit" class="btn danger p-x-md">Oui, Supprimer !</button>
                        </div>
                    </form>
                </div>
                <!-- Modal-content -->
            </div>
        </div>
    </div>

@endsection

@section('scripts')


    <script>
        //Set tooltip
        $(document).ready(function() {
            $("[data-tt=tooltip]").tooltip();
            table = $('#dtable').DataTable();
        });

        //For update
        $('body').on('click', '#btnEdit', function() {
            //for row data
            $('#libelleUnite2').val($(this).attr('data-libelleunite'));
            var rowId = $(this).attr('data-id');
            document.forms.editForm.action = '/unitevente/' + rowId;
        });

        //For delete
        $(document).on('click', '#btnDelete', function() {
            let id = $(this).attr('data-id');
            console.log(id);
            $('#id').val(id);
            document.forms.deleteForm.action = '/unitevente/' + id;
        });

        function activeMagasin(that) {
            //console.log(that.value);
            if (that.value == 22) {
                document.getElementsByName('magasinRecepteur')[0].disabled = false;
            } else {
                document.getElementsByName('magasinRecepteur')[0].disabled = true;
            }
        }

    </script>

    @if ($errors->has('typeDocument') || $errors->has('dateDocument') || $errors->has('magasinEmetteur') || $errors->has('collaborateur'))
        <script>
            $(function() {
                $('#bottom').modal({
                    show: true
                });
            });

        </script>
    @endif
@endsection
