@extends('layouts.admin')
@section('title')
Liste des taxes | Lite - Gestion commerciale
@endsection
@section('navtitle')
Paramètres généraux - Taxes
@endsection

@section('content')
<div class="padding">
    <p class="btn-groups" style="text-align:right">
        <a style="text-align:left; float: left;" class="btn btn-rounded white" data-tt="tooltip" title="Actualiser"
            href="{{ route('taxe.index') }}">
            <i class="fa fa-refresh"></i>
        </a>
        <button class="btn btn-rounded primary" data-toggle="modal" data-target="#bottom">
            <i class="fa fa-plus"></i> Ajouter
        </button>
        <button class="btn btn-outline btn-rounded white b-black text-black">
            <i class="fa fa-print"></i> Imprimer
        </button>
        <button class="btn btn-outline btn-rounded b-warning text-warning">
            <i class="fa fa-file-import"></i> Importer
        </button>
        <button class="btn btn-outline btn-rounded b-success text-success">
            <i class="fa fa-file-export"></i> Exporter
        </button>
    </p>

    @if (session()->has('successMessage'))
        <div class="alert alert-success" role="alert">
            {{ session('successMessage') }}
        </div>
    @endif
    @if (session()->has('errorMessage'))
        <div class="alert alert-danger" role="alert">
            {{ session('errorMessage') }}
        </div>
    @endif
    <div class="table-responsive">
        <table id="dtable" class="table table-sm v-middle p-0 m-0 box">
            <thead>
                <tr>
                    <th>CODE TAXE</th>
                    <th>TAUX</th>
                    <th>LIBELLÉ</th>
                    <th>SENS</th>
                    <th style="text-align:right"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($TaxeList as $item)
                    <tr>
                        <td class="">{{ $item->CodeTaxe }}</td>
                        <td class="">{{ $item->Taux }}</td>
                        <td class="">{{ $item->Libelle }}</td>
                        <td class="">{{ $item->Sens }}</td>
                        <td class="" style="text-align:right">
                            <a id="btnEdit" class="btn btn-sm black edit" data-toggle="modal" data-target="#bottom2"
                            data-codetaxe="{{ $item->CodeTaxe }}" data-taux="{{ $item->Taux }}" data-libelle="{{ $item->Libelle }}" data-sens="{{ $item->Sens }}"
                            title="Modifier" data-tt="tooltip" data-id="{{$item->id}}"><i class="fa fa-pencil text-warn"></i></a>
                            <a id="btnDelete" class="btn btn-sm text-danger" data-toggle="modal" data-target="#m-a-a" data-id='{{$item->id}}' title="Supprimer" data-tt="tooltip" href="{{$item->id}}"><i class="fa fa-trash text-danger"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!--Modal-->
    <div class="modal fade" id="bottom" data-backdrop="true" aria-hidden="true">
        <div class="modal-dialog modal-bottom white b-t" style="height:75%">
            <div class="d-flex flex-column h-100">
                <form id="addForm" action="{{ route('taxe.store') }}" method="POST">
                    @csrf
                    <div class="p-3 b-b d-flex no-shrink">
                        <h5 class="h5 m-0">Ajout d'une taxe</h5>
                        <div class="ml-auto">
                            <button id="formSubmit" type="submit" class="btn btn-rounded primary">
                                Enrégistrer
                            </button>
                            <button data-dismiss="modal" class="btn btn-rounded danger">
                                Annuler
                            </button>
                        </div>

                    </div>
                    <div class="scroll-y">
                        <!--Contenu du modal-->
                        <div class="box">
                            <div class="box-body offset-md-2 col-md-8">

                                <div class="form-row">
                                    <div class="form-group col-md-9">
                                        <label for="codeTaxe">Code Taxe</label>
                                        <input type="text" id="codeTaxe" name="codeTaxe"
                                            class="form-control codetaxe @error('codeTaxe') is-invalid @enderror"
                                            placeholder="Nom de la taxe" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('codeTaxe') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="taux">Taux</label>
                                        <input type="text" id="taux" name="taux"
                                            class="form-control @error('taux') is-invalid @enderror"
                                            placeholder="Taux" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('taux') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-9">
                                        <label for="libelle">Libelle</label>
                                        <input type="text" id="libelle" name="libelle"
                                            class="form-control @error('libelle') is-invalid @enderror"
                                            placeholder="Libelle" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('libelle') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="sens">Sens</label>
                                        <input type="text" id="sens" name="sens"
                                            class="form-control @error('sens') is-invalid @enderror"
                                            placeholder="Sens (Prends que des chiffres)" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('sens') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
                <div style="bottom:0;" class="mt-auto">
                    <div class="p-3 b-t">
                        <a href="#" class="text-muted">
                            <i class="fa fa-fw fa-twitter"></i> Twitter</a>
                        <a href="#" class="text-muted m-h">
                            <i class="fa fa-fw fa-facebook"></i> Facebook</a>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <!--Modal->Modification-->
    <div class="modal fade" id="bottom2" data-backdrop="true" aria-hidden="true">
        <div class="modal-dialog modal-bottom white b-t" style="height:75%">
            <div class="d-flex flex-column h-100">
                <form id="editForm" action="" method="POST">
                    @csrf
                    @method('put')
                    <div class="p-3 b-b d-flex no-shrink">
                        <h5 class="h5 m-0">Modification de la taxe</h5>
                        <div class="ml-auto">
                            <button id="formSubmit" type="submit" class="btn btn-rounded primary">
                                Enrégistrer
                            </button>
                            <button data-dismiss="modal" class="btn btn-rounded danger">
                                Annuler
                            </button>
                        </div>

                    </div>
                    <div class="scroll-y">
                        <!--Contenu du modal-->
                        <div class="box">
                            <div class="box-body offset-md-2 col-md-8">

                                <div class="form-row">
                                    {{-- <input type="hidden" name="paysid" id="paysid"> --}}
                                    <div class="form-group col-md-9">
                                        <label for="codeTaxe2">Code Taxe</label>
                                        <input type="text" id="codeTaxe2" name="codeTaxe2" value="{{ old('codeTaxe2') }}"
                                            class="form-control codetaxe @error('codeTaxe2') is-invalid @enderror"
                                            placeholder="Code Taxe" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('codeTaxe2') <span class="text-danger" role="alert"> {{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="taux2">Taux</label>
                                        <input type="text" id="taux2" name="taux2" value="{{ old('taux2') }}"
                                            class="form-control @error('taux2') is-invalid @enderror"
                                            placeholder="Taux" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('taux2') <span class="text-danger" role="alert"> {{ $message }}</span>@enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    {{-- <input type="hidden" name="paysid" id="paysid"> --}}
                                    <div class="form-group col-md-9">
                                        <label for="libelle2">Libelle</label>
                                        <input type="text" id="libelle2" name="libelle2" value="{{ old('libelle2') }}"
                                            class="form-control @error('libelle2') is-invalid @enderror"
                                            placeholder="Libelle" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('libelle2') <span class="text-danger" role="alert"> {{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="sens2">Sens</label>
                                        <input type="text" id="sens2" name="sens2" value="{{ old('sens2') }}"
                                            class="form-control @error('sens2') is-invalid @enderror"
                                            placeholder="Sens (Prends que des chiffres)" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('sens2') <span class="text-danger" role="alert"> {{ $message }}</span>@enderror
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </form>
                <div style="bottom:0;" class="mt-auto">
                    <div class="p-3 b-t">
                        <a href="#" class="text-muted">
                            <i class="fa fa-fw fa-twitter"></i>Twitter</a>
                        <a href="#" class="text-muted m-h">
                            <i class="fa fa-fw fa-facebook"></i>Facebook</a>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- Modal->Suppression-->
    <div id="m-a-a" class="modal black-overlay" data-backdrop="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="deleteForm" action="" method="POST">
                    @csrf
                    @method('delete')
                    <div class="modal-header">
                        <h5 class="modal-title">Confirmation de suppression</h5>
                    </div>
                    <div class="modal-body text-center p-lg">
                        {{-- <input type="text" id="id" name='id'> --}}
                        <p>Êtes-vous sûr de vouloir supprimer cet élément ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Non</button>
                        <button type="submit" class="btn danger p-x-md" >Oui, Supprimer !</button>
                    </div>
                </form>
            </div>
            <!-- Modal-content -->
        </div>
    </div>
</div>

@endsection

@section('scripts')


    <script>
        //Set tooltip
        $(document).ready(function () {
            $("[data-tt=tooltip]").tooltip();
            table= $('#dtable').DataTable();
            $(".codetaxe").alphanum({
            allow              : '._-/',
            disallow           : '&é(èçà)ù^$%!:²',
            allowSpace         : false,
            allowNumeric       : true,
            allowUpper         : true,
            allowLower         : true,
            allowCaseless      : true,
           //allowLatin         : true,
           //allowOtherCharSets : true,
            //forceUpper         : true,
           // forceLower         : false,
            maxLength          : 6
        });
        });

        //For update
        $('body').on('click', '#btnEdit', function(){
            //to get currently clicked row object
            let id = $(this).attr('data-id');
            let codetaxe = $(this).attr('data-codetaxe');
            let taux = $(this).attr('data-taux');
            let libelle = $(this).attr('data-libelle');
            let sens = $(this).attr('data-sens');
            //for row data
            $('#codeTaxe2').val(codetaxe);
            $('#taux2').val(taux);
            $('#libelle2').val(libelle);
            $('#sens2').val(sens);
            //var rowId=table.row(row).data()[0];
            document.forms.editForm.action = '/taxe/'+ id;
        });

        //For delete
        $(document).on('click','#btnDelete',function(){
            let id = $(this).attr('data-id');
            console.log(id);
            $('#id').val(id);
            document.forms.deleteForm.action = '/taxe/'+id;
        });

        $(function() {
            // Initialize form validation on the registration form.
            // It has the name attribute "registration"
            $("#addForm").validate({
                // Specify validation rules
                rules: {
                    // The key name on the left side is the name attribute
                    // of an input field. Validation rules are defined
                    // on the right side

                    libelle: "required",
                    sens: "required",
                    taux: "required",
                    codeTaxe: {
                        required: true,
                        maxlength: 3
                    }
                },
                // Specify validation error messages
                messages: {
                    libelle: "Veuillez renseigner le champs",
                    sens: "Veuillez renseigner le champs",
                    taux: "Veuillez renseigner le champs",
                    codeTaxe: {
                        required: "Veuillez renseigner le code",
                        maxlength: "Le code doit etre 3 caratères au maximum"
                    },
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function(form) {

                    form.submit();
                }
            });

            $("#editForm").validate({

                // Specify validation rules
                rules: {
                    // The key name on the left side is the name attribute
                    // of an input field. Validation rules are defined
                    // on the right side

                    libelle2: "required",
                    taux2: "required",
                    codeTaxe2: {
                        required: true,
                        maxlength: 3
                    },
                    sens2: {
                        required: true,
                        number: true
                    }
                },
                // Specify validation error messages
                messages: {
                    libelle2: "Veuillez renseigner le champs",
                    codeTaxe2: {
                        required: "Veuillez renseigner le code",
                        maxlength: "Le code doit etre 3 caratères au maximum"
                    },
                    taux2: {
                        required: "Veuillez renseigner le sens",
                        number: "Le sens doit etre numerique"
                    },
                    sens2: {
                        required: "Veuillez renseigner le sens",
                        number: "Le sens doit etre numerique"
                    },
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });
    </script>

    @if($errors->has('codeTaxe') || $errors->has('taux') || $errors->has('libelle') || $errors->has('sens'))
        <script>
            $(function () {
                $('#bottom').modal({
                    show: true
                });
            });
        </script>
    @endif

    @if($errors->has('codeTaxe2') || $errors->has('taux2') || $errors->has('libelle2') || $errors->has('sens2'))
        <script>
            $(function () {
                $('#bottom2').modal({
                    show: true
                });
            });
        </script>
    @endif
@endsection
