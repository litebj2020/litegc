@extends('layouts.admin')
@section('title')
Liste des devises | Lite - Gestion commerciale
@endsection
@section('navtitle')
Paramètres généraux - Devise
@endsection

@section('content')
<div class="padding">
    <p class="btn-groups" style="text-align:right">
        <a style="text-align:left; float: left;" class="btn btn-rounded white" data-tt="tooltip" title="Actualiser"
            href="{{ route('devise.index') }}">
            <i class="fa fa-refresh"></i>
        </a>
        <button class="btn btn-rounded primary" data-toggle="modal" data-target="#bottom">
            <i class="fa fa-plus"></i> Ajouter
        </button>
        <button class="btn btn-outline btn-rounded white b-black text-black">
            <i class="fa fa-print"></i> Imprimer
        </button>
        <button class="btn btn-outline btn-rounded b-warning text-warning">
            <i class="fa fa-file-import"></i> Importer
        </button>
        <button class="btn btn-outline btn-rounded b-success text-success">
            <i class="fa fa-file-export"></i> Exporter
        </button>
    </p>

    @if (session()->has('successMessage'))
        <div class="alert alert-success" role="alert">
            {{ session('successMessage') }}
        </div>
    @endif
    @if (session()->has('errorMessage'))
        <div class="alert alert-danger" role="alert">
            {{ session('errorMessage') }}
        </div>
    @endif
    <div class="table-responsive">
        <table id="dtable" class="table table-sm v-middle p-0 m-0 box">
            <thead>
                <tr>
                    <th>DESIGNATION</th>
                    <th>CODE ISO</th>
                    <th>SYMBOLE</th>
                    <th class="d-none">PRINCIPAL</th>
                    <th style="text-align:right"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($DeviseList as $item)
                    <tr>
                        <td class="">{{ $item->Designation }}</td>
                        <td class="">{{ $item->CodeISO }}
                            @if ($item->Principal==1)
                            <span style="float:right" class="badge success pos-rlt mr-2"><b class="arrow top b-success pull-in"></b>PRINCIPAL</span>
                            @endif
                        </td>
                        <td class="">{{ $item->Symbole }}</td>
                        <td class="d-none">{{ $item->Principal }}</td>
                        <td class="" style="text-align:right">
                            <a id="btnEdit" class="btn btn-sm black edit" data-toggle="modal" data-target="#bottom2"
                            data-codeIso="{{$item->CodeISO}}" data-designation="{{$item->Designation}}" data-symbole="{{$item->Symbole}}" data-isPrincipal="{{$item->Principal}}"
                             title="Modifier" data-tt="tooltip" data-id="{{$item->id}}"><i class="fa fa-pencil default"></i></a>
                            <a id="btnDelete" class="btn btn-sm text-danger" data-toggle="modal" data-target="#m-a-a"
                            data-id='{{$item->id}}' title="Supprimer" data-tt="tooltip" href="{{$item->id}}"><i class="fa fa-trash text-danger"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!--Modal-->
    <div class="modal fade" id="bottom" data-backdrop="true" aria-hidden="true">
        <div class="modal-dialog modal-bottom white b-t" style="height:75%">
            <div class="d-flex flex-column h-100">
                <form id="addForm" action="{{ route('devise.store') }}" method="POST">
                    @csrf
                    <div class="p-3 b-b d-flex no-shrink">
                        <h5 class="h5 m-0">Ajout d'une devise</h5>
                        <div class="ml-auto">
                            <button id="formSubmit" type="submit" class="btn btn-rounded primary">
                                Enrégistrer
                            </button>
                            <button data-dismiss="modal" class="btn btn-rounded danger">
                                Annuler
                            </button>
                        </div>

                    </div>
                    <div class="scroll-y">
                        <!--Contenu du modal-->
                        <div class="box">
                            <div class="box-body offset-md-2 col-md-8">

                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="designation">Designation</label>
                                        <input type="text" id="designation" name="designation"
                                            class="form-control @error('designation') is-invalid @enderror"
                                            placeholder="Designation" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('designation') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="codeIso">Code ISO</label>
                                        <input type="text" id="codeIso" name="codeIso"
                                            class="form-control codedevise @error('codeIso') is-invalid @enderror"
                                            placeholder="Code ISO (03 caractères maximum)" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('codeIso') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="symbole">Symbole</label>
                                        <input type="text" id="symbole" name="symbole"
                                            class="form-control @error('symbole') is-invalid @enderror"
                                            placeholder="Symbole (€ / $ / FCFA)" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('symbole') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <p>
                                            Définir comme devise principale ?
                                            <label class="md-switch">
                                                <input type="checkbox" id="principal" name="principal">
                                                <i class="blue"></i>

                                            </label>
                                        </p>
                                        {{-- <input type="text" id="principal" name="principal"
                                            class="form-control @error('Principal') is-invalid @enderror"
                                            placeholder="Principal" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('principal') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
                <div style="bottom:0;" class="mt-auto">
                    <div class="p-3 b-t">
                        <a href="#" class="text-muted">
                            <i class="fa fa-fw fa-twitter"></i> Twitter</a>
                        <a href="#" class="text-muted m-h">
                            <i class="fa fa-fw fa-facebook"></i> Facebook</a>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <!--Modal->Modification-->
    <div class="modal fade" id="bottom2" data-backdrop="true" aria-hidden="true">
        <div class="modal-dialog modal-bottom white b-t" style="height:75%">
            <div class="d-flex flex-column h-100">
                <form id="ff" action="" method="POST">
                    @csrf
                    @method('put')
                    <div class="p-3 b-b d-flex no-shrink">
                        <h5 class="h5 m-0">Modification de la devise</h5>
                        <div class="ml-auto">
                            <button id="formSubmit" type="submit" class="btn btn-rounded primary">
                                Enrégistrer
                            </button>
                            <button data-dismiss="modal" class="btn btn-rounded danger">
                                Annuler
                            </button>
                        </div>

                    </div>
                    <div class="scroll-y">
                        <!--Contenu du modal-->
                        <div class="box">
                            <div class="box-body offset-md-2 col-md-8">

                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="designation2">Designation</label>
                                    <input type="text" id="designation2" name="designation2" value="{{old('designation2')}}"
                                            class="form-control @error('designation2') is-invalid @enderror"
                                            placeholder="Désignation" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('designation2') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="codeIso2">Code ISO</label>
                                        <input type="text" id="codeIso2" name="codeIso2" value="{{old('codeIso2')}}"
                                            class="form-control codedevise @error('codeIso2') is-invalid @enderror"
                                            placeholder="Code ISO (03 caractères maximum)" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('codeIso2') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="symbole2">Symbole</label>
                                        <input type="text" id="symbole2" name="symbole2" value="{{old('symbole2')}}"
                                            class="form-control @error('symbole2') is-invalid @enderror"
                                            placeholder="Symbole (€ / $ / FCFA)" onkeyup="this.value = this.value.toUpperCase();">
                                        @error('symbole2') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <p>
                                            Définir comme devise principale ?
                                            <label class="md-switch">
                                                <input type="checkbox" id="principal2" name="principal2">
                                                <i class="blue"></i>

                                            </label>
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </form>
                <div style="bottom:0;" class="mt-auto">
                    <div class="p-3 b-t">
                        <a href="#" class="text-muted">
                            <i class="fa fa-fw fa-twitter"></i>Twitter</a>
                        <a href="#" class="text-muted m-h">
                            <i class="fa fa-fw fa-facebook"></i>Facebook</a>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- Modal->Suppression-->
    <div id="m-a-a" class="modal black-overlay" data-backdrop="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="deleteForm" action="" method="POST">
                    @csrf
                    @method('delete')
                    <div class="modal-header">
                        <h5 class="modal-title">Confirmation de suppression</h5>
                    </div>
                    <div class="modal-body text-center p-lg">
                        {{-- <input type="text" id="id" name='id'> --}}
                        <p>Êtes-vous sûr de vouloir supprimer cet élément ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Non</button>
                        <button type="submit" class="btn danger p-x-md" >Oui, Supprimer !</button>
                    </div>
                </form>
            </div>
            <!-- Modal-content -->
        </div>
    </div>
</div>

@endsection

@section('scripts')


    <script>
        //Set tooltip
        $(document).ready(function () {
            $("[data-tt=tooltip]").tooltip();
            table= $('#dtable').DataTable();
            $(".codedevise").alphanum({
            allow              : '._-/',
            disallow           : '&é(èçà)ù^$%!:²',
            allowSpace         : false,
            allowNumeric       : true,
            allowUpper         : true,
            allowLower         : true,
            allowCaseless      : true,
           //allowLatin         : true,
           //allowOtherCharSets : true,
            //forceUpper         : true,
           // forceLower         : false,
            maxLength          : 3
        });
        });

        //For update
        $('body').on('click', '#btnEdit', function(){
            let id = $(this).attr('data-id');
            let codeiso = $(this).attr('data-codeIso');
            let designation = $(this).attr('data-designation');
            let symbole = $(this).attr('data-symbole');
            let principal = $(this).attr('data-isPrincipal');

           /*  if (principal != 0) {
                $('#isPrincipal2').prop('checked', true);
            } */
            console.log(principal)
            if (principal == 1) {
                $('#principal2').prop('checked', true);
            } else {
                $('#principal2').prop('checked', false);
            }

            $('#codeIso2').val(codeiso);
            $('#designation2').val(designation);
            $('#symbole2').val(symbole);
            $('#isPrincipal2').val(principal);

            document.forms.editForm.action = '/devise/' + id;
        });

        //For delete
        $(document).on('click','#btnDelete',function(){
            let id = $(this).attr('data-id');
            console.log(id);
            $('#id').val(id);
            document.forms.deleteForm.action = '/devise/'+id;
        });

        $(function () {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#addForm").validate({
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                designation: "required",
                symbole: "required",
                codeIso: {
                    required: true,
                    maxlength: 3
                }
            },
            // Specify validation error messages
            messages: {
                designation: "Veuillez renseigner la désignation de la devise",
                symbole: "Veuillez renseigner le symbole",
                codeIso: {
                    required: "Veuillez renseigner le code de la devise",
                    maxlength: "Le code de la devise doit etre 3 caratères au maximum"
                },
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function (form) {

                form.submit();
            }
        });

        $("#ff").validate({

            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                designation2: "required",
                symbole2: "required",
                codeIso2: {
                    required: true,
                    maxlength: 3
                }
            },
            // Specify validation error messages
            messages: {
                designation2: "Veuillez renseigner la désignation de la devise",
                symbole2: "Veuillez renseigner le symbole",
                codeIso2: {
                    required: "Veuillez renseigner le code de la devise",
                    maxlength: "Le code de la devise doit etre 3 caratères au maximum"
                },
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function (form) {
                form.submit();
            }
        });
    });
    </script>

    @if($errors->has('codeIso') || $errors->has('designation')|| $errors->has('symbole')|| $errors->has('principal'))
        <script>
            $(function () {
                $('#bottom').modal({
                    show: true
                });
            });
        </script>
    @endif

    @if($errors->has('codeIso2') || $errors->has('designation2')|| $errors->has('symbole2')|| $errors->has('principal2'))
        <script>
            $(function () {
                $('#bottom2').modal({
                    show: true
                });
            });
        </script>
    @endif
@endsection
