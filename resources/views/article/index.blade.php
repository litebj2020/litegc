@extends('layouts.admin')
@section('title')
Liste des articles | Lite - Gestion commerciale
@endsection
@section('navtitle')
Éléments de base - Articles
@endsection

@section('content')
<div class="padding">
    <p class="btn-groups" style="text-align:right">
        <a style="text-align:left; float: left;" class="btn btn-rounded white" data-tt="tooltip" title="Actualiser"
            href="{{ route('client.index') }}">
            <i class="fa fa-refresh"></i>
        </a>
        <button class="btn btn-rounded primary" data-toggle="modal" data-target="#bottom">
            <i class="fa fa-plus"></i> Ajouter
        </button>
        <button class="btn btn-outline btn-rounded white b-black text-black">
            <i class="fa fa-print"></i> Imprimer
        </button>
        <button class="btn btn-outline btn-rounded b-warning text-warning">
            <i class="fa fa-file-import"></i> Importer
        </button>
        <button class="btn btn-outline btn-rounded b-success text-success">
            <i class="fa fa-file-export"></i> Exporter
        </button>
    </p>

    @if (session()->has('successMessage'))
    <div class="alert alert-success" role="alert">
        {{ session('successMessage') }}
    </div>
    @endif
    @if (session()->has('errorMessage'))
    <div class="alert alert-danger" role="alert">
        {{ session('errorMessage') }}
    </div>
    @endif
    <div class="table-responsive">
        <table id="dtable" class="table table-sm v-middle p-0 m-0 box">
            <thead>
                <tr>
                    <th>RÉF. ARTICLE</th>
                    <th>DÉSIGNATION</th>
                    <th>FAMILLE</th>
                    <th>SUIVI STOCK</th>
                    <th>UNITÉ</th>
                    <th>STOCK RÉEL</th>
                    <th style="text-align:right"></th>
                </tr>
            </thead>
            <tbody>


                @foreach ($DataList as $item)
                <tr>
                    <td class="">{{ $item->RefArticle }}</td>
                    <td class="">{{ $item->Designation }}</td>
                    <td class="">{{ $item->CodeFamille}}</td>
                    <td class="">{{ $item->LibelleSuivi}}</td>
                    <td class="">{{ $item->LibelleUnite }}</td>
                    <td class="" style="text-align:right">{{ number_format($item->QteStock, 0, ',', ' ') }}</td>
                    <td class="" style="text-align:right">
                        <a id="btnOpen" class="btn btn-sm text-primary" title="Ouvrir" data-tt="tooltip"
                            href="{{ route('article.show',$item->id) }}"><i class="fa fa-eye text-primary"></i></a>
                        <a id="btnDelete" class="btn btn-sm text-danger" data-toggle="modal" data-target="#m-a-a"
                            data-id='{{$item->id}}' title="Supprimer" data-tt="tooltip" href="{{$item->id}}"><i
                                class="fa fa-trash text-danger"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!--Modal-->
    <div class="modal fade" id="bottom" data-backdrop="true" aria-hidden="true">
        <div class="modal-dialog modal-bottom white b-t" style="height: 65%">
            <div class="d-flex flex-column h-100">
                <form id="addForm" action="{{ route('article.store') }}" method="POST">
                    @csrf
                    <div class="p-3 b-b d-flex no-shrink">
                        <h5 class="h5 m-0">Ajout d'un article</h5>
                        <div class="ml-auto">
                            <button id="formSubmit" type="submit" class="btn btn-rounded primary">
                                Enrégistrer
                            </button>
                            <button data-dismiss="modal" class="btn btn-rounded danger">
                                Annuler
                            </button>
                        </div>

                    </div>
                    <div class="">
                        <!--Contenu du modal-->
                        <div class="">
                            <div class="box-body offset-md-2 col-md-8">
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <div class="checkbox">
                                            <label class="ui-check ui-check-md">
                                                <input type="checkbox">
                                                <i class="dark-white"></i>
                                                Création en rafale
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label for="refArticle">Référence</label>
                                        <input type="text" id="refArticle" name="refArticle"
                                            class="form-control coderef @error('refArticle') is-invalid @enderror"
                                            placeholder="Référence article" value="{{ old('refArticle') }}"
                                            onkeyup="this.value = this.value.toUpperCase();">
                                        @error('refArticle') <span class="text-danger" role="alert">
                                            {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="designation">Désignation</label>
                                        <input type="text" id="designation" name="designation" value="{{ old('designation') }}"
                                            class="form-control @error('designation') is-invalid @enderror"
                                            placeholder="Désignation de l'article"
                                            onkeyup="this.value = this.value.toUpperCase();">
                                        @error('designation') <span class="text-danger" role="alert">
                                            {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="codeFamille">Famille</label>
                                        <select id="select2-single" type="text" name="codeFamille"
                                            class="form-control select2 @error('codeFamille') is-invalid @enderror"
                                            data-plugin="select2" style="width: 100%">
                                            <option value="">Sélectionnez la famille</option>
                                            @foreach ($FamilleList as $item)
                                            <option
                                                value="{{ $item->id }}|{{ $item->CptVente }}|{{ $item->CptAchat }}|{{ $item->CptStock }}|{{ $item->SuiviId }}|{{ $item->CodeSuivi }}|{{ $item->StockNegatif }}|{{ $item->UniteId }}">
                                                {{ $item->CodeFamille }}</option>
                                            @endforeach
                                        </select>
                                        @error('codeFamille') <span class="text-danger" role="alert">
                                            {{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label for="type">Type</label>
                                        <input type="text" id="type" name="type" value="{{ old('type') }}"
                                            class="form-control @error('type') is-invalid @enderror"
                                            placeholder="Type de l'article"
                                            onkeyup="this.value = this.value.toUpperCase();">
                                        @error('type') <span class="text-danger" role="alert">
                                            {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="marque">Marque</label>
                                        <input type="text" id="marque" name="marque" value="{{ old('marque') }}"
                                            class="form-control @error('marque') is-invalid @enderror"
                                            placeholder="Marque de l'article"
                                            onkeyup="this.value = this.value.toUpperCase();">
                                        @error('marque') <span class="text-danger" role="alert">
                                            {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="nature">Nature</label>
                                        <input type="text" id="nature" name="nature" value="{{ old('nature') }}"
                                            class="form-control @error('nature') is-invalid @enderror"
                                            placeholder="Nature de l'article"
                                            onkeyup="this.value = this.value.toUpperCase();">
                                        @error('nature') <span class="text-danger" role="alert">
                                            {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="codeBarre">Code barre</label>
                                        <input type="text" id="codeBarre" name="codeBarre" value="{{ old('codeBarre') }}"
                                            class="form-control @error('codeBarre') is-invalid @enderror"
                                            placeholder="Code barre"
                                            onkeyup="this.value = this.value.toUpperCase();">
                                        @error('codeBarre') <span class="text-danger" role="alert">
                                            {{ $message }}</span>
                                        @enderror
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </form>
                <div style="bottom:0;" class="mt-auto">
                    <div class="p-3 b-t">
                        <a href="#" class="text-muted">
                            <i class="fa fa-fw fa-twitter"></i> Twitter</a>
                        <a href="#" class="text-muted m-h">
                            <i class="fa fa-fw fa-facebook"></i> Facebook</a>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- Modal->Suppression-->
    <div id="m-a-a" class="modal black-overlay" data-backdrop="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="deleteForm" action="" method="POST">
                    @csrf
                    @method('delete')
                    <div class="modal-header">
                        <h5 class="modal-title">Confirmation de suppression</h5>
                    </div>
                    <div class="modal-body text-center p-lg">
                        {{-- <input type="text" id="id" name='id'> --}}
                        <p>Êtes-vous sûr de vouloir supprimer cet élément ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Non</button>
                        <button type="submit" class="btn danger p-x-md">Oui, Supprimer !</button>
                    </div>
                </form>
            </div>
            <!-- Modal-content -->
        </div>
    </div>
</div>

@endsection

@section('scripts')


<script>
    //Set tooltip and initialize datatable
    $(document).ready(function () {
        $("[data-tt=tooltip]").tooltip();
        $(".coderef").alphanum({
            allow              : '._-/',
            disallow           : '&é(èçà)ù^$%!:²',
            allowSpace         : false,
            allowNumeric       : true,
            allowUpper         : true,
            allowLower         : true,
            allowCaseless      : true,
           //allowLatin         : true,
           //allowOtherCharSets : true,
            //forceUpper         : true,
           // forceLower         : false,
            maxLength          : 20
        });
    });

    //For delete
    $(document).on('click', '#btnDelete', function () {
        let id = $(this).attr('data-id');
        console.log(id);
        $('#id').val(id);
        document.forms.deleteForm.action = '/client/' + id;
    });

    $(function () {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#addForm").validate({
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                refArticle: "required",
                designation: "required",
                codeFamille: "required",
            },
            // Specify validation error messages
            messages: {
                refArticle: "Veuillez renseigner la référence article",
                designation: "Veuillez renseigner la désignation",
                codeFamille: "Veuillez renseigner le code de la famille",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function (form) {

                form.submit();
            }
        });
    });


</script>

@if($errors->has('refArticle') || $errors->has('designation') || $errors->has('codeFamille'))
<script>
    $(function () {
        $('#bottom').modal({
            show: true
        });
    });

</script>
@endif
@endsection
