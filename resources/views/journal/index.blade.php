@extends('layouts.admin')
@section('title')
    Liste des journaux | Lite - Gestion commerciale
@endsection
@section('navtitle')
    Paramètres généraux - Journal
@endsection

@section('content')
    <div class="padding">
        <p class="btn-groups" style="text-align:right">
            <a style="text-align:left; float: left;" class="btn btn-rounded white" data-tt="tooltip" title="Actualiser"
                href="{{ route('journal.index') }}">
                <i class="fa fa-refresh"></i>
            </a>
            <button class="btn btn-rounded primary" data-toggle="modal" data-target="#bottom">
                <i class="fa fa-plus"></i> Ajouter
            </button>
            <button class="btn btn-outline btn-rounded white b-black text-black">
                <i class="fa fa-print"></i> Imprimer
            </button>
            <button class="btn btn-outline btn-rounded b-warning text-warning">
                <i class="fa fa-file-import"></i> Importer
            </button>
            <button class="btn btn-outline btn-rounded b-success text-success">
                <i class="fa fa-file-export"></i> Exporter
            </button>
        </p>

        @if (session()->has('successMessage'))
            <div class="alert alert-success" role="alert">
                {{ session('successMessage') }}
            </div>
        @endif
        @if (session()->has('errorMessage'))
            <div class="alert alert-danger" role="alert">
                {{ session('errorMessage') }}
            </div>
        @endif
        <div class="table-responsive">
            <table id="dtable" class="table table-sm v-middle p-0 m-0 box">
                <thead>
                    <tr>
                        <th>CODE JOURNAL</th>
                        <th>JOURNAL</th>
                        <th>TYPE</th>
                        <th>N° COMPTE</th>
                        <th style="text-align:right"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($JournalList as $item)
                        <tr>
                            <td class="">{{ $item->CodeJournal }}</td>
                            <td class="">{{ $item->LibelleJournal }}</td>
                            <td class="">
                                @if ($item->TypeJournal == 0)
                                    ACHAT
                                @endif
                                @if ($item->TypeJournal == 1)
                                    VENTE
                                @endif
                                @if ($item->TypeJournal == 2)
                                    TRÉSORERIE
                                @endif
                                @if ($item->TypeJournal == 3)
                                    GÉNÉRAL
                                @endif
                            </td>
                            <td class="">{{ $item->NoCompte }}</td>
                            <td class="" style="text-align:right">
                                <a href="{{ route('journal.edit', $item->id) }}" data-tt="tooltip" class="btn btn-sm"> <i
                                        class="fa fa-pencil default"></i></a>
                                {{-- <a id="btnEdit"
                                    data-ncompte="{{ $item->NoCompte ? $item->NoCompte : 'null' }}"
                                    data-type="{{ $item->TypeJournal ? $item->TypeJournal : 'null' }}"
                                    class="btn btn-sm black edit" data-toggle="modal" data-target="#bottom2"
                                    title="Modifier" data-tt="tooltip" href="{{ $item->id }}">
                                    <i class="fa fa-pencil default"></i>

                                </a> --}}
                                <a id="btnDelete" class="btn btn-sm text-danger" data-toggle="modal" data-target="#m-a-a"
                                    data-id='{{ $item->id }}' title="Supprimer" data-tt="tooltip" href="{{ $item->id }}"><i
                                        class="fa fa-trash text-danger"></i></a>

                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!--Modal-->
        <div class="modal fade" id="bottom" data-backdrop="true" aria-hidden="true">
            <div class="modal-dialog modal-bottom white b-t" style="height:75%">
                <div class="d-flex flex-column h-100">
                    <form id="addForm" action="{{ route('journal.store') }}" method="POST">
                        @csrf
                        <div class="p-3 b-b d-flex no-shrink">
                            <h5 class="h5 m-0">Ajout d'un journal</h5>
                            <div class="ml-auto">
                                <button id="formSubmit" type="submit" class="btn btn-rounded primary">
                                    Enrégistrer
                                </button>
                                <button data-dismiss="modal" class="btn btn-rounded danger">
                                    Annuler
                                </button>
                            </div>
                        </div>
                        <div class="scroll-y">
                            <!--Contenu du modal-->
                            <div class="box">
                                <div class="box-body offset-md-2 col-md-8">
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <label for="codeJournal">Code du Journal</label>
                                            <input type="text" id="codeJournal" name="codeJournal"
                                                class="form-control codejournal @error('codeJournal') is-invalid @enderror"
                                                placeholder="Code du Journal (03 caractères maximum)"
                                                onkeyup="this.value = this.value.toUpperCase();">
                                            @error('codeJournal') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-9">
                                            <label for="libelleJournal">Intitulé</label>
                                            <input type="text" id="libelleJournal" name="libelleJournal"
                                                class="form-control @error('libelleJournal') is-invalid @enderror"
                                                placeholder="Nom du journal"
                                                onkeyup="this.value = this.value.toUpperCase();">
                                            @error('libelleJournal') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="typeJournal">Type de journal</label>
                                            <select id="select2-single" name="typeJournal"
                                                class="form-control select2 journal @error('typeJournal') is-invalid @enderror"
                                                data-plugin="select2" style="width: 100%" onchange="showNCompte(this);">
                                                <option value="">Sélectionnez le type</option>
                                                <option value="0">ACHAT</option>
                                                <option value="1">VENTE</option>
                                                <option value="2">TRÉSORERIE</option>
                                                <option value="3">GÉNÉRAL</option>
                                            </select>
                                            @error('typeJournal') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group classcompte col-md-6" style="display: none;">
                                            <label for="noCompte">N° Compte</label>
                                            <select id="select2-single" name="noCompte"
                                                class="form-control select2   @error('noCompte') is-invalid @enderror"
                                                data-plugin="select2" style="width: 100%">
                                                <option value="">Sélectionnez le compte</option>
                                                @foreach ($NCompteList as $item)
                                                    <option value="{{ $item->NoCompte }}">
                                                        {{ $item->NoCompte . ' | ' . $item->IntituleCompte }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('noCompte') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                    <div style="bottom:0;" class="mt-auto">
                        <div class="p-3 b-t">
                            <a href="#" class="text-muted">
                                <i class="fa fa-fw fa-twitter"></i> Twitter</a>
                            <a href="#" class="text-muted m-h">
                                <i class="fa fa-fw fa-facebook"></i> Facebook</a>

                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!--Modal->Modification-->
        <div class="modal fade" id="bottom2" data-backdrop="true" aria-hidden="true">
            <div class="modal-dialog modal-bottom white b-t" style="height:75%">
                <div class="d-flex flex-column h-100">
                    <form id="editForm" action="" method="POST">
                        @csrf
                        @method('put')
                        <div class="p-3 b-b d-flex no-shrink">
                            <h5 class="h5 m-0">Modification du journal</h5>
                            <div class="ml-auto">
                                <button id="formSubmit" type="submit" class="btn btn-rounded primary">
                                    Enrégistrer
                                </button>
                                <button data-dismiss="modal" class="btn btn-rounded danger">
                                    Annuler
                                </button>
                            </div>

                        </div>
                        <div class="scroll-y">
                            <!--Contenu du modal-->
                            <div class="box">
                                <div class="box-body offset-md-2 col-md-8">

                                    <div class="form-row">
                                        <input type="hidden" name="paysid" id="paysid">
                                        <div class="form-group col-md-6">
                                            <label for="libelleJournal2">Nom du journal</label>
                                            <input type="text" id="libelleJournal2" name="libelleJournal2"
                                                value="{{ old('libelleJournal2') }}"
                                                class="form-control @error('libelleJournal2') is-invalid @enderror"
                                                placeholder="Nom du journal"
                                                onkeyup="this.value = this.value.toUpperCase();">
                                            @error('libelleJournal2') <span class="text-danger" role="alert">
                                                {{ $message }}</span>@enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="codeJournal2">Code du journal</label>
                                            <input type="text" id="codeJournal2" name="codeJournal2"
                                                value="{{ old('codeJournal2') }}"
                                                class="form-control @error('codeJournal2') is-invalid @enderror"
                                                placeholder="Code du journal (03 caractères maximum)"
                                                onkeyup="this.value = this.value.toUpperCase();">
                                            @error('codeJournal2') <span class="text-danger" role="alert">
                                                {{ $message }}</span>@enderror
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="typeJournal2">Type de journal</label>
                                            <select id="select2-single" name="typeJournal2"
                                                class="form-control select2 journal @error('typeJournal2') is-invalid @enderror"
                                                data-plugin="select2" style="width: 100%" onchange="showNCompte(this);">
                                                <option value="">Sélectionnez le type</option>
                                                <option value="0">ACHAT</option>
                                                <option value="1">VENTE</option>
                                                <option value="2">TRÉSORERIE</option>
                                                <option value="3">GÉNÉRAL</option>
                                            </select>
                                            @error('typeJournal') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group classcompte col-md-6" style="display: none;">
                                            <label for="noCompte">N° Compte</label>
                                            <select id="select2-single" name="noCompte"
                                                class="form-control select2   @error('noCompte') is-invalid @enderror"
                                                data-plugin="select2" style="width: 100%">
                                                <option value="">Sélectionnez le compte</option>
                                                @foreach ($NCompteList as $item)
                                                    <option value="{{ $item->NoCompte }}">
                                                        {{ $item->NoCompte . ' | ' . $item->IntituleCompte }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('noCompte') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </form>
                    <div style="bottom:0;" class="mt-auto">
                        <div class="p-3 b-t">
                            <a href="#" class="text-muted">
                                <i class="fa fa-fw fa-twitter"></i>Twitter</a>
                            <a href="#" class="text-muted m-h">
                                <i class="fa fa-fw fa-facebook"></i>Facebook</a>

                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!-- Modal->Suppression-->
        <div id="m-a-a" class="modal black-overlay" data-backdrop="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="deleteForm" action="" method="POST">
                        @csrf
                        @method('delete')
                        <div class="modal-header">
                            <h5 class="modal-title">Confirmation de suppression</h5>
                        </div>
                        <div class="modal-body text-center p-lg">

                            <p>Êtes-vous sûr de vouloir supprimer cet élément ?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Non</button>
                            <button type="submit" class="btn danger p-x-md">Oui, Supprimer !</button>
                        </div>
                    </form>
                </div>
                <!-- Modal-content -->
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        //Set tooltip
        $(document).ready(function() {
            $("[data-tt=tooltip]").tooltip();
            table = $('#dtable').DataTable();
            $(".codejournal").alphanum({
            allow              : '._-/',
            disallow           : '&é(èçà)ù^$%!:²',
            allowSpace         : false,
            allowNumeric       : true,
            allowUpper         : true,
            allowLower         : true,
            allowCaseless      : true,
           //allowLatin         : true,
           //allowOtherCharSets : true,
            //forceUpper         : true,
           // forceLower         : false,
            maxLength          : 3
        });
        });

        //For update
        $('body').on('click', '#btnEdit', function() {
            //to get currently clicked row object
            var row = $(this).parents('tr')[0];
            //for row data
            $('#libelleJournal2').val(table.row(row).data()[1]);
            $('#codeJournal2').val(table.row(row).data()[2]);
            $('#typeJournal2').val(table.row(row).data()[3]);
            let type = $(this).attr('data-type');
            let ncompte = $(this).attr('data-ncompte');
            console.log(type);


            if (ncompte != null) {
                $(".ncompte  > option[value=" + ncompte + "]").attr("selected", true);
            }

            if (type != null) {
                $(".journal  > option[value=" + type + "]").attr("selected", true);
            }

            //$('#noCompte2').val(table.row(row).data()[4]);
            var rowId = table.row(row).data()[0];
            document.forms.editForm.action = '/journal/' + rowId;
        });

        //For delete
        $(document).on('click', '#btnDelete', function() {
            let id = $(this).attr('data-id');
            $('#id').val(id);
            document.forms.deleteForm.action = '/journal/' + id;
        });

        function showNCompte(that) {
            if (that.value == 2 || that.value == 3) {
                document.getElementsByClassName('classcompte')[0].style.display = 'block';
            } else {
                document.getElementsByClassName('classcompte')[0].style.display = 'none';
            }
        }

        $(function () {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#addForm").validate({
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                libelleJournal: "required",
                typeJournal: "required",
                noCompte: "required",
                codeJournal: {
                    required: true,
                    maxlength: 3
                }
            },
            // Specify validation error messages
            messages: {
                libelleJournal: "Veuillez renseigner le libellé du journal",
                typeJournal: "Veuillez renseigner le type du journal",
                noCompte: "Veuillez renseigner le numéro de compte",
                codeJournal: {
                    required: "Veuillez renseigner le code du  journal",
                    maxlength: "Le code du journal doit etre 3 caratères au maximum"
                },
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function (form) {

                form.submit();
            }
        });
    });

    </script>

    @if ($errors->has('libelleJournal') || $errors->has('codeJournal') || $errors->has('typeJournal') || $errors->has('noCompte'))
        <script>
            $(function() {
                $('#bottom').modal({
                    show: true
                });
            });

        </script>
    @endif

    @if ($errors->has('libelleJournal2') || $errors->has('codeJournal2') || $errors->has('typeJournal2') || $errors->has('noCompte2'))
        <script>
            $(function() {
                $('#bottom2').modal({
                    show: true
                });
            });

        </script>
    @endif
@endsection
