@extends('layouts.admin')
@section('title')
    Liste des journaux | Lite - Gestion commerciale
@endsection
@section('navtitle')
    Paramètres généraux - Modification du journal {{ $tjournal->LibelleJournal }}
@endsection


@section('content')
    <div class="padding">
        <div class="box">
            <div class="box-body">
                <form id="editForm" action="{{ route('journal.update', $tjournal->id) }}" method="POST">
                    @csrf
                    @method('put')
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="codeJournal2">Code journal</label>
                            <input type="text" id="codeJournal2" name="codeJournal2" value="{{ $tjournal->CodeJournal }}"
                                class="form-control form-control-sm @error('codeJournal2') is-invalid @enderror"
                                placeholder="Code du journal (05 caractères maximum)"
                                onkeyup="this.value = this.value.toUpperCase();" readonly>
                            @error('codeJournal2') <span class="text-danger" role="alert">
                                {{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="libelleJournal2">Nom du journal</label>
                            <input type="text" id="libelleJournal2" name="libelleJournal2"
                                value="{{ $tjournal->LibelleJournal }}"
                                class="form-control form-control-sm @error('libelleJournal2') is-invalid @enderror"
                                placeholder="Nom du journal" onkeyup="this.value = this.value.toUpperCase();">
                            @error('libelleJournal2') <span class="text-danger" role="alert">
                                {{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="typeJournal2">Type journal</label>
                            <select id="typeJournal2" name="typeJournal2"
                                class="form-control form-control-sm select2 journal @error('typeJournal2') is-invalid @enderror"
                                data-plugin="select2" style="width: 100%" onchange="showNCompte(this);" disabled>
                                <option value="">Sélectionnez le type</option>
                                <option value="0" {{ $tjournal->TypeJournal == 0 ? 'selected' : '' }}>ACHAT</option>
                                <option value="1" {{ $tjournal->TypeJournal == 1 ? 'selected' : '' }}>VENTE</option>
                                <option value="2" {{ $tjournal->TypeJournal == 2 ? 'selected' : '' }}>TRÉSORERIE
                                </option>
                                <option value="3" {{ $tjournal->TypeJournal == 3 ? 'selected' : '' }}>GÉNÉRAL
                                </option>
                            </select>
                            @error('typeJournal') <span class="text-danger" role="alert">
                                    {{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group classcompte col-md-6" style="display: none;">
                            <label for="noCompte">N° Compte</label>
                            <select id="select2-single" name="noCompte3"
                                class="form-control form-control-sm select2   @error('noCompte') is-invalid @enderror"
                                data-plugin="select2" style="width: 100%" disabled>
                                <option value="">Sélectionnez le compte</option>
                                @foreach ($NCompteList as $item)
                                    <option value="{{ $item->NoCompte }}"
                                        {{ $item->NoCompte == $tjournal->NoCompte ? 'selected' : '' }}>
                                        {{ $item->NoCompte . ' | ' . $item->IntituleCompte }}
                                    </option>
                                @endforeach
                            </select>
                            @error('noCompte') <span class="text-danger" role="alert">
                                    {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <button class="btn btn-outline btn-rounded b-warning text-warning">Modifier</button>
                    <a href="{{ route('journal.index') }}"
                        class="btn btn-outline btn-rounded b-dark text-secondary">Annuler</a>
                </form>
            </div>

        </div>
    </div>
@endsection

@section('scripts')


    <script>
        //Set tooltip
        $(document).ready(function() {
            $("[data-tt=tooltip]").tooltip();
            table = $('#dtable').DataTable();

            let type = $(".journal option:selected").val();

            if (type == 2 || type == 3) {

                document.getElementsByClassName('classcompte')[0].style.display = 'block';
            } else {
                document.getElementsByClassName('classcompte')[0].style.display = 'none';
            }

        });

        //For update
        $('body').on('click', '#btnEdit', function() {
            //to get currently clicked row object
            var row = $(this).parents('tr')[0];
            //for row data
            $('#libelleJournal2').val(table.row(row).data()[1]);
            $('#codeJournal2').val(table.row(row).data()[2]);
            $('#typeJournal2').val(table.row(row).data()[3]);

            let type = $(this).attr('data-type');
            let ncompte = $(this).attr('data-ncompte');
            console.log(type);


            if (ncompte != null) {
                $(".ncompte  > option[value=" + ncompte + "]").attr("selected", true);
            }

            if (type != null) {
                $(".journal  > option[value=" + type + "]").attr("selected", true);
            }

            //$('#noCompte2').val(table.row(row).data()[4]);
            var rowId = table.row(row).data()[0];
            document.forms.editForm.action = '/journal/' + rowId;
        });

        //For delete
        $(document).on('click', '#btnDelete', function() {
            let id = $(this).attr('data-id');
            $('#id').val(id);
            document.forms.deleteForm.action = '/journal/' + id;
        });

        function showNCompte(that) {
            if (that.value == 2 || that.value == 3) {

                document.getElementsByClassName('classcompte')[0].style.display = 'block';
            } else {
                document.getElementsByClassName('classcompte')[0].style.display = 'none';
            }


        }

    </script>

    @if ($errors->has('libelleJournal') || $errors->has('codeJournal') || $errors->has('typeJournal') || $errors->has('noCompte'))
        <script>
            $(function() {
                $('#bottom').modal({
                    show: true
                });
            });

        </script>
    @endif

    @if ($errors->has('libelleJournal2') || $errors->has('codeJournal2') || $errors->has('typeJournal2') || $errors->has('noCompte2'))
        <script>
            $(function() {
                $('#bottom2').modal({
                    show: true
                });
            });

        </script>
    @endif
@endsection
