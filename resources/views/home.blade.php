@extends('layouts.admin')
@section('title')
Accueil | Lite - Gestion commerciale
@endsection
@section('navtitle')
Accueil
@endsection

@section('content')
<div>
    <div class="row no-gutters">
        <div class="col-6 col-sm-6 col-md-3 light">
            <div class="padding">
                <h5 class="_600">
                    <span class="float-right text-muted">
                        <i class="fa fa-repeat text-sm text-muted"></i>
                    </span>
                    <span>120</span>
                </h5>
                <div class="text-muted mb-4">Visits</div>
                <div class="py-3 b-b">
                    <span class="float-right text-md text-primary">+45% <i class="fa fa-caret-up"></i></span>
                    <span class="_600 d-block">542</span>
                    <small class="text-muted">People</small>
                </div>
                <div class="py-3">
                    <span class="float-right text-md text-warning">25% <i class="fa fa-caret-down"></i></span>
                    <span class="_600 d-block">235</span>
                    <small class="text-muted">Active</small>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-6 col-md-3 light">
            <div class="padding">
                <h5 class="_600">
                    <span class="float-right text-muted">
                        <i class="fa fa-repeat text-sm text-muted"></i>
                    </span>
                    <span>45</span>
                </h5>
                <p class="text-muted mb-4">New Projects</p>
                <div class="py-3">
                    <canvas id="chart-doughnut-2" data-plugin="chart" height="100"></canvas>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-6 col-md-3 light">
            <div class="padding">
                <h5 class="_600">
                    <span class="float-right text-muted">
                        <i class="fa fa-repeat text-sm text-muted"></i>
                    </span>
                    <span>350</span>
                </h5>
                <div class="text-muted mb-4">Clients</div>
                <div class="py-3">
                    <canvas id="chart-bar-2"></canvas>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-6 col-md-3 light">
            <div class="padding">
                <h5 class="_600">
                    <span class="float-right text-muted">
                        <i class="fa fa-repeat text-sm text-muted"></i>
                    </span>
                    <span>579</span>
                </h5>
                <div class="text-muted mb-4">Tasks</div>
                <div class="py-3">
                    <canvas id="chart-line-2"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="row no-gutters box">
        <div class="col-md-9 lt p-3">
            {{-- <div id="jqvmap-world" data-plugin="vectorMap" style="height:280px"></div> --}}
            <div class="padding">
                <h6>Gestion des ventes</h6>
                <p class="text-muted my-3">Établissement de documents de vente, Interrogation des clients etc...</p>
                <button class="md-btn md-fab mb-2 primary">
                    <i class="fa fa-plus"></i>
                </button>
                <button class="md-btn md-fab mb-2 pink">
                    <i class="fa fa-search"></i>
                </button>
                <button class="md-btn md-fab mb-2 white">
                    <i class="fa fa-users"></i>
                </button>
            </div>
            <div class="padding">
                <h6>Gestion des achats</h6>
                <p class="text-muted my-3">Établissement de documents d'achats, Interrogation des fournisseurs etc...</p>
                <button class="md-btn md-fab mb-2 primary">
                    <i class="fa fa-plus"></i>
                </button>
                <button class="md-btn md-fab mb-2 pink">
                    <i class="fa fa-search"></i>
                </button>
                <button class="md-btn md-fab mb-2 white">
                    <i class="fa fa-users"></i>
                </button>
            </div>
        </div>
        <div class="col-md-3 light">
            <div class="padding">
                <h6 class="mb-2">Global sales</h6>
                <p class="text-sm">
                    <i class="fa fa-caret-down text-warn"></i> <span class="text-muted">Min:</span>
                    $39,050
                    <i class="fa fa-caret-up text-success ml-2"></i> <span
                        class="text-muted">Max:</span> $78,560
                </p>
                <div>
                    <div class="p-1">
                        <small>
                            <span class="float-right text-muted">45%</span> USA
                        </small>
                        <div class="progress my-1" style="height:6px;">
                            <div class="progress-bar primary" style="width: 45%"></div>
                        </div>
                    </div>
                    <div class="p-1">
                        <small>
                            <span class="float-right text-muted">25%</span> Germany
                        </small>
                        <div class="progress my-1" style="height:6px;">
                            <div class="progress-bar primary" style="width: 25%"></div>
                        </div>
                    </div>
                    <div class="p-1">
                        <small>
                            <span class="float-right text-muted">15%</span> France
                        </small>
                        <div class="progress my-1" style="height:6px;">
                            <div class="progress-bar primary" style="width: 15%"></div>
                        </div>
                    </div>
                    <div class="p-1">
                        <small>
                            <span class="float-right text-muted">5%</span> Other
                        </small>
                        <div class="progress my-1" style="height:6px;">
                            <div class="progress-bar grey lt" style="width: 5%"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="row no-gutters">
            <div class="col-md-4">
                <div class="padding">
                    <h6>Sales Statistical Overview</h6>
                    <p class="text-muted my-3">Sale information on advertising, exhibitions, market
                        research, online media, customer desires, PR and much more</p>
                    <p>
                        <i class="fa fa-arrow-circle-o-up text-success mr-1"></i><span class="text-success">15%</span>
                        more than last week</p>
                    <a href="#" class="btn btn-sm btn-rounded success">Update</a>
                    <a href="#" class="btn btn-sm btn-rounded white">See detail</a>
                </div>
            </div>
            <div class="col-md-8">
                <div class="padding">
                    <canvas id="chart-line" height="120">
                    </canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="row no-gutters align-items-stretch">
        <div class="col-md-4 white lt">
            <div class="padding">
                <span class="badge success float-right">5</span>
                <h6 class="mb-3">Activity</h6>
                <div class="streamline streamline-dotted">
                    <div class="sl-item  b- ">
                        <div class="sl-left">
                            <img src="../assets/images/a9.jpg" class="circle" alt=".">
                        </div>
                        <div class="sl-content">
                            <span class="sl-date text-muted">15:00</span>
                            <div>
                                <a href="#" class="text-primary">Pablo Nouvelle</a>
                                Added API call to update track element
                            </div>
                        </div>
                    </div>
                    <div class="sl-item  b- ">
                        <div class="sl-left">
                            <img src="../assets/images/a8.jpg" class="circle" alt=".">
                        </div>
                        <div class="sl-content">
                            <span class="sl-date text-muted">14:00</span>
                            <div>
                                <a href="#" class="text-primary">RYD</a>
                                Add inline SVG icon
                            </div>
                        </div>
                    </div>
                    <div class="sl-item  b- ">
                        <div class="sl-left">
                            <img src="../assets/images/a1.jpg" class="circle" alt=".">
                        </div>
                        <div class="sl-content">
                            <span class="sl-date text-muted">July 21</span>
                            <div>
                                <a href="#" class="text-primary">Summerella</a>
                                Submit a support ticket
                            </div>
                        </div>
                    </div>
                    <div class="sl-item  b- ">
                        <div class="sl-left">
                            <img src="../assets/images/a5.jpg" class="circle" alt=".">
                        </div>
                        <div class="sl-content">
                            <span class="sl-date text-muted">09:50</span>
                            <div>
                                <a href="#" class="text-primary">Radionomy</a>
                                Was added to Repo
                            </div>
                        </div>
                    </div>
                    <div class="sl-item  b- ">
                        <div class="sl-content">
                            <span class="sl-date text-muted">13:00</span>
                            <div>
                                <a href="#" class="text-primary">Garrett Winters</a>
                                Followed by Jessic
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 light bg">
            <div class="padding">
                <h6 class="mb-3">Tasks</h6>
                <div class="streamline streamline-dotted">
                    <div class="sl-item  b- ">
                        <div class="sl-content">
                            <span class="sl-date text-muted">05:35</span>
                            <div>
                                <a href="#" class="text-primary">Fifth Harmony</a>
                                Bug fixes
                            </div>
                        </div>
                    </div>
                    <div class="sl-item  b-success ">
                        <div class="sl-content">
                            <span class="sl-date text-muted">09:05</span>
                            <div>
                                <a href="#" class="text-primary">Jeremy Scott</a>
                                Assign you a task
                            </div>
                        </div>
                    </div>
                    <div class="sl-item  b-info ">
                        <div class="sl-content">
                            <span class="sl-date text-muted">12:05</span>
                            <div>
                                <a href="#" class="text-primary">Judith Garcia</a>
                                Follow up to close deal
                            </div>
                        </div>
                    </div>
                    <div class="sl-item  b-primary ">
                        <div class="sl-content">
                            <span class="sl-date text-muted">13:00</span>
                            <div>
                                <a href="#" class="text-primary">Garrett Winters</a>
                                Followed by Jessic
                            </div>
                        </div>
                    </div>
                    <div class="sl-item  b- ">
                        <div class="sl-content">
                            <span class="sl-date text-muted">11:30</span>
                            <div>
                                <a href="#" class="text-primary">Ashton Cox</a>
                                New feedback from John
                            </div>
                        </div>
                    </div>
                </div>
                <div class="p-2">
                    <a href="#" class="btn btn-sm btn-block white">Load More</a>
                </div>
            </div>
        </div>
        <div class="col-md-4 dk">
            <div class="padding">
                <h6>Friends</h6>
                <div class="list inset">
                    <div class="list-item " data-id="item-2">
                        <span class="w-24 avatar circle light-blue">
                            <img src="../assets/images/a2.jpg" alt=".">
                        </span>
                        <div class="list-body">
                            <a href="" class="item-title _500">Kygo</a>
                            <div class="item-tag tag hide">
                            </div>
                        </div>
                        <div>
                            <div class="item-action dropdown">
                                <a href="#" data-toggle="dropdown" class="text-muted">
                                    <i class="fa fa-fw fa-ellipsis-v"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right text-color" role="menu">
                                    <a class="dropdown-item">
                                        <i class="fa fa-tag"></i>
                                        Action
                                    </a>
                                    <a class="dropdown-item">
                                        <i class="fa fa-pencil"></i>
                                        Another action
                                    </a>
                                    <a class="dropdown-item">
                                        <i class="fa fa-reply"></i>
                                        Something else here
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item">
                                        <i class="fa fa-ellipsis-h"></i>
                                        Separated link
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="list-item " data-id="item-8">
                        <span class="w-24 avatar circle teal">
                            <img src="../assets/images/a8.jpg" alt=".">
                        </span>
                        <div class="list-body">
                            <a href="" class="item-title _500">RYD</a>
                            <div class="item-tag tag hide">
                            </div>
                        </div>
                        <div>
                            <div class="item-action dropdown">
                                <a href="#" data-toggle="dropdown" class="text-muted">
                                    <i class="fa fa-fw fa-ellipsis-v"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right text-color" role="menu">
                                    <a class="dropdown-item">
                                        <i class="fa fa-tag"></i>
                                        Action
                                    </a>
                                    <a class="dropdown-item">
                                        <i class="fa fa-pencil"></i>
                                        Another action
                                    </a>
                                    <a class="dropdown-item">
                                        <i class="fa fa-reply"></i>
                                        Something else here
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item">
                                        <i class="fa fa-ellipsis-h"></i>
                                        Separated link
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="list-item " data-id="item-15">
                        <span class="w-24 avatar circle indigo">
                            GW
                        </span>
                        <div class="list-body">
                            <a href="" class="item-title _500">Garrett Winters</a>
                            <div class="item-tag tag hide">
                            </div>
                        </div>
                        <div>
                            <div class="item-action dropdown">
                                <a href="#" data-toggle="dropdown" class="text-muted">
                                    <i class="fa fa-fw fa-ellipsis-v"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right text-color" role="menu">
                                    <a class="dropdown-item">
                                        <i class="fa fa-tag"></i>
                                        Action
                                    </a>
                                    <a class="dropdown-item">
                                        <i class="fa fa-pencil"></i>
                                        Another action
                                    </a>
                                    <a class="dropdown-item">
                                        <i class="fa fa-reply"></i>
                                        Something else here
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item">
                                        <i class="fa fa-ellipsis-h"></i>
                                        Separated link
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="list-item " data-id="item-11">
                        <span class="w-24 avatar circle blue">
                            TN
                        </span>
                        <div class="list-body">
                            <a href="" class="item-title _500">Tiger Nixon</a>
                            <div class="item-tag tag hide">
                            </div>
                        </div>
                        <div>
                            <div class="item-action dropdown">
                                <a href="#" data-toggle="dropdown" class="text-muted">
                                    <i class="fa fa-fw fa-ellipsis-v"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right text-color" role="menu">
                                    <a class="dropdown-item">
                                        <i class="fa fa-tag"></i>
                                        Action
                                    </a>
                                    <a class="dropdown-item">
                                        <i class="fa fa-pencil"></i>
                                        Another action
                                    </a>
                                    <a class="dropdown-item">
                                        <i class="fa fa-reply"></i>
                                        Something else here
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item">
                                        <i class="fa fa-ellipsis-h"></i>
                                        Separated link
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="list-item " data-id="item-14">
                        <span class="w-24 avatar circle brown">
                            BW
                        </span>
                        <div class="list-body">
                            <a href="" class="item-title _500">Brielle Williamson</a>
                            <div class="item-tag tag hide">
                            </div>
                        </div>
                        <div>
                            <div class="item-action dropdown">
                                <a href="#" data-toggle="dropdown" class="text-muted">
                                    <i class="fa fa-fw fa-ellipsis-v"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right text-color" role="menu">
                                    <a class="dropdown-item">
                                        <i class="fa fa-tag"></i>
                                        Action
                                    </a>
                                    <a class="dropdown-item">
                                        <i class="fa fa-pencil"></i>
                                        Another action
                                    </a>
                                    <a class="dropdown-item">
                                        <i class="fa fa-reply"></i>
                                        Something else here
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item">
                                        <i class="fa fa-ellipsis-h"></i>
                                        Separated link
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="list-item " data-id="item-7">
                        <span class="w-24 avatar circle indigo">
                            <img src="../assets/images/a7.jpg" alt=".">
                        </span>
                        <div class="list-body">
                            <a href="" class="item-title _500">Fifth Harmony</a>
                            <div class="item-tag tag hide">
                            </div>
                        </div>
                        <div>
                            <div class="item-action dropdown">
                                <a href="#" data-toggle="dropdown" class="text-muted">
                                    <i class="fa fa-fw fa-ellipsis-v"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right text-color" role="menu">
                                    <a class="dropdown-item">
                                        <i class="fa fa-tag"></i>
                                        Action
                                    </a>
                                    <a class="dropdown-item">
                                        <i class="fa fa-pencil"></i>
                                        Another action
                                    </a>
                                    <a class="dropdown-item">
                                        <i class="fa fa-reply"></i>
                                        Something else here
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item">
                                        <i class="fa fa-ellipsis-h"></i>
                                        Separated link
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')


<script>
    //Set tooltip and initialize datatable
    $(document).ready(function () {
        $("[data-tt=tooltip]").tooltip();
        var toto={!! json_encode((string)Auth()->user()->email) !!};
        console.log(toto)
    });



</script>

@endsection
