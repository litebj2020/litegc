@extends('layouts.admin')
@section('title')
    Liste des journaux | Lite - Gestion commerciale
@endsection
@section('navtitle')
    Paramètres généraux - Societé
@endsection
<style>
    .file {
        visibility: hidden;
        position: absolute;
    }

</style>

@section('content')
    <div>
        <div class="d-sm-flex">

            <div class="w w-auto-xs light bg bg-auto-sm b-r">
                <div class="py-3">
                    <div class="nav-active-border left b-primary">
                        <ul class="nav flex-column nav-sm">
                            <li class="nav-item">
                                <a class="nav-link active" href="#" data-toggle="tab" data-target="#tab-1">Identfication</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " href="#" data-toggle="tab" data-target="#tab-2">Contacts et
                                    adresses</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " href="#" data-toggle="tab" data-target="#tab-3">Informations
                                    fiscales</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " href="#" data-toggle="tab" data-target="#tab-4">Monnaies et formats</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " href="#" data-toggle="tab" data-target="#tab-5">Controle de
                                    facturation</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " href="#" data-toggle="tab" data-target="#tab-6">Préference</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " href="#" data-toggle="tab" data-target="#tab-7">Logo</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col p-0">
                <form action="{{ route('societe.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="tab-content pos-rlt">
                        <div class="tab-pane active" id="tab-1">
                            <div class="p-4 b-b _600">Identification de la societé</div>
                            <div class="col-md-6 p-4">
                                <div class="form-group">
                                    <label>Type de societé</label>
                                    <select name="typeSociete" id="" class="form-control form-control-sm"
                                        style="height: 37px;">
                                        <option value="E.I"
                                            {{ $SocieteList && $SocieteList->TypeSociete == 'E.I' ? 'selected' : '' }}>
                                            E.I
                                        </option>
                                        <option value="E.T.S"
                                            {{ $SocieteList && $SocieteList->TypeSociete == 'E.T.S' ? 'selected' : '' }}>
                                            E.T.S</option>
                                        <option value="S.A.R.L"
                                            {{ ($SocieteList and $SocieteList->TypeSociete == 'S.A.R.L') ? 'selected' : '' }}>
                                            S.A.R.L
                                        </option>
                                        <option value="S.A.S"
                                            {{ ($SocieteList and $SocieteList->TypeSociete == 'S.A.S') ? 'selected' : '' }}>
                                            S.A.S</option>
                                        <option value="S.A"
                                            {{ ($SocieteList and $SocieteList->TypeSociete == 'S.A') ? 'selected' : '' }}>
                                            S.A
                                        </option>
                                        <option value="Autres"
                                            {{ ($SocieteList and $SocieteList->TypeSociete == 'Autres') ? 'selected' : '' }}>
                                            Autres</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Raison sociale</label>
                                    <input type="text" class="form-control form-control-sm" name="raisonSociale"
                                        value="{{ $SocieteList ? $SocieteList->RaisonSociale : '' }}" style="height: 37px;">
                                </div>
                                <div class="form-group">
                                    <label>Abrege</label>
                                    <input type="text" class="form-control form-control-sm" name="abrege"
                                        style="height: 37px;" value="{{ $SocieteList ? $SocieteList->Abrege : '' }}">
                                </div>
                                <div class="form-group">
                                    <label>Capital</label>
                                    <input type="number" class="form-control form-control-sm" name="capital"
                                        style="height: 37px;" value="{{ $SocieteList ? $SocieteList->Capital : '' }}">
                                </div>
                                <div class="form-group">
                                    <label>Activité</label>
                                    <input type="text" class="form-control form-control-sm" name="activite"
                                        style="height: 37px;" value="{{ $SocieteList ? $SocieteList->Activite : '' }}">
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-2">
                            <div class="p-4 b-b _600 my-4">Contacts</div>
                            <div class="form-row mx-3">
                                <div class="form-group col-md-6">
                                    <label>Mobile </label>
                                    <input type="text" class="form-control form-control-sm" name="contact1"
                                        style="height: 37px;" value="{{ $SocieteList ? $SocieteList->Contact1 : '' }}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Fixe</label>
                                    <input type="text" class="form-control form-control-sm" name="contact2"
                                        style="height: 37px;" value="{{ $SocieteList ? $SocieteList->Contact2 : '' }}">
                                </div>
                            </div>
                            <div class="form-row mx-3">
                                <div class="form-group col-md-6">
                                    <label>Fax</label>
                                    <input type="text" class="form-control form-control-sm" name="fax" style="height: 37px;"
                                        value="{{ $SocieteList ? $SocieteList->Fax : '' }}">
                                </div>
                                <div class="form- col-md-6">
                                    <label>Email</label>
                                    <input type="text" class="form-control form-control-sm" name="email"
                                        style="height: 37px;" value="{{ $SocieteList ? $SocieteList->Email : '' }}">
                                </div>
                            </div>
                            <div class="form-row mx-3">
                                <div class="form-group col-md-6">
                                    <label>Site Web</label>
                                    <input type="text" class="form-control form-control-sm" name="siteWeb"
                                        style="height: 37px;" value="{{ $SocieteList ? $SocieteList->SiteWeb : '' }}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Adresse</label>
                                    <input type="text" class="form-control form-control-sm" name="adresse"
                                        style="height: 37px;" value="{{ $SocieteList ? $SocieteList->Adresse : '' }}">
                                </div>
                            </div>
                            <div class="form-row mx-3">
                                <div class="form-group col-md-6">
                                    <label>Code Postal</label>
                                    <input type="text" class="form-control form-control" name="codePostal"
                                        style="height: 37px;" value="{{ $SocieteList ? $SocieteList->CodePostal : '' }}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Région</label>
                                    <input type="text" class="form-control form-control" name="region" style="height: 37px;"
                                        value="{{ $SocieteList ? $SocieteList->Region : '' }}">
                                </div>

                            </div>
                            <div class="form-row mx-3">

                                <div class="form-group col-md-6">
                                    <label>Pays</label>
                                    <select name="pays" id="pays" class="form-control form-control-sm select2 "
                                        data-plugin="select2" style="width: 100%">
                                        <option value="">Veuillez choisir le pays</option>
                                        @foreach ($PaysList as $item)
                                            <option value="{{ $item->NomPays }}"
                                                {{ ($SocieteList and $SocieteList->Pays == $item->NomPays) ? 'selected' : '' }}>
                                                {{ $item->CodePays . ' | ' . $item->NomPays }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Ville</label>
                                    <select name="ville" id="ville" class="form-control form-control-sm select2 "
                                        data-plugin="select2" style="width: 100%">
                                        <option value="">Veuillez choisir le pays</option>
                                        @foreach ($VilleList as $item)
                                            <option value="{{ $item->NomVille }}"
                                                {{ ($SocieteList and $SocieteList->Ville == $item->NomVille) ? 'selected' : '' }}>
                                                {{ $item->NomVille }}
                                            </option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-3">
                            <div class="p-4 b-b _600">Informations
                                fiscales</div>
                            <div class="col-md-6 p-4">

                                <div class="form-group">
                                    <label>Identifiant Fiscal</label>
                                    <input type="text" class="form-control form-control-sm" name="identifiantFiscal"
                                        value="{{ $SocieteList ? $SocieteList->IdentifiantFiscal : '' }}">
                                </div>
                                <div class="form-group">
                                    <label>Régistre de commerce</label>
                                    <input type="text" class="form-control form-control-sm" name="infoFiscal1"
                                        value="{{ $SocieteList ? $SocieteList->InfoFiscal1 : '' }}">
                                </div>
                                <div class="form-group">
                                    <label>Numéro fiscal</label>
                                    <input type="text" class="form-control form-control-sm" name="infoFiscal2"
                                        value="{{ $SocieteList ? $SocieteList->InfoFiscal2 : '' }}">
                                </div>

                            </div>
                        </div>
                        <div class="tab-pane" id="tab-4">
                            <div class="p-4 b-b _600">Monnaies et formats</div>
                            <div class="col-md-6 p-4">
                                <div class="form-group">
                                    <label>Devise</label>
                                    <select name="codeIso" id="" class="form-control form-control-sm">
                                        <option value="">Veuillez choisir</option>
                                        @foreach ($DeviseList as $item)
                                            <option value="{{ $item->id }}"
                                                {{ ($SocieteList and $SocieteList->DeviseId == $item->id) ? 'selected' : '' }}>
                                                {{ $item->Designation }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-5">
                            <div class="p-4 b-b _600">Controle de facturation</div>
                            <div class="p-4 col-md-6">
                                <label class="md-switch">
                                    <input type="checkbox" id="haveMcf" name="haveMcf"
                                        {{ ($SocieteList and $SocieteList->HaveMCF == 1) ? 'checked' : '' }}>
                                    <i class="blue"></i>
                                    Controle de facturation MCF
                                </label>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-6">
                            <div class="p-4 b-b _600">Préference</div>
                            <div class="p-4">
                                <div class="form-group col-md-6">
                                    <label>Complément</label>
                                    <input type="text" class="form-control form-control-sm" name="complement"
                                        value="{{ $SocieteList ? $SocieteList->Complement : '' }}">
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-7">
                            <div class="p-4 b-b _600">Logo</div>

                            <div class="p-4">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="customFile" name="logo">
                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        @if ($SocieteList and $SocieteList->Logo)
                                            <img src="{{ asset('uploads/' . $SocieteList->Logo) }}" height="300px"
                                                width="300px;" alt="logo">
                                        @endif
                                        <div class="ml-2 col-sm-6 preview" style="display: none">
                                            <img src="" id="preview" height="300px" width="300px;" class="img-thumbnail">
                                        </div>
                                    </div>
                                </div>



                            </div>

                        </div>
                        <button id="formSubmit" type="submit" class="btn btn-rounded primary mx-3 my-3">
                            Enrégistrer
                        </button>
                    </div>
                </form>
            </div>
        </div>

    </div>

@endsection

@section('scripts')
    <script>
        // Add the following code if you want the name of the file appear on select
        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });

        $(document).on("click", ".browse", function() {
            var file = $(this).parents().find(".file");
            file.trigger("click");
        });
        $('input[type="file"]').change(function(e) {
            var fileName = e.target.files[0].name;
            $("#file").val(fileName);

            var reader = new FileReader();
            reader.onload = function(e) {
                // get loaded data and render thumbnail.
                document.getElementById("preview").src = e.target.result;
                document.getElementsByClassName('preview')[0].style.display = 'block';
            };
            // read the image file as a data URL.
            reader.readAsDataURL(this.files[0]);
        });

    </script>


@endsection
