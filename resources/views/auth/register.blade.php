{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                    name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email"
                                class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password"
                                class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    required autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm"
                                class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control"
                                    name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
 --}}
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8" />
    <title>S'inscrire | Lite - Gestion commerciale</title>
    <meta name="description" content="Responsive, Bootstrap, BS4" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- for ios 7 style, multi-resolution icon of 152x152 -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
    <link rel="apple-touch-icon" href="{{asset('assets/images/logo.svg')}}">
    <meta name="apple-mobile-web-app-title" content="Flatkit">
    <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" sizes="196x196" href="{{asset('assets/images/logo.svg')}}">
    <!-- style -->
    <link rel="stylesheet" href="{{asset('libs/font-awesome/css/font-awesome.min.css')}}" type="text/css" />
    <!-- build:css ../assets/css/app.min.css -->
    <link rel="stylesheet" href="{{asset('libs/bootstrap/dist/css/bootstrap.min.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/css/app.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}" type="text/css" />
    <!-- endbuild -->
</head>

<body>
    <div class="d-flex flex-column flex">
        <div class="navbar light bg pos-rlt box-shadow">
            <div class="mx-auto">
                <!-- brand -->
                <a href="index.html" class="navbar-brand">
                    <svg viewBox="0 0 24 24" height="28" width="28" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0 0h24v24H0z" fill="none" />
                        <path
                            d="M19.51 3.08L3.08 19.51c.09.34.27.65.51.9.25.24.56.42.9.51L20.93 4.49c-.19-.69-.73-1.23-1.42-1.41zM11.88 3L3 11.88v2.83L14.71 3h-2.83zM5 3c-1.1 0-2 .9-2 2v2l4-4H5zm14 18c.55 0 1.05-.22 1.41-.59.37-.36.59-.86.59-1.41v-2l-4 4h2zm-9.71 0h2.83L21 12.12V9.29L9.29 21z"
                            fill="#fff" class="fill-theme" />
                    </svg>
                    <img src="../assets/images/logo.png" alt="." class="hide">
                    <span class="hidden-folded d-inline">Lite</span>
                </a>
                <!-- / brand -->
            </div>
        </div>
        <div id="content-body">
            <div class="py-5 text-center w-100">
                <div class="mx-auto w-xxl w-auto-xs">
                    <div class="px-3">
                        <div>
                            <a href="#" class="btn btn-block indigo text-white mb-2">
                                <i class="fa fa-facebook float-left"></i>
                                S'inscrire avec Facebook
                            </a>
                            <a href="#" class="btn btn-block red text-white">
                                <i class="fa fa-google-plus float-left"></i>
                                S'inscrire avec Google+
                            </a>
                        </div>
                        <div class="my-3 text-sm">
                            OU
                        </div>
                        <form name="form" method="POST" action="{{ route('register') }}">
                            @csrf
                            <div class="form-group">
                                <input id="name" name="name" type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Nom d'utilisateur" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                            </div>
                            <div class="form-group">
                                <input id="email" name="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" value="{{ old('email') }}" required
                                autocomplete="email">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input id="password" name="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Mot de passe" required autocomplete="new-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input id="password-confirm" name="password_confirmation" type="password" class="form-control" placeholder="Confirmation du mot de passe" required autocomplete="new-password">

                            </div>
                            <div class="mb-3 text-sm">
                                <span class="text-muted">En cliquant sur S'enregistrer, Je confirme avoir pris connaissance et accepté les </span>
                                <a href="#">Termes et conditions</a>
                                <span class="text-muted">ainsi que la </span>
                                <a href="#">Politique de confidentialité.</a>
                            </div>
                            <button type="submit" class="btn primary">{{ __('S\'enregistrer') }}</button>
                        </form>
                        <div class="py-4 text-center">
                            <div>Vous avez déjà un compte ?
                                <a href="{{route('login')}}" class="text-primary _600">Se connecter</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- build:js ../assets/js/app.min.js -->
    <!-- jQuery -->
    <script src="{{asset('libs/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('libs/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- core -->
    <script src="{{asset('libs/pace-progress/pace.min.js')}}"></script>
    <script src="{{asset('libs/pjax/pjax.min.js')}}"></script>
    <script src="{{asset('assets/js/lazyload.config.js')}}"></script>
    <script src="{{asset('assets/js/lazyload.js')}}"></script>
    <script src="{{asset('assets/js/plugin.js')}}"></script>
    <script src="{{asset('assets/js/nav.js')}}"></script>
    <script src="{{asset('assets/js/scrollto.js')}}"></script>
    <script src="{{asset('assets/js/toggleclass.js')}}"></script>
    <script src="{{asset('assets/js/theme.js')}}"></script>
    <script src="{{asset('assets/js/ajax.js')}}"></script>
    <script src="{{asset('assets/js/app.js')}}"></script>
    <!-- endbuild -->
    <!-- endbuild -->
</body>

</html>
