<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8" />
    <!--<title>Dashboard | Lite - Gestion commerciale</title>-->
    <title>@yield('title')</title>
    <meta name="description" content="Application de gestion commerciale" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- for ios 7 style, multi-resolution icon of 152x152 -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
    <link rel="apple-touch-icon" href="assets/images/logo.svg">
    <meta name="apple-mobile-web-app-title" content="Flatkit">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" sizes="196x196" href="{{ asset('assets/images/logo.svg') }}">
    <!-- style -->
    <link rel="stylesheet" href="{{ asset('libs/font-awesome/css/font-awesome.min.css') }}" type="text/css" />
    <!-- build:css ../assets/css/app.min.css -->

    <link rel="stylesheet" href="{{ asset('libs/bootstrap/dist/css/bootstrap.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/css/select2-bootstrap.min.css') }}" type="text/css" />

    <!-- Datatables -->
    <link rel="stylesheet" href="{{ asset('libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}"
        type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/css/toastr.min.css') }}" type="text/css" />
    {{-- @toastr_css --}}
    <link rel="stylesheet" href="{{ asset('libs/select2/dist/css/select2.min.css') }}">

</head>

<body class="pace-done fixed-aside">
    <div class="app" id="app">
        <!-- ############ LAYOUT START-->
        <!-- ############ Aside START-->
        <div id="aside" class="app-aside fade box-shadow-x nav-expand white" aria-hidden="true">
            <div class="sidenav modal-dialog dk white">
                <!-- sidenav top -->
                <div class="navbar lt">
                    <!-- brand -->
                    <a href="index.html" class="navbar-brand">
                        <svg viewBox="0 0 24 24" height="28" width="28" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0 0h24v24H0z" fill="none" />
                            <path
                                d="M19.51 3.08L3.08 19.51c.09.34.27.65.51.9.25.24.56.42.9.51L20.93 4.49c-.19-.69-.73-1.23-1.42-1.41zM11.88 3L3 11.88v2.83L14.71 3h-2.83zM5 3c-1.1 0-2 .9-2 2v2l4-4H5zm14 18c.55 0 1.05-.22 1.41-.59.37-.36.59-.86.59-1.41v-2l-4 4h2zm-9.71 0h2.83L21 12.12V9.29L9.29 21z"
                                fill="#fff" class="fill-theme" />
                        </svg>
                        <img src="{{ asset('assets/images/logo.png') }}" alt="." class="hide">
                        <span class="hidden-folded d-inline">Gescom</span>
                    </a>
                    <!-- / brand -->
                </div>
                <!-- Flex nav content -->
                <div class="flex hide-scroll">
                    <div class="scroll">
                        <div class="nav-border b-primary" data-nav>
                            <ul class="nav bg">
                                <li class="nav-header">
                                    <div class="py-3">
                                        <a href="{{ route('societe.index') }}"
                                            class="btn btn-sm success theme-accent btn-block">
                                            <i class="fa fa-fw fa-institution"></i>
                                            <span class="hidden-folded d-inline">Informations société</span>
                                        </a>
                                    </div>
                                    <span class="text-xs hidden-folded">Principal</span>
                                </li>
                                <li>
                                    <a href="dashboard.html">
                                        <span class="nav-icon"><i class="fa fa-dashboard"></i></span>
                                        <span class="nav-text">Dashboard</span>
                                    </a>
                                </li>
                                <hr>
                                <li>
                                    <a>
                                        <span class="nav-caret"><i class="fa fa-caret-down"></i></span>
                                        <span class="nav-icon"><i class="fa fa-align-left"></i></span>
                                        <span class="nav-text">Éléments de base</span>
                                    </a>
                                    <ul class="nav-sub">
                                        <li>
                                            <a href="{{ route('famillearticle.index') }}">
                                                <span class="nav-text">Familles d'articles</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="layout.sidenav.html">
                                                <span class="nav-text">Articles</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('client.index') }}">
                                                <span class="nav-text">Clients</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('fournisseur.index') }}">
                                                <span class="nav-text">Fournisseurs</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('collaborateur.index') }}">
                                                <span class="nav-text">Collaborateurs</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('magasin.index') }}">
                                                <span class="nav-text">Magasins</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="pb-2 hidden-folded"></li>
                            </ul>
                            <ul class="nav ">
                                <li class="nav-header hidden-folded mt-2">
                                    <span class="text-xs">Traitement quotidien</span>
                                </li>
                                <li>
                                    <a>
                                        <span class="nav-caret"><i class="fa fa-caret-down"></i></span>
                                        <span class="nav-icon no-fade"><i
                                                class="badge badge-xs badge-o md b-warning"></i></span>
                                        <span class="nav-text">Gestion des achats</span>
                                    </a>
                                    <ul class="nav-sub nav-mega nav-mega-3">
                                        <li>
                                            <a href="ui.arrow.html">
                                                <span class="nav-text">Commandes</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="ui.badge.html">
                                                <span class="nav-text">Livraisons</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="ui.box.html">
                                                <span class="nav-text">Factures</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="ui.box.html">
                                                <span class="nav-text">Avoirs</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a>
                                        <span class="nav-caret"><i class="fa fa-caret-down"></i></span>
                                        <span class="nav-icon no-fade"><i
                                                class="badge badge-xs badge-o md b-success"></i></span>
                                        <span class="nav-text">Gestion des ventes</span>
                                    </a>
                                    <ul class="nav-sub nav-mega">
                                        <li>
                                            <a href="ui.calendar.html">
                                                <span class="nav-text">Devis & Proforma</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="ui.scroll.html">
                                                <span class="nav-text">Livraisons</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="ui.sortable.html">
                                                <span class="nav-text">Factures</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="ui.scrollreveal.html">
                                                <span class="nav-text">Avoirs</span>
                                            </a>
                                        </li>

                                    </ul>
                                </li>
                                <li>
                                    <a>
                                        <span class="nav-caret"><i class="fa fa-caret-down"></i></span>
                                        <span class="nav-icon no-fade"><i
                                                class="badge badge-xs badge-o md b-primary"></i></span>
                                        <span class="nav-text">Gestion des stocks</span>
                                    </a>
                                    <ul class="nav-sub">
                                        <li>
                                            <a href="form.layout.html">
                                                <span class="nav-text">Entrées de stock</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="form.element.html">
                                                <span class="nav-text">Sorties de stock</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="form.validation.html">
                                                <span class="nav-text">Transferts de stock</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-header hidden-folded mt-2">
                                    <span class="text-xs">Extras</span>
                                </li>
                                <li>
                                    <a>
                                        <span class="nav-caret"><i class="fa fa-caret-down"></i></span>
                                        <span class="nav-icon"><i class="fa fa-money"></i></span>
                                        <span class="nav-text">Règlements</span>
                                    </a>
                                    <ul class="nav-sub nav-mega">
                                        <li>
                                            <a href="profile.html">
                                                <span class="nav-text">Règlements client</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="setting.html">
                                                <span class="nav-text">Règlements fournisseur</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="app.inbox.html">
                                        <span class="nav-badge"><b
                                                class="badge badge-sm badge-pill success">6</b></span>
                                        <span class="nav-icon"><i class="fa fa-archive"></i></span>
                                        <span class="nav-text">Inventaire</span>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav bg">
                                <li class="nav-header">
                                    <span class="text-xs hidden-folded">Paramètres généraux</span>
                                </li>

                                <li>
                                    <a>
                                        <span class="nav-caret"><i class="fa fa-caret-down"></i></span>
                                        <span class="nav-icon"><i class="fa fa-book"></i></span>
                                        <span class="nav-text">Comptabilité</span>
                                    </a>
                                    <ul class="nav-sub">
                                        <li>
                                            <a href="{{ route('comptegeneral.index') }}">
                                                <span class="nav-text">Comptes généraux</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('taxe.index') }}">
                                                <span class="nav-text">Taxes</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('journal.index') }}">
                                                <span class="nav-text">Journaux</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('modereglement.index') }}">
                                                <span class="nav-text">Modes de règlement</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a>
                                        <span class="nav-caret"><i class="fa fa-caret-down"></i></span>
                                        <span class="nav-icon"><i class="fa fa-gear"></i></span>
                                        <span class="nav-text">Initialisation</span>
                                    </a>
                                    <ul class="nav-sub">
                                        <li>
                                            <a href="{{ route('pays.index') }}">
                                                <span class="nav-text">Pays</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('ville.index') }}">
                                                <span class="nav-text">Villes</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('devise.index') }}">
                                                <span class="nav-text">Devises</span>
                                            </a>
                                        </li>
                                        <hr>
                                        <li>
                                            <a href="{{ route('domaine.index') }}">
                                                <span class="nav-text">Domaines de gestion</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('typedoc.index') }}">
                                                <span class="nav-text">Types & numérotations</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('suivistock.index') }}">
                                                <span class="nav-text">Types de suivi stock</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('unitevente.index') }}">
                                                <span class="nav-text">Unités de vente</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <hr>
                                <li>
                                    <a href="#">
                                        <span class="nav-icon"><i class="fa fa-user"></i></span>
                                        <span class="nav-text">Utilisateurs</span>
                                    </a>
                                </li>

                                <li class="pb-2 hidden-folded"></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <!-- sidenav bottom -->
                <div class="no-shrink lt">
                    <div class="nav-fold">
                        <a class="d-flex p-2-3" data-toggle="dropdown">
                            <span class="avatar w-28 grey hide">J</span>
                            <img src="{{ asset('assets/images/1.jpg') }}" alt="..." class="w-28 circle">
                        </a>
                        <div class="dropdown-menu  w pt-0 mt-2 animate fadeIn">
                            <div class="row no-gutters b-b mb-2">
                                <div class="col-4 b-r">
                                    <a href="app.user.html" class="py-2 pt-3 d-block text-center">
                                        <i class="fa text-md fa-phone-square text-muted"></i>
                                        <small class="d-block">Rappel</small>
                                    </a>
                                </div>
                                <div class="col-4 b-r">
                                    <a href="app.message.html" class="py-2 pt-3 d-block text-center">
                                        <i class="fa text-md fa-comment text-muted"></i>
                                        <small class="d-block">Chat</small>
                                    </a>
                                </div>
                                <div class="col-4">
                                    <a href="app.inbox.html" class="py-2 pt-3 d-block text-center">
                                        <i class="fa text-md fa-envelope text-muted"></i>
                                        <small class="d-block">E-mail</small>
                                    </a>
                                </div>
                            </div>
                            <a class="dropdown-item" href="profile.html">
                                <span>Profil</span>
                            </a>
                            <a class="dropdown-item" href="setting.html">
                                <span>Paramètres</span>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="docs.html">
                                Besoin d'aide ?
                            </a>
                            <button class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">Se déconnecter
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </button>
                        </div>
                        <div class="hidden-folded flex p-2-3 bg">
                            <div class="d-flex p-1">
                                <a href="app.inbox.html" class="flex text-nowrap">
                                    <i class="fa fa-bell text-muted mr-1"></i>
                                    <span class="badge badge-pill theme">20</span>
                                </a>
                                <a href="lockme.html" class="px-2" data-toggle="tooltip" title="Logout">
                                    <i class="fa fa-power-off text-muted"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ############ Aside END-->
        <!-- ############ Content START-->
        <div id="content" class="app-content box-shadow-0" role="main">
            <!-- Header -->
            <div class="content-header white  box-shadow-0" id="content-header">
                <div class="navbar navbar-expand-lg">
                    <!-- btn to toggle sidenav on small screen -->
                    <a class="d-lg-none mx-2" data-toggle="modal" data-target="#aside">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 512 512">
                            <path d="M80 304h352v16H80zM80 248h352v16H80zM80 192h352v16H80z" />
                        </svg>
                    </a>
                    <!-- Page title -->
                    <div class="navbar-text nav-title flex" id="pageTitle">@yield('navtitle')</div>
                    <ul class="nav flex-row order-lg-2">
                        <!-- Notification -->
                        <li class="nav-item dropdown">
                            <a class="nav-link px-3" data-toggle="dropdown">
                                <i class="fa fa-bell text-muted"></i>
                                <span class="badge badge-pill up danger">15</span>
                            </a>
                            <!-- dropdown -->
                            <div class="dropdown-menu dropdown-menu-right w-md animate fadeIn mt-2 p-0">
                                <div class="scrollable hover" style="max-height: 250px">
                                    <div class="list">
                                        <div class="list-item " data-id="item-7">
                                            <span class="w-24 avatar circle indigo">
                                                <span class="fa fa-bug"></span>
                                            </span>
                                            <div class="list-body">
                                                <a href="" class="item-title _500">Fifth Harmony</a>
                                                <div class="item-except text-sm text-muted h-1x">
                                                    Send you a invitation to SWO
                                                </div>
                                                <div class="item-tag tag hide">
                                                </div>
                                            </div>
                                            <div>
                                                <span class="item-date text-xs text-muted">05:35</span>
                                            </div>
                                        </div>
                                        <div class="list-item " data-id="item-12">
                                            <span class="w-24 avatar circle green">
                                                <span class="fa fa-dot-circle-o"></span>
                                            </span>
                                            <div class="list-body">
                                                <a href="" class="item-title _500">Ashton Cox</a>
                                                <div class="item-except text-sm text-muted h-1x">
                                                    Looking for some client-work
                                                </div>
                                                <div class="item-tag tag hide">
                                                </div>
                                            </div>
                                            <div>
                                                <span class="item-date text-xs text-muted">11:30</span>
                                            </div>
                                        </div>
                                        <div class="list-item " data-id="item-1">
                                            <span class="w-24 avatar circle grey">
                                                <span class="fa fa-comment"></span>
                                            </span>
                                            <div class="list-body">
                                                <a href="" class="item-title _500">Summerella</a>
                                                <div class="item-except text-sm text-muted h-1x">
                                                    Send you a message
                                                </div>
                                                <div class="item-tag tag hide">
                                                </div>
                                            </div>
                                            <div>
                                                <span class="item-date text-xs text-muted">July 21</span>
                                            </div>
                                        </div>
                                        <div class="list-item " data-id="item-5">
                                            <span class="w-24 avatar circle blue-grey">
                                                <span class="fa fa-github"></span>
                                            </span>
                                            <div class="list-body">
                                                <a href="" class="item-title _500">Radionomy</a>
                                                <div class="item-except text-sm text-muted h-1x">
                                                    Competitive gaming ladders
                                                </div>
                                                <div class="item-tag tag hide">
                                                </div>
                                            </div>
                                            <div>
                                                <span class="item-date text-xs text-muted">09:50</span>
                                            </div>
                                        </div>
                                        <div class="list-item " data-id="item-2">
                                            <span class="w-24 avatar circle light-blue">
                                                <span class="fa fa-git"></span>
                                            </span>
                                            <div class="list-body">
                                                <a href="" class="item-title _500">Kygo</a>
                                                <div class="item-except text-sm text-muted h-1x">
                                                    What&#x27;s the project progress now
                                                </div>
                                                <div class="item-tag tag hide">
                                                </div>
                                            </div>
                                            <div>
                                                <span class="item-date text-xs text-muted">08:05</span>
                                            </div>
                                        </div>
                                        <div class="list-item " data-id="item-9">
                                            <span class="w-24 avatar circle cyan">
                                                <span class="fa fa-puzzle-piece"></span>
                                            </span>
                                            <div class="list-body">
                                                <a href="" class="item-title _500">Pablo Nouvelle</a>
                                                <div class="item-except text-sm text-muted h-1x">
                                                    It&#x27;s been a Javascript kind of day
                                                </div>
                                                <div class="item-tag tag hide">
                                                </div>
                                            </div>
                                            <div>
                                                <span class="item-date text-xs text-muted">15:00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex px-3 py-2 b-t">
                                    <div class="flex">
                                        <span>6 Notifications</span>
                                    </div>
                                    <a href="setting.html">See all
                                        <i class="fa fa-angle-right text-muted"></i>
                                    </a>
                                </div>
                            </div>
                            <!-- / dropdown -->
                        </li>
                        <!-- User dropdown menu -->
                        <li class="dropdown d-flex align-items-center">
                            <a href="#" data-toggle="dropdown" class="d-flex align-items-center">
                                <span><strong>{{ Auth::user()->name }}</strong></span>
                                <span class="avatar w-40" style="margin-left: 10px">
                                    <img src="{{ asset('assets/images/1.jpg') }}" alt="...">
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right w pt-0 mt-2 animate fadeIn">
                                <div class="row no-gutters b-b mb-2">
                                    <div class="col-4 b-r">
                                        <a href="app.user.html" class="py-2 pt-3 d-block text-center">
                                            <i class="fa text-md fa-phone-square text-muted"></i>
                                            <small class="d-block">Rappel</small>
                                        </a>
                                    </div>
                                    <div class="col-4 b-r">
                                        <a href="app.message.html" class="py-2 pt-3 d-block text-center">
                                            <i class="fa text-md fa-comment text-muted"></i>
                                            <small class="d-block">Chat</small>
                                        </a>
                                    </div>
                                    <div class="col-4">
                                        <a href="app.inbox.html" class="py-2 pt-3 d-block text-center">
                                            <i class="fa text-md fa-envelope text-muted"></i>
                                            <small class="d-block">E-mail</small>
                                        </a>
                                    </div>
                                </div>
                                <a class="dropdown-item" href="profile.html">
                                    <span>Profil</span>
                                </a>
                                <a class="dropdown-item" href="setting.html">
                                    <span>Paramètres</span>
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="docs.html">
                                    Besoin d'aide ?
                                </a>
                                <button class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">Se déconnecter
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </button>
                            </div>
                        </li>
                        <!-- Navarbar toggle btn -->
                        <li class="d-lg-none d-flex align-items-center">
                            <a href="#" class="mx-2" data-toggle="collapse" data-target="#navbarToggler">
                                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 512 512">
                                    <path d="M64 144h384v32H64zM64 240h384v32H64zM64 336h384v32H64z" />
                                </svg>
                            </a>
                        </li>
                    </ul>
                    <!-- Navbar collapse -->
                    <div class="collapse navbar-collapse no-grow order-lg-1" id="navbarToggler">
                        <form class="input-group m-2 my-lg-0">
                            <span class="input-group-btn">
                                <button type="button" class="btn no-border no-bg no-shadow"><i
                                        class="fa fa-search"></i></button>
                            </span>
                            <input type="text" class="form-control no-border no-bg no-shadow"
                                placeholder="Search projects...">
                        </form>
                    </div>
                </div>
            </div>
            <!-- Main -->
            <div class="content-main " id="content-main">
                <!-- ############ Main START-->

                <!--Mon content-->
                @yield('content')

                <!-- ############ Main END-->
            </div>
            <!-- Footer -->
            <div style="float: right;" class="content-footer white mt-auto" id="content-footer">
                <div class="d-flex p-3">
                    <span class="text-sm text-muted flex">&copy; Copyright. Lite Software</span>
                    <div class="text-sm text-muted">Version 1.1.0</div>
                </div>
            </div>
        </div>

    </div>

    <style>
        form .error {
            color: #ff0000;
        }
    </style>


    <script src="{{ asset('libs/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('libs/jquery-validation/jquery.validate.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('libs/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('libs/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- core -->

    <script src="{{ asset('assets/js/kit-fontawesome.js') }}" crossorigin="anonymous"></script>
    <script src="{{ asset('libs/pace-progress/pace.min.js') }}"></script>
    {{-- <script src="{{ asset('libs/pjax/pjax.min.js') }}"></script>
    --}}
    <script src="{{ asset('assets/js/lazyload.config.js') }}"></script>
    <script src="{{ asset('assets/js/lazyload.js') }}"></script>
    <script src="{{ asset('assets/js/plugin.js') }}"></script>
    <script src="{{ asset('assets/js/nav.js') }}"></script>
    <script src="{{ asset('assets/js/scrollto.js') }}"></script>
    <script src="{{ asset('assets/js/toggleclass.js') }}"></script>
    <script src="{{ asset('assets/js/theme.js') }}"></script>
    <script src="{{ asset('assets/js/ajax.js') }}"></script>
    <script src="{{ asset('assets/js/app.js') }}"></script>
    <script src="{{ asset('libs/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('libs/datatables.net-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery.numeric.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery.alphanum.js') }}"></script>

    <script>
        $('#dtable').DataTable({

            "order": []
        });
        $('#dtable2').DataTable({

            "order": []
        });
        $(document).ready(function() {
            $('.select2').select2({
                theme: "bootstrap"
            });
        });
    </script>
    <!-- endbuild -->
    @yield('scripts')
    @toastr_js
    @toastr_render
</body>

</html>
