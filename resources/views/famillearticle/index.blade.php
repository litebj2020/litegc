@extends('layouts.admin')
@section('title')
    Liste des familles d'articles | Lite - Gestion commerciale
@endsection
@section('navtitle')
    Paramètres généraux - Famile d'article
@endsection

@section('content')
    <div class="padding">
        <p class="btn-groups" style="text-align:right">
            <a style="text-align:left; float: left;" class="btn btn-rounded white" data-tt="tooltip" title="Actualiser"
                href="{{ route('famillearticle.index') }}">
                <i class="fa fa-refresh"></i>
            </a>
            <button class="btn btn-rounded primary" data-toggle="modal" data-target="#bottom">
                <i class="fa fa-plus"></i> Ajouter
            </button>
            <button class="btn btn-outline btn-rounded white b-black text-black">
                <i class="fa fa-print"></i> Imprimer
            </button>
            <button class="btn btn-outline btn-rounded b-warning text-warning">
                <i class="fa fa-file-import"></i> Importer
            </button>
            <button class="btn btn-outline btn-rounded b-success text-success">
                <i class="fa fa-file-export"></i> Exporter
            </button>
        </p>

        @if (session()->has('successMessage'))
            <div class="alert alert-success" role="alert">
                {{ session('successMessage') }}
            </div>
        @endif
        @if (session()->has('errorMessage'))
            <div class="alert alert-danger" role="alert">
                {{ session('errorMessage') }}
            </div>
        @endif
        <div class="table-responsive">
            <table id="dtable" class="table table-sm v-middle p-0 m-0 box">
                <thead>
                    <tr>
                        <th>CODE</th>
                        <th>LIBELLÉ</th>
                        <th>SUIVI</th>
                        <th>UNITÉ</th>
                        <th>VENTE</th>
                        <th>ACHAT</th>
                        <th>STOCK</th>
                        <th class="text-center">NEGATIF ?</th>
                        <th style="text-align:right"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($FamilleartList as $item)
                        <tr>
                            <td class="">{{ $item->CodeFamille }}</td>
                            <td class="">{{ $item->LibelleFamille }}</td>
                            <td class="">{{ $item->psuivistocks->LibelleSuivi }}</td>
                            <td class="">{{ $item->puniteventes->LibelleUnite }}</td>
                            <td class="">{{ $item->CptVente }}</td>
                            <td class="">{{ $item->CptAchat }}</td>
                            <td class="">{{ $item->CptStock }}</td>
                            <td class="text-center">

                                @if ($item->StockNegatif == 1)
                                    <span class="badge danger pos-rlt mr-2  text-sm"><b
                                            class="arrow top b-danger pull-in"></b>AUTORISÉ</span>
                                @else
                                    <span class="badge success pos-rlt mr-2  text-sm"><b
                                            class="arrow top b-success pull-in"></b>NON AUTORISÉ</span>
                                @endif
                            </td>
                            <td class="" style="text-align:right">
                                <a id="btnEdit" data-id="{{ $item->id }}" data-CodeFamille="{{ $item->CodeFamille }}"
                                    data-LibelleFamille="{{ $item->LibelleFamille }}"
                                    data-StockNegatif="{{ $item->StockNegatif }}"
                                    data-psuivistocks="{{ $item->psuivistocks->id . '|' . $item->psuivistocks->CodeSuivi }}"
                                    data-puniteventes="{{ $item->puniteventes->id }}" data-CptVente="{{ $item->CptVente }}"
                                    data-CptAchat="{{ $item->CptAchat }}" data-CptStock="{{ $item->CptStock }}"
                                    class="btn btn-sm black edit" data-toggle="modal" data-target="#bottom2"
                                    title="Modifier" data-tt="tooltip" href="{{ $item->id }}">
                                    <i class="fa fa-pencil default"></i>
                                </a>
                                <a id="btnDelete" class="btn btn-sm text-danger" data-toggle="modal" data-target="#m-a-a"
                                    data-id='{{ $item->id }}' title="Supprimer" data-tt="tooltip" href="{{ $item->id }}"><i
                                        class="fa fa-trash text-danger"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!--Modal-->
        <div class="modal fade" id="bottom" data-backdrop="true" aria-hidden="true">
            <div class="modal-dialog modal-bottom white b-t" style="height:75%">
                <div class="d-flex flex-column h-100">
                    <form id="addForm" action="{{ route('famillearticle.store') }}" method="POST">
                        @csrf
                        <div class="p-3 b-b d-flex no-shrink">
                            <h5 class="h5 m-0">Ajout d'une famille d'article</h5>
                            <div class="ml-auto">
                                <button id="formSubmit" type="submit" class="btn btn-rounded primary">
                                    Enrégistrer
                                </button>
                                <button data-dismiss="modal" class="btn btn-rounded danger">
                                    Annuler
                                </button>
                            </div>

                        </div>
                        <div class="scroll-y">
                            <!--Contenu du modal-->
                            <div class="box">
                                <div class="box-body offset-md-2 col-md-8">
                                    <div class="form-row">
                                        <div class="form-group col-md-2">
                                            <label for="codeFamille">Code famille</label>
                                            <input type="text" id="codeFamille" name="codeFamille"
                                                class="form-control codefamille @error('codeJournal') is-invalid @enderror"
                                                placeholder="Code de la famille (15 caractères maximum)"
                                                onkeyup="this.value = this.value.toUpperCase();">
                                            @error('codeFamille') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-7">
                                            <label for="libelleFamille">Libellé de la famille</label>
                                            <input type="text" id="libelleFamille" name="libelleFamille"
                                                class="form-control @error('libelleFamille') is-invalid @enderror"
                                                placeholder="Libelle de la famille"
                                                onkeyup="this.value = this.value.toUpperCase();">
                                            @error('libelleFamille') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="libelleSuivi">Suivi de stock</label>
                                            <select id="libelleSuivi" name="libelleSuivi"
                                                class="form-control select2 @error('libelleSuivi') is-invalid @enderror"
                                                data-plugin="select2" style="width: 100%" onchange="showNegatif(this);">
                                                <option value="">Sélectionnez le type</option>
                                                @foreach ($SuivistockList as $item)
                                                    <option value="{{ $item->id . '|' . $item->CodeSuivi }}">
                                                        {{ $item->LibelleSuivi }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('libelleSuivi') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-row">

                                        <div class="form-group col-md-3">
                                            <label for="libelleUnite">Unité de vente</label>
                                            <select id="select2-single" name="libelleUnite"
                                                class="form-control select2 @error('libelleUnite') is-invalid @enderror"
                                                data-plugin="select2" style="width: 100%">
                                                <option value="">Sélectionnez l'unité</option>
                                                @foreach ($UniteventeList as $item)
                                                    <option value="{{ $item->id }}">{{ $item->LibelleUnite }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('libelleUnite') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group classcompte col-md-3">
                                            <label for="noCompteVente">N° Compte Vente</label>
                                            <select id="select2-single" name="noCompteVente"
                                                class="form-control select2   @error('noCompteVente') is-invalid @enderror"
                                                data-plugin="select2" style="width: 100%">
                                                <option value="">Sélectionnez le compte</option>
                                                @foreach ($NCompteList as $item)
                                                    <option value="{{ $item->NoCompte }}">
                                                        {{ $item->NoCompte . ' | ' . $item->IntituleCompte }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('noCompteVente') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group classcompte col-md-3">
                                            <label for="noCompteAchat">N° Compte Achat</label>
                                            <select id="select2-single" name="noCompteAchat"
                                                class="form-control select2   @error('noCompteAchat') is-invalid @enderror"
                                                data-plugin="select2" style="width: 100%">
                                                <option value="">Sélectionnez le compte</option>
                                                @foreach ($NCompteList as $item)
                                                    <option value="{{ $item->NoCompte }}">
                                                        {{ $item->NoCompte . ' | ' . $item->IntituleCompte }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('noCompteAchat') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group classcompte col-md-3">
                                            <label for="noCompteStock">N° Compte Stock</label>
                                            <select id="select2-single" name="noCompteStock"
                                                class="form-control select2   @error('noCompteStock') is-invalid @enderror"
                                                data-plugin="select2" style="width: 100%">
                                                <option value="">Sélectionnez le compte</option>
                                                @foreach ($NCompteList as $item)
                                                    <option value="{{ $item->NoCompte }}">
                                                        {{ $item->NoCompte . ' | ' . $item->IntituleCompte }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('noCompteStock') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-row" id="negatif" style="display: block">
                                        <div class="form-group col-md-12">
                                            <p>
                                                Autorisez-vous la gestion de stock en négatif ?
                                                <label class="md-switch">
                                                    <input type="checkbox" id="stockNegatif" name="stockNegatif">
                                                    <i class="blue"></i>
                                                </label>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                    <div style="bottom:0;" class="mt-auto">
                        <div class="p-3 b-t">
                            <a href="#" class="text-muted">
                                <i class="fa fa-fw fa-twitter"></i> Twitter</a>
                            <a href="#" class="text-muted m-h">
                                <i class="fa fa-fw fa-facebook"></i> Facebook</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!--Modal->Modification-->
        <div class="modal fade" id="bottom2" data-backdrop="true" aria-hidden="true">
            <div class="modal-dialog modal-bottom white b-t" style="height:75%">
                <div class="d-flex flex-column h-100">
                    <form id="ff" action="" method="POST">
                        @csrf
                        @method('put')
                        <div class="p-3 b-b d-flex no-shrink">
                            <h5 class="h5 m-0">Modification d'une famille d'article</h5>
                            <div class="ml-auto">
                                <button id="formSubmit" type="submit" class="btn btn-rounded primary">
                                    Enrégistrer
                                </button>
                                <button data-dismiss="modal" class="btn btn-rounded danger">
                                    Annuler
                                </button>
                            </div>
                        </div>
                        <div class="scroll-y">
                            <!--Contenu du modal-->
                            <div class="box">
                                <div class="box-body offset-md-2 col-md-8">
                                    <div class="form-row">
                                        <div class="form-group col-md-2">
                                            <label for="codeFamille2">Code famille</label>
                                            <input type="text" id="codeFamille2" name="codeFamille2"
                                                class="form-control codefamille @error('codeFamille2') is-invalid @enderror"
                                                placeholder="Code de la famille (03 caractères maximum)"
                                                onkeyup="this.value = this.value.toUpperCase();" readonly>
                                            @error('codeFamille2') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-7">
                                            <label for="libelleFamille2">Libellé de la famille</label>
                                            <input type="text" id="libelleFamille2" name="libelleFamille2"
                                                class="form-control @error('libelleFamille2') is-invalid @enderror"
                                                placeholder="Libelle de la famille"
                                                onkeyup="this.value = this.value.toUpperCase();">
                                            @error('libelleFamille') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="libelleSuivi2">Suivi de stock</label>
                                            <select id="libelleSuivi2" name="libelleSuivi2" onchange="showNegatif(this);"
                                                class="form-control select2  @error('libelleSuivi2') is-invalid @enderror"
                                                data-plugin="select2" style="width: 100%">
                                                <option value="">Sélectionnez le type</option>
                                                @foreach ($SuivistockList as $item)
                                                    <option value="{{ $item->id . '|' . $item->CodeSuivi }}">
                                                        {{ $item->LibelleSuivi }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('libelleSuivi2') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-row">

                                        <div class="form-group col-md-3">
                                            <label for="libelleUnite2">Unité de vente</label>
                                            <select id="libelleUnite2" name="libelleUnite2"
                                                class="form-control select2 @error('libelleUnite2') is-invalid @enderror"
                                                data-plugin="select2" style="width: 100%">
                                                <option value="">Sélectionnez l'unité</option>
                                                @foreach ($UniteventeList as $item)
                                                    <option value="{{ $item->id }}">{{ $item->LibelleUnite }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('libelleUnite2') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group classcompte col-md-3">
                                            <label for="noCompteVente2">N° Compte Vente</label>
                                            <select id="noCompteVente2" name="noCompteVente2"
                                                class="form-control select2   @error('noCompteVente2') is-invalid @enderror"
                                                data-plugin="select2" style="width: 100%">
                                                <option value="">Sélectionnez le compte</option>
                                                @foreach ($NCompteList as $item)
                                                    <option value="{{ $item->NoCompte }}">
                                                        {{ $item->NoCompte . ' | ' . $item->IntituleCompte }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('noCompteVente2') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group classcompte col-md-3">
                                            <label for="noCompteAchat2">N° Compte Achat</label>
                                            <select id="noCompteAchat2" name="noCompteAchat2"
                                                class="form-control select2   @error('noCompteAchat2') is-invalid @enderror"
                                                data-plugin="select2" style="width: 100%">
                                                <option value="">Sélectionnez le compte</option>
                                                @foreach ($NCompteList as $item)
                                                    <option value="{{ $item->NoCompte }}">
                                                        {{ $item->NoCompte . ' | ' . $item->IntituleCompte }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('noCompteAchat2') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group classcompte col-md-3">
                                            <label for="noCompteStock2">N° Compte Stock</label>
                                            <select id="noCompteStock2" name="noCompteStock2"
                                                class="form-control select2   @error('noCompteStock2') is-invalid @enderror"
                                                data-plugin="select2" style="width: 100%">
                                                <option value="">Sélectionnez le compte</option>
                                                @foreach ($NCompteList as $item)
                                                    <option value="{{ $item->NoCompte }}">
                                                        {{ $item->NoCompte . ' | ' . $item->IntituleCompte }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('noCompteStock2') <span class="text-danger" role="alert">
                                                    {{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <p>
                                                Autorisez-vous la gestion de stock en négatif ?
                                                <label class="md-switch">
                                                    <input type="checkbox" id="stockNegatif2" name="stockNegatif2">
                                                    <i class="blue"></i>
                                                </label>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                    <div style="bottom:0;" class="mt-auto">
                        <div class="p-3 b-t">
                            <a href="#" class="text-muted">
                                <i class="fa fa-fw fa-twitter"></i>Twitter</a>
                            <a href="#" class="text-muted m-h">
                                <i class="fa fa-fw fa-facebook"></i>Facebook</a>

                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!-- Modal->Suppression-->
        <div id="m-a-a" class="modal black-overlay" data-backdrop="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="deleteForm" action="" method="POST">
                        @csrf
                        @method('delete')
                        <div class="modal-header">
                            <h5 class="modal-title">Confirmation de suppression</h5>
                        </div>
                        <div class="modal-body text-center p-lg">
                            {{-- <input type="text" id="id" name='id'>
                            --}}
                            <p>Êtes-vous sûr de vouloir supprimer cet élément ?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Non</button>
                            <button type="submit" class="btn danger p-x-md">Oui, Supprimer !</button>
                        </div>
                    </form>
                </div>
                <!-- Modal-content -->
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        //Set tooltip
        $(document).ready(function() {
            $("[data-tt=tooltip]").tooltip();
            table = $('#dtable').DataTable();
            $(".codefamille").alphanum({
            allow              : '._-/',
            disallow           : '&é(èçà)ù^$%!:²',
            allowSpace         : false,
            allowNumeric       : true,
            allowUpper         : true,
            allowLower         : true,
            allowCaseless      : true,
           //allowLatin         : true,
           //allowOtherCharSets : true,
            //forceUpper         : true,
           // forceLower         : false,
            maxLength          : 15
        });
        });

        //For update
        $('body').on('click', '#btnEdit', function() {

            let id = $(this).attr('data-id');
            let CodeFamille = $(this).attr('data-CodeFamille');
            let LibelleFamille = $(this).attr('data-LibelleFamille');
            let puniteventes = $(this).attr('data-puniteventes');
            let psuivistocks = $(this).attr('data-psuivistocks');
            let CptVente = $(this).attr('data-CptVente');
            let CptAchat = $(this).attr('data-CptAchat');
            let CptStock = $(this).attr('data-CptStock');
            let libelleSuivi = $(this).attr('data-libelleSuivi');
            let StockNegatif = $(this).attr('data-StockNegatif');


            if (StockNegatif > 0) {
                $('#stockNegatif2').prop('checked', true);
            } else {
                $('#stockNegatif2').prop('checked', false);
            }

            $('#libelleSuivi2').val(psuivistocks);
            $('#libelleSuivi2').select2().trigger('change');

            $('#libelleUnite2').val(puniteventes);
            $('#libelleUnite2').select2().trigger('change');

            $('#noCompteAchat2').val(CptAchat);
            $('#noCompteAchat2').select2().trigger('change');

            $('#noCompteVente2').val(CptVente);
            $('#noCompteVente2').select2().trigger('change');

            $('#noCompteStock2').val(CptStock);
            $('#noCompteStock2').select2().trigger('change');

            $('#codeFamille2').val(CodeFamille);
            $('#libelleFamille2').val(LibelleFamille);

            document.forms.editForm.action = '/famillearticle/' + id;
        });

        //For delete
        $(document).on('click', '#btnDelete', function() {
            let id = $(this).attr('data-id');
            $('#id').val(id);
            document.forms.deleteForm.action = '/famillearticle/' + id;
        });

        function showNegatif(that) {
            let id = that.value.split("|")[1]
            if (id == 0) {
                document.getElementById('negatif').style.display = 'none';
            } else {
                document.getElementById('negatif').style.display = 'block';
            }
        }

        $(function () {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#addForm").validate({
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                libelleFamille: "required",
                libelleSuivi: "required",
                libelleUnite: "required",
                noCompteVente: "required",
                noCompteAchat: "required",
                noCompteStock: "required",
                codeFamille: {
                    required: true,
                    maxlength: 15
                }
            },
            // Specify validation error messages
            messages: {
                libelleFamille: "Veuillez renseigner la famille",
                libelleSuivi: "Veuillez renseigner le suivi",
                libelleUnite: "Veuillez renseigner l'unité",
                noCompteVente: "Veuillez renseigner le numéro de compte de la vente",
                noCompteAchat: "Veuillez renseigner le numéro de compte de l'achat",
                noCompteStock: "Veuillez renseigner le numéro de compte du stock",
                codeFamille: {
                    required: "Veuillez renseigner le code de la famille",
                    maxlength: "Le code de lafamille doit etre 15 caratères au maximum"
                },
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function (form) {

                form.submit();
            }
        });

        $("#ff").validate({

            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                libelleFamille2: "required",
                libelleSuivi2: "required",
                libelleUnite2: "required",
                noCompteVente2: "required",
                noCompteAchat2: "required",
                noCompteStock2: "required",
                codeFamille2: {
                    required: true,
                    maxlength: 15
                }
            },
            // Specify validation error messages
            messages: {
                libelleFamille2: "Veuillez renseigner la famille",
                libelleSuivi2: "Veuillez renseigner le suivi",
                libelleUnite2: "Veuillez renseigner l'unité",
                noCompteVente2: "Veuillez renseigner le numéro de compte de la vente",
                noCompteAchat2: "Veuillez renseigner le numéro de compte de l'achat",
                noCompteStock2: "Veuillez renseigner le numéro de compte du stock",
                codeFamille2: {
                    required: "Veuillez renseigner le code de la famille",
                    maxlength: "Le code de lafamille doit etre 15 caratères au maximum"
                },
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function (form) {
                form.submit();
            }
        });
    });

    </script>

    @if ($errors->has('codeFamille') || $errors->has('libelleFamille') || $errors->has('puniteventes') || $errors->has('CptVente') || $errors->has('CptAchat') || $errors->has('CptStock'))
        <script>
            $(function() {
                $('#bottom').modal({
                    show: true
                });
            });

        </script>
    @endif

    @if ($errors->has('codeFamille2') || $errors->has('libelleFamille2') || $errors->has('puniteventes2') || $errors->has('CptVente2') || $errors->has('CptAchat') || $errors->has('CptStock'))
        <script>
            $(function() {
                $('#bottom2').modal({
                    show: true
                });
            });

        </script>
    @endif
@endsection
