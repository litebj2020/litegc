@extends('layouts.admin')
@section('title')
Fiche du fournisseur | Lite - Gestion commerciale
@endsection
@section('navtitle')
Fournisseur : [{{$CurrentData->CodeTiers}} - {{$CurrentData->Intitule}}]
@endsection

@section('content')
<div class="padding">
    <div class="row">

        <div class="col-sm-6 col-md-4 col-lg-3">
            <div class="box list-item">
                <span class="avatar w-40 text-center circle pink">
                    <span class="fa fa-file"></span>
                </span>
                <div class="list-body">
                    <h4 class="m-0 text-md">
                        <a href="#">58 <span class="text-sm">Documents</span></a>
                    </h4>
                    <small class="text-muted"><span class="text-danger">52 </span>Factures |</small>
                    <small class="text-muted"><span class="text-danger">5 </span>Livraisons |</small>
                    <small class="text-muted"><span class="text-danger">1 </span>Commandes</small>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-4 col-lg-3">
            <div class="box list-item">
                <span class="avatar w-40 text-center rounded primary">
                    <span class="fa fa-dollar"></span>
                </span>
                <div class="list-body">
                    <h4 class="m-0 text-md">
                        <a href="#">75 452 000 <span class="text-sm">FCFA</span></a>
                    </h4>
                    <small class="text-muted">Chiffres d'affaire total.</small>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-4 col-lg-3">
            <div class="box list-item success">
                <span class="avatar w-40 text-center lt">
                    <span class="fa fa-pie-chart"></span>
                </span>
                <div class="list-body">
                    <h4 class="m-0 text-md">
                        <a href="#">2 588 000 <span class="text-sm">FCFA</span></a>
                    </h4>
                    <small class="text-muted">Portefeuille BL & Facture</small>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-4 col-lg-3">
            <div class="box list-item primary theme">
                <div class="list-body">
                    <h4 class="m-0 text-md">
                        <a href="#">6 856 340 <span class="text-sm">FCFA</span></a>
                    </h4>
                    <small class="text-muted">Portefeuille Commande</small>
                </div>
                <span class="avatar w-40 text-center circle dk">
                    <span class="fa fa-pencil"></span>
                </span>
            </div>
        </div>
    </div>

    @if (session()->has('successMessage'))
    <div class="alert alert-success" role="alert">
        {{ session('successMessage') }}
    </div>
    @endif
    @if (session()->has('errorMessage'))
    <div class="alert alert-danger" role="alert">
        {{ session('errorMessage') }}
    </div>
    @endif

    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <a style="text-align:right; float: right;" class="btn btn-rounded white" data-toggle="modal" data-target="#m-a-a" data-tt="tooltip"
                    title="Supprimer" href="{{$CurrentData->id}}">
                        <i class="fa fa-trash text-danger"></i></a>
                    <h2>Fiche du fournisseur</h2>

                    <small>Créé le <span class="text-primary">
                            {{\Carbon\Carbon::parse($CurrentData->created_at)->format('d/m/Y H:m:s')}}</span> par
                        {{$CurrentData->createdby}} | Modifié le <span
                            class="text-primary">{{$CurrentData->updated_at}}</span> par
                        {{$CurrentData->updatedby}}</small>

                </div>
                <div class="box-divider m-0"></div>

                <div class="box-body">
                    <form action="{{ route('fournisseur.update', $CurrentData->id) }}" method="POST">
                        @csrf
                        @method('put')
                        <div class="b-b nav-active-bg">
                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#" data-toggle="tab" data-target="#tab1">Identification</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-toggle="tab" data-target="#tab2">Tarification</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-toggle="tab" data-target="#tab3">Livraison</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-toggle="tab" data-target="#tab4">Solvabilité</a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content p-3 mb-3">
                            <div class="tab-pane animate fadeIn active text-muted" id="tab1">
                                <div class="form-row">
                                    <div class="form-group col-md-2">
                                        <label for="codeTiers">Code tiers</label>
                                        <input type="text" name="codeTiers" class="form-control" @error('codeTiers') is-invalid
                                            @enderror value="{{$CurrentData->CodeTiers}}" readonly>
                                        @error('codeTiers') <span class="text-danger" role="alert">
                                            {{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="intitule">Intitulé</label>
                                        <input type="text" name="intitule" class="form-control" @error('intitule') is-invalid
                                            @enderror value="{{$CurrentData->Intitule}}" placeholder="Intitulé du fournisseur">
                                        @error('intitule') <span class="text-danger" role="alert">
                                            {{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="noCompte">Compte collectif</label>
                                        <select id="select2-single" type="text" name="noCompte"
                                            class="form-control select2 @error('noCompte') is-invalid @enderror"
                                            data-plugin="select2" style="width: 100%;">
                                            <option value="">Sélectionnez le compte</option>
                                            @foreach ($CompteList as $item)
                                            <option {{$item->NoCompte == $CurrentData->NoCompte ? 'selected' : ''}}
                                                value="{{ $item->NoCompte }}">
                                                {{ $item->NoCompte . ' | ' . $item->IntituleCompte}}</option>
                                            @endforeach
                                        </select>
                                        @error('noCompte') <span class="text-danger" role="alert">
                                            {{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="idFiscal">Identifiant fiscal</label>
                                        <input type="text" name="idFiscal" class="form-control" @error('idFiscal') is-invalid
                                            @enderror value="{{$CurrentData->IdFiscal}}"
                                            placeholder="Identifiant fiscal">
                                        @error('idFiscal') <span class="text-danger" role="alert">
                                            {{ $message }}</span>@enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-2">
                                        <label for="infoFiscal1">Régistre</label>
                                        <input type="text" name="infoFiscal1" class="form-control" @error('infoFiscal1')
                                            is-invalid @enderror value="{{$CurrentData->InfoFiscal1}}" placeholder="Régistre">
                                        @error('infoFiscal1') <span class="text-danger" role="alert">
                                            {{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="infoFiscal2">Autre Identifiant</label>
                                        <input type="text" name="infoFiscal2" class="form-control" @error('infoFiscal2')
                                            is-invalid @enderror value="{{$CurrentData->InfoFiscal2}}"
                                            placeholder="Autre identifiant">
                                        @error('infoFiscal2') <span class="text-danger" role="alert">
                                            {{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="contact1">Téléphone</label>
                                        <input type="text" name="contact1" class="form-control" @error('contact1') is-invalid
                                            @enderror value="{{$CurrentData->Contact1}}" placeholder="N° Téléphone">
                                        @error('contact1') <span class="text-danger" role="alert">
                                            {{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="contact2">Fax</label>
                                        <input type="text" name="contact2" class="form-control" @error('contact2') is-invalid
                                            @enderror value="{{$CurrentData->Contact2}}" placeholder="Fax">
                                        @error('contact2') <span class="text-danger" role="alert">
                                            {{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="email">E-mail</label>
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                                <span class="input-group-text">@</span>
                                            </span><input type="email" name="email" class="form-control" @error('email')
                                                is-invalid @enderror value="{{$CurrentData->Email}}"
                                                placeholder="Adresse e-mail">
                                            @error('email') <span class="text-danger" role="alert">
                                                {{ $message }}</span>@enderror
                                        </div>
                                    </div>
                                    <div class="form-group col-md-1">
                                        <label for="codePostal">Code Postal</label>
                                        <input type="text" name="codePostal" class="form-control" @error('codePostal')
                                            is-invalid @enderror value="{{$CurrentData->CodePostal}}" placeholder="Code postal">
                                        @error('codePostal') <span class="text-danger" role="alert">
                                            {{ $message }}</span>@enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="adresse">Adresse</label>
                                        <input type="text" name="adresse" class="form-control" @error('adresse') is-invalid
                                            @enderror value="{{$CurrentData->Adresse}}" placeholder="Adresse géographique">
                                        @error('adresse') <span class="text-danger" role="alert"> {{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="region">Région</label>
                                        <input type="text" name="region" class="form-control" @error('region') is-invalid
                                            @enderror value="{{$CurrentData->Region}}" placeholder="Région ou Département">
                                        @error('region') <span class="text-danger" role="alert"> {{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="ville">Ville</label>
                                        <select id="select2-single" type="text" name="ville"
                                            class="form-control select2 @error('ville') is-invalid @enderror"
                                            data-plugin="select2" style="width: 100%"">
                                            <option value="">Sélectionnez la ville</option>
                                            @foreach ($VilleList as $item)
                                            <option {{$item->NomVille == $CurrentData->Ville ? 'selected' : ''}} value="
                                            {{ $item->NomVille }}">{{ $item->NomVille }}</option>
                                            @endforeach
                                        </select>
                                        @error('ville') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="pays">Pays</label>
                                        <select id="select2-single" type="text" name="pays"
                                            class="form-control select2 @error('pays') is-invalid @enderror"
                                            data-plugin="select2" style="width: 100%">
                                            <option value="">Sélectionnez le pays</option>
                                            @foreach ($PaysList as $item)
                                            <option {{$item->NomPays == $CurrentData->Pays ? 'selected' : ''}}
                                                value="{{ $item->NomPays }}">{{ $item->CodePays . ' | ' . $item->NomPays}}
                                            </option>
                                            @endforeach
                                        </select>
                                        @error('pays') <span class="text-danger" role="alert"> {{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane animate fadeIn text-muted" id="tab2">
                                Tarification
                            </div>
                            <div class="tab-pane animate fadeIn text-muted" id="tab3">
                                Livraison
                            </div>
                            <div class="tab-pane animate fadeIn text-muted" id="tab4">
                                Solvabilité
                            </div>
                        </div>

                        <div class="form-row">
                            <div>
                                <a style="text-align:right; float: right;" class="btn btn-rounded white"
                                    data-tt="tooltip" title="Retour à la liste" href="{{ route('fournisseur.index') }}">
                                    <i class="fa fa-arrow-left text-black"></i></a>
                            </div>
                            <div class="ml-auto">
                                <button type="submit" class="btn btn-rounded primary">Enrégistrer</button>
                            </div>
                        </div>

                    </form>
                </div>


            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h2>Statistiques détaillées</h2>
                    <small>Les graphes ci-dessous vous donnent une vue détaillée sur la situation du tiers</small>
                </div>
                <div class="box-divider m-0"></div>
            </div>
        </div>
        <div class="col-6 col-md-4 col-lg-3">

            <div class="box list-item danger">
                <span class="avatar w-40 text-center lt">
                    <span class="fa fa-pie-chart"></span>
                </span>
                <div class="list-body">
                    <h4 class="m-0 text-md">
                        <a href="#">2 588 000 <span class="text-sm">FCFA</span></a>
                    </h4>
                    <small class="text-muted">Portefeuille BL & Facture</small>
                </div>
            </div>

            <div class="box p-3">
                <div class="py-3">
                    <div class="easypiechart" data-plugin="easyPieChart"
                        data-option="{barColor: app.color.accent}" data-percent="65" data-size="100"
                        data-scale-length="10">
                        <div>
                            <span class="text-accent">65%</span>
                            <div class="text-xs text-muted">growth</div>
                        </div>
                    </div>
                </div>
                <div class="d-flex">
                    <span class="flex text-muted">World market</span>
                    <span><i class="fa fa-caret-up text-success"></i> 10%</span>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="box">
                <div class="box-header">
                    <h3>Line line chart</h3>
                    <small class="block text-muted">Combine charts</small>
                </div>
                <div class="box-body">
                    <canvas id="chart-line-line" height="170">
                    </canvas>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal->Suppression-->
    <div id="m-a-a" class="modal black-overlay" data-backdrop="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="deleteForm" action="{{ route('fournisseur.destroy', $CurrentData->id) }}" method="POST">
                    @csrf
                    @method('delete')
                    <div class="modal-header">
                        <h5 class="modal-title">Confirmation de suppression</h5>
                    </div>
                    <div class="modal-body text-center p-lg">
                        {{-- <input type="text" id="id" name='id'> --}}
                        <p>Êtes-vous sûr de vouloir supprimer cet élément ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Non</button>
                        <button type="submit" class="btn danger p-x-md">Oui, Supprimer !</button>
                    </div>
                </form>
            </div>
            <!-- Modal-content -->
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    //Set tooltip and initialize datatable
    $(document).ready(function () {
        $("[data-tt=tooltip]").tooltip();
        table= $('#dtable').DataTable();
    });
</script>

@endsection
