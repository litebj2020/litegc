<?php

use App\Http\Controllers\AddLotController;
use App\Mail\LiteMail;
use App\Models\Tfamilleart;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PpaysController;
use App\Http\Controllers\PtaxeController;
use App\Http\Controllers\PvilleController;
use App\Http\Controllers\PdeviseController;
use App\Http\Controllers\PdomaineController;
use App\Http\Controllers\PsocieteController;
use App\Http\Controllers\PtypedocController;
use App\Http\Controllers\TarticleController;
use App\Http\Controllers\TjournalController;
use App\Http\Controllers\TmagasinController;
use App\Http\Controllers\TdocachatController;
use App\Http\Controllers\TdocstockController;
use App\Http\Controllers\TdocventeController;
use App\Http\Controllers\TlignedocController;
use App\Http\Controllers\TtierscltController;
use App\Http\Controllers\TtiersfrsController;
use App\Http\Controllers\SessionLotController;
use App\Http\Controllers\PsuivistockController;
use App\Http\Controllers\PuniteventeController;
use App\Http\Controllers\TcomptegeneController;
use App\Http\Controllers\TfamilleartController;
use App\Http\Controllers\PmodereglementController;
use App\Http\Controllers\TcollaborateurController;
use App\Http\Controllers\TcomptearticleController;
use App\Http\Controllers\TdocachatligneController;
use App\Http\Controllers\TdocstockligneController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/testconnection', function () {
    // Test database SQL Server connection
    try {
    DB::connection('sqlsrv')->getPdo();
    echo "Succès de connexion à SQL Server: " . DB::connection('sqlsrv')->getDatabaseName();
    } catch (\Exception $e) {
    die("Échec de connexion à SQL Server. Erreur:" . $e );
    }
    return view('welcome');
    });
*/

Route::get('sendmail', function () {

    $details = [
        'title' => 'Toto l\'argent',
        'body' => 'abragadabra,kfeg,fkjnjnbjgfbkondsnbkngjngbjnfinbjnonbjonkognjbnjgrnbjoniogrnibnr'
    ];
    Mail::to('nabilkpossa51@gmail.com')->send(new LiteMail($details));
    dd("Email envoyé avec succès.");
});

Route::get('/', function () {
    return view('welcome');
});
Route::post('', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('pays', PpaysController::class)->middleware('auth');
Route::resource('ville', PvilleController::class)->middleware('auth');
Route::resource('domaine', PdomaineController::class)->middleware('auth');
Route::resource('societe', PsocieteController::class)->middleware('auth');
Route::resource('unitevente', PuniteventeController::class)->middleware('auth');
Route::resource('suivistock', PsuivistockController::class)->middleware('auth');
Route::resource('taxe', PtaxeController::class)->middleware('auth');
Route::resource('typedoc', PtypedocController::class)->middleware('auth');
Route::resource('devise', PdeviseController::class)->middleware('auth');
Route::resource('modereglement', PmodereglementController::class)->middleware('auth');
Route::resource('journal', TjournalController::class)->middleware('auth');
Route::resource('comptegeneral', TcomptegeneController::class)->middleware('auth');
Route::resource('client', TtierscltController::class)->middleware('auth');
Route::resource('fournisseur', TtiersfrsController::class)->middleware('auth');
Route::resource('magasin', TmagasinController::class)->middleware('auth');
Route::resource('famillearticle', TfamilleartController::class)->middleware('auth');
Route::resource('collaborateur', TcollaborateurController::class)->middleware('auth');
Route::resource('comptearticle', TcomptearticleController::class)->middleware('auth');
Route::resource('article', TarticleController::class)->middleware('auth');
Route::resource('documents-stock', TdocstockController::class)->middleware('auth');
Route::resource('documents-stock-ligne', TdocstockligneController::class)->middleware('auth');
Route::resource('documents-vente', TdocventeController::class)->middleware('auth');
Route::post('addlot', [SessionLotController::class,'add'])->name('addlot');
Route::resource('documents-achat', TdocachatController::class)->middleware('auth');
Route::resource('documents-achat-ligne', TdocachatligneController::class)->middleware('auth');
Route::resource('addlot', SessionLotController::class)->middleware('auth');
Route::get('savelot', [AddLotController::class, 'savelotData'])->name('savelotdata');
Route::get('deletelotline/{id}', [AddLotController::class, 'deletelotline'])->name('deletelotline');



